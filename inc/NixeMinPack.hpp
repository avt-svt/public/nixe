#ifndef NIXE_MIN_PACK_HPP__
#define NIXE_MIN_PACK_HPP__
#include "NixeCnames.hpp"

#ifdef __cplusplus
  extern "C" {
#endif
	void hybrj1_(void(*fcn)(int* , double*, double*, double*,
              int*, int*, void*),int* n,double* x,double* fvec,double* fjac,
			  int* ldfjac,double* tol,int* info,void* comm);

	void hybrj_(void(*fcn)(const int* , const double*, double*, double*,
               const int*, int*), int* n, double* x,double* fvec, double* fjac, int* ldfjac, double* tol,
			   int* maxfev,double* diag, int* mode, double* factor, int* nprint, int* info, int* nfev, int* njev,
			   double* r, int* lr,double* qtf, double* wa1, double* wa2, double* wa3, double* wa4);


#ifdef __cplusplus
  }; // extern "C" 
#endif



#endif // NIXE_MIN_PACK_HPP__
