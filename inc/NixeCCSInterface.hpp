#ifndef _NIXE_CCS_INTERFACE_INCLUDE_
#define _NIXE_CCS_INTERFACE_INCLUDE_



namespace Nixe {

class CCSInterface {
  public:
    int nzmax; // maximum number of entries
    int m;     // number of rows
    int n;     // number of columns
    int *pc;   // column pointers, size n+1
    int *ir;   // row indizes, size nzmax
    double *values;  // numerical values, size nzmax    
  };

} // namespace Nixe

#endif

