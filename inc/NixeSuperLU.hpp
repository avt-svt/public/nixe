#ifndef _NIXE_SUPER_LU_INCLUDE_
#define _NIXE_SUPER_LU_INCLUDE_

#include "NixeConfig.hpp"
#include "NixeCCSInterface.hpp"
#include "NixeDCMInterface.hpp"
#include "NixeUtil.hpp"

namespace Nixe {
  /* class LUresuseInfo: provides data structures and access routines for the keep 
     tracking of the column permutations and 
     the elimination tree for reuse in a the linear solver SuperLU */

  class SuperLUreuseInfo {
  public:
    NIXE_DECLSPEC SuperLUreuseInfo (int dimension); // Constructor which allocates memory of dimeinsion n and sets IsFirstCall=true 
    NIXE_DECLSPEC ~SuperLUreuseInfo ();
    int GetDimension() {return m_Dimension;}
    int *GetPermCol() {return m_PermCol;}
    int *GetEliminationTree() {return m_EliminationTree;}
    bool GetIsFirstCall() {return m_IsFirstCall;}
    void SetIsFirstCall( bool IsFirstCall) {m_IsFirstCall=IsFirstCall;}
  private:
    int m_Dimension; // maximum/current dimension of the linear system
    bool m_IsAllocatedMemory; // Keep track, whether memory is allocated
    int* NIXE_RESTRICT m_PermCol; // size m_Dimension
    int* NIXE_RESTRICT m_EliminationTree;  // size m_Dimension
    bool m_IsFirstCall;  
  };



  // class LU: object oriented interface to SuperLU

  class SuperLUImpl;

  class SuperLU {
  public:
    NIXE_DECLSPEC SuperLU(CCSInterface *linearSystem);
    NIXE_DECLSPEC ~SuperLU();
    NIXE_DECLSPEC void SLUSolveAgain(Nixe::DMatrix& solutionMatrix,
		       const Nixe::DMatrix& rightHandSideMatrix, bool transpose);
    NIXE_DECLSPEC void SLUSolve(Nixe::DMatrix& solutionMatrix,
		  const Nixe::DMatrix& rightHandSideMatrix,
		  Nixe::SuperLUreuseInfo *reuseInfo, bool *success, bool transpose);
  private:
    SuperLUImpl *m_Impl;
  };




} // namespace NIXE



#endif
