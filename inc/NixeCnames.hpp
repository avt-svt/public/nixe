/*! @file NixeCnames.h
 * \brief Macros defining how C routines will be called
 *
 * <pre>
 * -- AdjointNixe--
 * based on slu_Cnames.h from SuperLU package
 *
 * These macros define how C routines will be called.  ADD_ assumes that
 * they will be called by fortran, which expects C routines to have an
 * underscore postfixed to the name (Suns, and the Intel expect this).
 * NOCHANGE indicates that fortran will be calling, and that it expects
 * the name called by fortran to be identical to that compiled by the C
 * (RS6K's do this).  UPCASE says it expects C routines called by fortran
 * to be in all upcase (CRAY wants this). 
 * </pre>
 */
#ifndef _NIXE_CNAMES_INCLUDE_ /* allow multiple inclusions */
#define _NIXE_CNAMES_INCLUDE_ 


#define ADD_       0
#define ADD__      1
#define NOCHANGE   2
#define UPCASE     3


#ifdef UpCase
#define F77_CALL_C UPCASE
#endif

#ifdef NoChange
#define F77_CALL_C NOCHANGE
#endif

#ifdef Add_
#define F77_CALL_C ADD_
#endif

#ifdef Add__
#define F77_CALL_C ADD__
#endif

/* Default */
#ifndef F77_CALL_C
#define F77_CALL_C ADD_
#endif


#if (F77_CALL_C == ADD_)
/*
 * These defines set up the naming scheme required to have a fortran 77
 * routine call a C routine
 * No redefinition necessary to have following Fortran to C interface:
 *           FORTRAN CALL               C DECLARATION
 *           call dgemm(...)           void dgemm_(...)
 *
 * This is the default.
 */

#endif

#if (F77_CALL_C == ADD__)
/*
 * These defines set up the naming scheme required to have a fortran 77
 * routine call a C routine 
 * for following Fortran to C interface:
 *           FORTRAN CALL               C DECLARATION
 *           call dgemv(...)           void dgemv__(...)
 */
/* BLAS */
#define dgemv_ 	dgemv__
#define dcopy_	dcopy__
#define dscal_	dscal__
#define daxpy_	daxpy__
#define dgemm_  dgemm__


/* LAPACK */
#define dgesvx_  dgesvx__
#define dgetrf_  dgetrf__
#define dgetrs_  dgetrs__
#define dgebal_  dgebal__
#define dgehrd_  dgehrd__
#define dhseqr_  dhseqr__

/* NLEQ1S */
#define nleq1s_ nleq1s__

/* MINPACK */
#define hybrj_ hybrj__
#define hybrj1_ hybrj1__

#endif

#if (F77_CALL_C == UPCASE)
/*
 * These defines set up the naming scheme required to have a fortran 77
 * routine call a C routine 
 * following Fortran to C interface:
 *           FORTRAN CALL               C DECLARATION
 *           call dgemm(...)           void DGEMM(...)
 */
/* BLAS */
#define dgemv_ 	DGEMV
#define dcopy_	DCOPY
#define dscal_	DSCAL
#define daxpy_	DAXPY
#define dgemm_  DGEMM

/* LAPACK */
#define dgesvx_  DGESVX
#define dgetrf_  DGETRF
#define dgetrs_  DGETRS
#define dgebal_  DGEBAL
#define dgehrd_  DGEHRD
#define dhseqr_  DHSEQR

/* NLEQ1S */
#define nleq1s_ NLEQ1S

/* MINPACK */
#define hybrj_ HYBRJ
#define hybrj1_ HYBRJ1

#endif


#if (F77_CALL_C == NOCHANGE)
/*
 * These defines set up the naming scheme required to have a fortran 77
 * routine call a C routine 
 * for following Fortran to C interface:
 *           FORTRAN CALL               C DECLARATION
 *           call dgemm(...)           void dgemm(...)
 */
/* BLAS */
#define dgemv_ 	dgemv
#define dcopy_	dcopy
#define dscal_	dscal
#define daxpy_	daxpy
#define dgemm_  dgemm

/* LAPACK */
#define dgesvx_  dgesvx
#define dgetrf_  dgetrf
#define dgetrs_  dgetrs
#define dgebal_  dgebal
#define dgehrd_  dgehrd
#define dhseqr_  dhseqr

/* NLEQ1S */
#define nleq1s_ nleq1s

/* MINPACK */
#define hybrj_ hybrj
#define hybrj1_ hybrj1

#endif



#endif

