#ifndef _NIXE_CONFIG_INCLUDE_
#define _NIXE_CONFIG_INCLUDE_

#define NIXE_RESTRICT
//__restrict

#undef NIXE_DECLSPEC
#ifdef MAKE_NIXE_DLL
#define NIXE_DECLSPEC __declspec(dllexport)
#else
#define NIXE_DECLSPEC 
#endif



#endif
