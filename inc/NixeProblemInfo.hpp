#ifndef _NIXE_DAE_SIZES_INCLUDE_ /* allow multiple inclusions */
#define _NIXE_DAE_SIZES_INCLUDE_

#include "NixeConfig.hpp"

namespace Nixe {

//!  ProblemInfo. 
/*!
  �The purpose of this class is to provide information about the dimensions of the DAE system and maybe some other information
  needed by the callback functions of Nixe::DaeInterface during forward and backward sweeps.
*/
	class ProblemInfo {
	public:
		NIXE_DECLSPEC ProblemInfo() ;
		NIXE_DECLSPEC ~ProblemInfo();
		NIXE_DECLSPEC ProblemInfo(const ProblemInfo& problemInfo);
		const int NumStates() const {return m_NumStates;}
		const int NumParams() const {return m_NumParams;}
		const int NumNonZerosMass() const {return m_NumNonZerosMass;}
		const int NumNonZerosJac() const {return m_NumNonZerosJac;}
		const int NumSwitchFctns() const {return m_NumSwitchFctns;}
		const int NumSens() const {return m_NumSens;}
		const int NumSaveSens() const {return m_NumSaveSens;}
		const int NumAdjoints() const {return m_NumAdjoints;}
		const int NumDers() const {return m_NumDers;}
		const int MaxOrderSens() const {return m_MaxOrderSens;}
		const double StartTime() const {return m_StartTime;}
		const double FinalTime() const {return m_FinalTime;}
		const bool Save() const { return m_Save;}
		const bool IsForwardSweep() const {return m_IsForwardSweep;}
		void SetStartTime(double startTime) {m_StartTime=startTime;}
		void SetFinalTime(double finalTime) {m_FinalTime=finalTime;}
		void SetNumStates(int numStates) {m_NumStates=numStates;}
		void SetNumParams(int numParams) {m_NumParams=numParams;}
		void SetNumNonZerosMass (int numNonZerosMass) {m_NumNonZerosMass=numNonZerosMass;}
		void SetNumNonZerosJac(int numNonZerosJac)  {m_NumNonZerosJac=numNonZerosJac;}
		void SetNumSwitchFctns(int numSwitchFctns)  {m_NumSwitchFctns=numSwitchFctns;}
		void SetNumSens(int numSens)  {m_NumSens=numSens;}
		void SetNumSaveSens(int numSaveSens) {m_NumSaveSens=numSaveSens;}
		void SetNumAdjoints(int numAdjoints)  {m_NumAdjoints=numAdjoints;}
		void SetNumDers(int numDers)  {m_NumDers=numDers;}
		void SetSave(bool save){m_Save=save;}
		void SetIsForwardSweep(bool isForwardSweep) {m_IsForwardSweep=isForwardSweep;}
		void SetMaxOrderSens(int maxOrderSens) {m_MaxOrderSens=maxOrderSens;}
	private:
		double m_StartTime; // start time of integration
		double m_FinalTime; // stop time of integration
		int m_NumStates; // dimension of the state vector x
		int m_NumParams; // dimension of the parameter vector p
		int m_NumNonZerosMass; // number of nonzeros in the mass matrix M
		int m_NumNonZerosJac; // number of nonzeros in the state Jacobian f_x(t,x,p) 
		int m_NumSwitchFctns; // number of switching function
		int m_NumSens; // number of sensitivities for forward sweep
		int m_NumSaveSens; // number of sensitivities to be saved in forward sweep for subsequent backward sweep
						   //  condition m_NumSaveSens <= m_NumSens
		int m_NumAdjoints; // number of adjoint vectors lambda (possibly including higher order adjoints D/Dp_i lambda
		int m_NumDers;     // dimension adjoint projection lambda^T f_p(t,x,p) and D/Dp_i lambda^T f_p(t,x,p) 
		int m_MaxOrderSens; // max order of forward sensitivities 
		bool m_Save; // if true, checkpoints are saved during forward sweep for subsequent backward sweep
		bool m_IsForwardSweep; // determin kind of current sweep, true, if current sweep is forward sweep, false if 
								// current sweep is backward sweep

	} ;

} // end namespace Nixe



#endif
