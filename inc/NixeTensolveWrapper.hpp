#ifndef __NIXE_TENSOLVE_WRAPPER_HPP__
#define __NIXE_TENSOLVE_WRAPPER_HPP__

#include <memory>
#include "NixeUtil.hpp"
#include "NixeConfig.hpp"

namespace Nixe {

	class EquationSystemTENSOLVE { // for square systems
	public:
		virtual void Func(const double *X, double *FX, const int& M, const int& N) = 0;
		virtual void Jacobian(const double *X, double *JAC, const int& MAXM, const int& M, const int& N)=0;
		virtual void InitialGuess(const int N, double *X0) {VectorCopy(N,X0,0.1);}
		virtual int GetDimension()=0;
	};

	class TensolveWrapper {
	public:
		NIXE_DECLSPEC TensolveWrapper(EquationSystemTENSOLVE *equationSystem); 		
		NIXE_DECLSPEC ~TensolveWrapper();
		NIXE_DECLSPEC void Solve();
		NIXE_DECLSPEC void Print();
		void GetSolution(double* solution) {VectorCopy(m_N,solution,m_Solution.Data());}
		bool SuccesfulSolved(){return m_Success;}
	private:
		EquationSystemTENSOLVE *m_EquationSystem;
		int m_N;
		int m_M;
		Array<double> m_Solution;
		bool m_Success;
	};



}



#endif