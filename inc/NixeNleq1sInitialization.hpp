/*
 * InitInterface.hpp
 *
 *  Created on: 08.01.2010
 *      Author: Moritz Schmitz and Ralf Hannemann
 */
#ifndef _INIT_INTERFACE_
#define _INIT_INTERFACE_

#include <memory>
#include <fstream>
#include <iostream>
#include <iomanip>
#include <ios>
#include <string>
#include "NixeDaeInterface.hpp"
#include "NixeDenseColumnMajor.hpp"
#include "NixeSuperLU.hpp"
#include "NixeUtil.hpp"
#include "NixeNleq1sWrapper.hpp"
#include "NixeNleq1sEquationSystem.hpp"
#include "NixeConfig.hpp"

namespace Nixe{


/*Class that finds consistent initial values and sensitivities for the algebraical variables for the 
system defined by "dae". The corresponding form of "dae" is:

								M dx^d/dt = F^d(x^a,x^d,p)
									    0 = F^a(x^a,x^d,p)

The system, the initial values of the differential variables and estimates of the initial values of the
algebraical variables have to be provided by the user.*/

class InitInterface {

public:
	
	NIXE_DECLSPEC InitInterface(DaeInterface<CCSInterface> *dae, const double tol);
	NIXE_DECLSPEC ~InitInterface();
	/*--------------------------------------------------
	Calculates consistent x^a(0)
	--------------------------------------------------*/
	NIXE_DECLSPEC void InitializeAlgVar(double *states);
	NIXE_DECLSPEC void InitializeAlgVarBlockDec(double *states);

	/*--------------------------------------------------
	Calculates consistent dx^a/dp(0)
	--------------------------------------------------*/
	NIXE_DECLSPEC void InitializeAlgSens(double *sens);

	NIXE_DECLSPEC void PrintSensToFile(const std::string& fileName);
	

private:
		DaeInterface<CCSInterface> *m_Dae;
		NIXE_SHARED_PTR<Nixe::ProblemInfo> m_Dae_problemInfo;
		std::unique_ptr<Nleq1sWrapper> m_Solver;
		std::unique_ptr<MyEquationSystem> m_EqSystem;
		std::unique_ptr<Nixe::CompressedColumn> m_LinearSystem;
		std::unique_ptr<Nixe::SuperLU> m_SuperLU;
		std::unique_ptr<Nixe::DMatrix> m_SolMatrix;
		Nixe::Array<double> m_SensAlg; // will contain the solution of InitializeAlgSens()
		//Nixe::Array<double> m_Parameters;
		Nixe::Array<double> m_Sens;
		int m_numMaxNonZerosLinearSystem;
		double m_Tol;

};



}
#endif /* _INIT_INTERFACE_ */
