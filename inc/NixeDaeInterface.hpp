/*
 * NixeDaeInterface.hpp
 *
 *  Created on: 20.09.2009
 *      Author: Ralf Hannemann
 */

#ifndef _NIXE_DAE_INTERFACE_HPP_
#define _NIXE_DAE_INTERFACE_HPP_


#include "NixeProblemInfo.hpp"
#include "NixeAdjointData.hpp"
#include "NixeCompressedColumn.hpp"
#include "NixeSharedPtr.hpp"

namespace Nixe{




/* DaeInterface has be be provided by the user by means of inheritance
It comprises mainly the callback functions to be provided by the user */
template <typename MatrixInterface>
class DaeInterface : public NIXE_ENABLE_SHARED_FROM_THIS< DaeInterface<MatrixInterface> > {
	public:
		virtual ~DaeInterface() {}

		 /*! \brief Eval the mass matrix M of the linearly-implicit DAE  M*x' = f(t,x,p)
		 *  \param[out] mass: time-invariant mass matrix: M
		 */
		virtual void EvalMass(MatrixInterface *mass)=0;

		
		
		/*! \brief Given the DAE system M*x'=f(t,x,p) and the first-order 
		 * sensitivity equations M*S'= D/Dp f(t,x,p), (higher orders are also allowed)
		 * this routine provides the right hand side f(t,x,p), possibly higher order tangent-linear
		 * models of the right hand side, and the Jacobian f_x(t,x,p) of the right hand side
		 *  \param[in] evalJac true, if Jacobian has to be set, false if not
		 *  \param[in] time  current time t
		 *  \param[in] states states at current time : x(t), size problemInfo.NumStates()
		 *  \param[in] sens  sensitivities at current time : S(t) ) D/Dp x(t)
		 *  \param[in] parameters p 
		 *  \param[out] rhsStates f(t,x,p)
		 *  \param[out] rhsSens  D/Dp f(t,x,p) , (higher-order) tangent linear models of f(t,x,p)
		 *  \param[in] problemInfo information about DAE system
		 */
		virtual void EvalForward(bool evalJac, double time, double* states, double *parameters, 
			double *rhsStates, double* sens, MatrixInterface* jacobian, 
			double* rhsStatesDtime, double* rhsSens, const Nixe::ProblemInfo& problemInfo) const = 0; 

		 /*! \brief Given the DAE system M*x'=f(t,x,p) this right routine evaluation
		 * (higher order adjoint projections) lambda^T*f_x(t,x,p) and lambda^T*f_p(t,x,p). Here lambda are
		 * the possibly first-order adjoints. For second-order adjoints the total derivatives with respect
		 * to the parameters p_i are computed: D/Dp_i lambda^T*f_x(t,x,p) and D/Dp_i lambda^T*f_p(t,x,p)  
		 *  \param[in]  time: t
		 *  \param[in] states: states at current time : x(t)
 		 *  \param[in] parameters: p 
		 *  \param[in] sens: sensitivities at current time : S(t)	
		 *  \param[in] adjoints: (higher-order) adjoints at current time : lambda(t) or D/Dp_i lambda
		 *  \param[out] rhsAdjoints: lambda^T*f_x(t,x,p) or D/Dp_i lambda^T*f_x(t,x,p)
		 *  \param[out] rhsDers:  lam^T*f_p(t,x,p) or D/Dp_i lam^T*f_p(t,x,p)
		 *  \param[in] problemInfo: information about DAE system
		 */
		virtual void EvalReverse(double time, double *states, double* parameters, double* sens, double* adjoints, 
			double * rhsAdjoints, double* rhsDers, const Nixe::ProblemInfo& problemInfo) const {}

		/*! \brief Set the intial values of the state variables x(t_0) and sensitivities S(t_0)
         *
		 *  \param[in] time: start time, where the states and sensitiviteis are to be set
	     *  \param[in] parameters: p 
		 *  \param[out] states: states at intial  time : x(t_0)
		 *  \param[out] sens: sensitivities at intial time : S(t_0)
		 *  \param[in] problemInfo: information about DAE system 
		 */
		virtual void EvalInitialValues(double time, double *parameters, double* states, double* sens, 
			const Nixe::ProblemInfo& problemInfo) {};




		 /*! \brief Set the final values of the adjoint variables
          *
		 *  \param[in] time: final time, where the adjoints are to be set
	     *  \param[in] parameters: p 
		 *  \param[in] states: states at final time : x(t_f)
		 *  \param[in] sens: sensitivities at final time : S(t_f)
		 *  \param[out] adjoints: adjoints at final time : lambda(t_f)
		 *  \param[in] problemInfo: information about DAE system 
		 */
		virtual void EvalFinalValues(double time, double *states, double *parameters, 
			double* sens, double* adjoints, const Nixe::ProblemInfo& problemInfo) 	
		{		
		}
		


		 /*! \brief Eval (vector-valued) switching function sigma(t,x,p) for nonsmooth systems
          *
		 *  \param[in]  time: t
		 *  \param[in] states: states at current time : x(t)
 		 *  \param[in] parameters: p 
		 *  \param[out] switchingFctn: values of switching function sigma(t,x,p)
		 *  \param[in] problemInfo: information about DAE system 
		 */		
		virtual void EvalSwitchingFunction(double time, double *states, double *parameters, 
			double *switchingFctn, const Nixe::ProblemInfo& problemInfo) const {}

		
		
		/*! \brief Initial set of parameters p of the DAE right hand side f(t,x,p)
          *
		  *  \param[out] parameters: p
	      */	
		virtual void EvalParameters(double *parameters, const Nixe::ProblemInfo& problemInfo) {}

		
		
		 /*! \brief provide necessary information about the DAE system, linke number of states, sensitivities,
		  * number of nonzeros in the Jacobian, etc. This routine returns a smart pointer to the ProblemInfo class described in 
		  * NixeProblemInfo.hpp
          *	
		  * \return Information about the DAE system, see Nixe::ProblemInfo.hpp
		  */
		virtual NIXE_SHARED_PTR<Nixe::ProblemInfo> GetProblemInfo() {
			NIXE_SHARED_PTR<Nixe::ProblemInfo> temp;
			return temp;
		}

		 /*! \brief obtain the structure of the jacobian matrix
          *
          * The method obtains information about the jacobian matrix (
          * the derivatives of f w.r.t x ), x contains ONLY states.
		  * The information returned comprises the location of entries in the 
		  * jacobian matrix. The caller of the method has to allocate space in
		  * appropriate dimensions in the rows and cols parameters. 
          *
          * \param nNonZeroDfdx - holds the number of non zeros in df/dx 
		  *				after the call
          * \param rows - holds the rows of the elements after the call
          * \param cols - holds the columns of the elements after the call
          */
		virtual void GetStateJacobianStruct(int& nNonZeroDfdx, int* rows, int* cols){}

		/*! \brief obtain selected rhs values 
          *
          * The method stores the selected rhs-values in rhsStates. �t takes the equation indices 
		  * of length len, the time and all the statevalues as input.
          *
          * \param[in] len: length of indices
		  * \param[in] indices: equation indices of rhsStates
		  *	\param[in] time: Evaluation time
		  * \param[in] states: State-values for the full System (size = numStates in ESO)
		  * \param[in] dimStates: dimension of states array
		  * \param[out] rhsStates: array of length len that contains the right-hand-side values after the call
          */
		virtual void EvalRhs(int len, long* indices, double time, double* states, int dimStates, double* rhsStates) {
			// default implementation (inefficient because no use of "block-evaluation" of residual function):
			
			NIXE_SHARED_PTR<Nixe::ProblemInfo> oldProblemInfo=this->GetProblemInfo();
			const int n_p = oldProblemInfo->NumParams();
			Nixe::Array<double> params(n_p);
			this->EvalParameters(params.Data(),*oldProblemInfo);
			int dimTotalSystem = dimStates;
			Nixe::ProblemInfo problemInfo;
			problemInfo.SetNumSens(0);
			problemInfo.SetNumSaveSens(0);
			problemInfo.SetNumParams(n_p);
			problemInfo.SetNumStates(dimTotalSystem);
			Nixe::Array<double> rhsStatesTemp(dimTotalSystem);
			this->EvalForward(false, time, states, params.Data(), rhsStatesTemp.Data(), 0, 0, 0, 0, problemInfo);
			if(indices == NULL){
				for(int i=0; i<len; i++){
					rhsStates[i] = rhsStatesTemp[i];
				}
			}
			else{
				for(int i=0; i<len; i++){
					rhsStates[i] = rhsStatesTemp[indices[i]];
				}
			}
		}

		/*! \brief Evaluates a quadratic subpart of the state-jacobian of the entire system and returns it in triplet format
		 *
		 *  Given the row- and column indices of the state-jacobian subpart in "rows" and "cols", the method calculates
		 *	the "reduced Jacobian" by using the row-wise evaluation ("block-evaluation") of the tangent-linear AD code.
		 *  If the user can't supply "block-evaluation" methods of the residuum function and his first-order derivatives, a
		 *	default implementation is provided below.
		 *
		 *	\param[in] time: Evaluation time
		 *  \param[in] states: State-values for the full System (size = numStates in ESO)
		 *  \param[in] dimStates: dimension of states array
		 *  \param[in] rows: row indices of "reduced" Jacobian in coordinate format
		 *  \param[in] cols: column indices of "reduced" Jacobian in coordinate format
		 *  \param[in] dim: dimension of reduced Jacobian
		 *  \param[in] SparsityPattern : It contains the indices of the non-zero entries of the reduced jacobian matrix
		 *								 in row-major format. Has not to be provided if the default implementation is used.
		 *																			   x 0 0 x 
		 *								 For example: the sparsity pattern of matrix   0 0 x x  would be: [0 3 6 7 9 10 11 13 15].
		 *																			   0 x x x
		 *																		       0 x 0 x
		 *  \param[in] len: number of nonZeros in reduced Jacobian
		 *  \param[out] jacRowInd: row indices of "reduced" Jacobian entries
		 *  \param[out] jacColInd: col indices of "reduced" Jacobian entries
		 *  \param[out] jacVal: values of "reduced" Jacobian in coordinate format
		 */
		virtual void EvalStateJac(double time, double* states, int dimStates, int* rows, int* cols, int dim, int* SparsityPattern, int* jacRowInd, int* jacColInd, double* jacVal, int len){}
	private:



	};
template<>
NIXE_DECLSPEC void DaeInterface<Nixe::CCSInterface>::EvalStateJac(double time, double *states, int dimStates, int *rows, int *cols, int dim, int *SparsityPattern, int *jacRowInd, int *jacColInd, double *jacVal, int len);

template<>
NIXE_DECLSPEC void DaeInterface<Nixe::CCSInterface>::GetStateJacobianStruct(int& nNonZeroDfdx, int* rows, int* cols) ;

} //namespace Nixe


#endif /* _NIXE_DAE_INTERFACE_HPP_ */
