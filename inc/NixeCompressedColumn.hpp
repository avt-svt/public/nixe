#ifndef _NIXE_COMPRESSED_COLUMN_INCLUDE_
#define _NIXE_COMPRESSED_COLUMN_INCLUDE_

#include "NixeConfig.hpp"
#include "NixeCCSInterface.hpp"
#include "NixeSuperLU.hpp"
#include <iostream>

struct cs_sparse;

namespace Nixe {
  
  class CompressedColumn : public CCSInterface {
  public:
    typedef CCSInterface  		TypeInterface;
    typedef SuperLUreuseInfo 	TypeLuReuse;
//    typedef SuperLU       TypeLinearSolver;
    NIXE_DECLSPEC CompressedColumn(int m, int n, int nnz);
    NIXE_DECLSPEC CompressedColumn(const double alpha, const CompressedColumn& A,
    		const double beta, const CompressedColumn& B);
    NIXE_DECLSPEC ~CompressedColumn();
    NIXE_DECLSPEC void Add(double *result, const int m, const int n);
    NIXE_DECLSPEC void Multiply(const double coeff, double *result,const double *toMult,const int m,const int n);
    NIXE_DECLSPEC void TransposeMultiply(const double coeff, double *result,const double *toMult,const int m,const int n);
    TypeInterface* Interface() {return this; }
    void Solve(DMatrix& solution,const DMatrix& rhs,
    		SuperLUreuseInfo * reuse, bool *success, bool transpose) {
      if(m_Lu) delete m_Lu;
      m_Lu = new SuperLU(this);
      m_Lu->SLUSolve(solution,rhs,reuse,success,transpose);
    }
    void SolveAgain(DMatrix& solution,const DMatrix& rhs, bool transpose) {
       m_Lu->SLUSolveAgain(solution,rhs,transpose);
    }
	NIXE_DECLSPEC void Transpose();
	NIXE_DECLSPEC int GetMaximalBlockSize() const;
  private:
    struct cs_sparse* m_Imp;
    SuperLU *m_Lu;
  };
} // namespace Nixe
#endif

