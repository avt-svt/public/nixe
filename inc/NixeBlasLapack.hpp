#ifndef _NIXE_BLASLAPACK_INCLUDE_ /* allow multiple inclusions */
#define _NIXE_BLASLAPACK_INCLUDE_ 

#include "NixeCnames.hpp"

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */
	/*BLAS */
	void dgemv_(const char *trans, const int *m, const int *n, const double *alpha,
		const double *a, const int *lda, const double *x, const int *incx,
		const double *beta, double *y, const int *incy);
	void   daxpy_(const int *n, const double *alpha, const double *x, const int *incx,
		double *y, const int *incy);
	void   dscal_(const int *n, const double *a, double *x, const int *incx);
	void   dcopy_(const int *n, const double *x, const int *incx, double *y, const int *incy);
	void  dgemm_(const char *transa, const char *transb, const int *m, const int *n, const int *k,
		const double *alpha, const double *a, const int *lda, const double *b, const int *ldb,
		const double *beta, double *c, const int *ldc);

	/* LAPACK */
	// linear systems
	void  dgesvx_( char *fact, char *trans, int *n, int *nrhs, double *a, int *lda, double *af, int *ldaf, int *ipiv, char *equed, double *r, double *c, double *b, int *ldb, double *x, int *ldx, double *rcond, double *ferr, double *berr, double *work, int *iwork, int *info );
	void  dgetrf_( const int *m, const int *n, double *a, const int * lda, int * ipiv, int * info);
	void  dgetrs_( char *trans, const int *n, const int *nrhs, const double *a, const int *lda,
		const int *ipiv, double* b, const int *ldb, int* info);
  // eigenvalue problems
	void    dgebal_( char *job, int *n, double *a, int *lda, int *ilo, 
						   int *ihi, double *scale, int *info );
	void    dgehrd_( int *n, int *ilo, int *ihi, double *a, int *lda, 
						   double *tau, double *work, int *lwork, int *info );
  void    dhseqr_( char *job, char *compz, int *n, int *ilo, int *ihi, 
						   double *h, int *ldh, double *wr, double *wi, double *z, 
						   int *ldz, double *work, int *lwork, int *info );

#ifdef __cplusplus
}
#endif /* __cplusplus */


#endif
