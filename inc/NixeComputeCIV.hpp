#ifndef _NIXE_CIV_HPP_
#define _NIXE_CIV_HPP_

#include "NixeNleq1sInitialization.hpp"
#include "NixeDaeInterface.hpp"
#include "NixeCCSInterface.hpp"
#include "NixeUtil.hpp"
#include <cstdlib>

namespace Nixe {

template<typename MatrixInterface> 
void ComputeConsistentInitialValues(NIXE_SHARED_PTR<Nixe::DaeInterface<MatrixInterface> > dae,int maxOrderSens,double*states, double *sens, const double tol){
	std::cerr << "Initialization not implemented yet for dense systems. Use sparse CCS representation for CIV computation!" << std::endl;
	exit(1);
}

template<>
inline void ComputeConsistentInitialValues<Nixe::CCSInterface> (NIXE_SHARED_PTR<Nixe::DaeInterface<Nixe::CCSInterface> > dae,int maxOrderSens,double *states, double *sens, const double tol){
//	double t0=Nixe::timer();
	std::unique_ptr<InitInterface> initInt(new InitInterface(dae.get(),tol));
	NIXE_SHARED_PTR<Nixe::ProblemInfo> problemInfo=dae->GetProblemInfo();
	const int numSens=problemInfo->NumSens();
	(*initInt).InitializeAlgVarBlockDec(states);
	if (numSens>0) (*initInt).InitializeAlgSens(sens);
//	double t1=Nixe::timer();

//	std::cout << "Intializiation took " << t1-t0 << " seconds" << std::endl;
}

}

#endif
