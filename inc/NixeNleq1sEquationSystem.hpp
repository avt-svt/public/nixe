/*
 * MyEquationSystem.hpp
 *
 *  Created on: 08.01.2010
 *      Author: Moritz Schmitz and Ralf Hannemann
 */
#ifndef __MY_NIXE_NLEQ1S_EQUATION_SYSTEM_HPP__
#define __MY_NIXE_NLEQ1S_EQUATION_SYSTEM_HPP__

#include "NixeNleq1sWrapper.hpp"
#include "NixeDaeInterface.hpp"
#include "NixeUtil.hpp"
#include "NixeProblemInfo.hpp"
#include "NixeCompressedColumn.hpp"
#include "NixeConfig.hpp"
#include <memory>

using namespace Nixe;


class MyEquationSystem : public EquationSystem {
public:
	NIXE_DECLSPEC ~MyEquationSystem();
	NIXE_DECLSPEC MyEquationSystem(DaeInterface<CCSInterface> *dae, const Nixe::ProblemInfo *dae_problemInfo);
	int GetDimension(){return m_Nalg;};
	int GetNumSens(){return m_Nu;};
	int GetDimDiff(){return m_Nx - m_Nalg;};
	int GetNonZeroJac(bool Alg){return (Alg) ? m_NonZeroJacAlg : m_NonZeroJacDiff;};
	void SetStates(double* states);
	double* GetStates(){return m_States.Data();};
	double GetStatesNum(int inum){return m_States[inum];};
	double* GetSens(){return m_Sens.Data();};
	double GetSensNum(int inum){return m_Sens[inum];};
	double* GetParameters(){return m_Parameters.Data();};
	double* GetRhs(){return m_Rhs.Data();};
	double* GetRhsSens(){return m_RhsSens.Data();};
	double* GetRhsStatesDtime(){return m_RhsStatesDtime.Data();};
	int GetVarAlg(int inum){return m_VarAlg[inum];};
	int IsVarAlg(int inum); // if variable with index inum is algebraic then the position in m_VarAlg is returned, else -1
	int IsEqnAlg(int inum); // if equation with index inum is algebraic then the position in m_EqnsAlg is returned, else -1
	int GetVarDiff(int inum){return m_VarDiff[inum];};
	int GetEqnsAlg(int inum){return m_EqnsAlg[inum];}
	int GetEqnsDiff(int inum) {return m_EqnsDiff[inum];}
	NIXE_DECLSPEC void InitialGuess(const int N, double *X0);
	/*--------------------------------------------------
	Calculates the Right-Hand-Side in X
	--------------------------------------------------*/
	NIXE_DECLSPEC void Func(const int& N, const double *X, double *FX, int& IFAIL);
	/*--------------------------------------------------------------------------
	Calculates the Jacobian of the Rhs with respect to X
	--------------------------------------------------------------------------*/
	NIXE_DECLSPEC void Jacobian(bool isFirstCall,const int& N,
		const double *X,double *DFX,int *IROW,int *ICOL,int& NFILL,int& IFAIL);
	/*--------------------------------------------------------------------------
	Finds the number of algebraical variables of the system defined by dae
	--------------------------------------------------------------------------*/
	//int MyEquationSystem::FindNumAlgVar(DaeInterface *dae){
	//	std::auto_ptr<Nixe::CompressedColumn> MassMatrix(new Nixe::CompressedColumn(dae->sizeInfo.numStates,dae->sizeInfo.numStates,dae->sizeInfo.numStates*dae->sizeInfo.numStates));
	//	myDae->EvalMass(MassMatrix.get());
	//	m_Ndiff = MassMatrix->pc[dae->sizeInfo.numStates];
	//	m_Nalg = dae->sizeInfo.numStates - m_Ndiff;
	//	return m_Nalg;
	//};

	/*--------------------------------------------------------------------------
	Finds the variable-indices of the algebraical variables
	--------------------------------------------------------------------------*/
	NIXE_DECLSPEC void FindAlgVar();
	NIXE_DECLSPEC void FindAlgEqns();
private:
	DaeInterface<CCSInterface> *myDae;
	NIXE_SHARED_PTR<Nixe::ProblemInfo> m_Dae_problemInfo;
	int m_Nx;
	int m_Nu;
	int m_Nalg;		  // m_Nalg equals number of algebraical variables
	int m_Ndiff;	  // m_Ndiff equals number of differential variables
	int m_NonZeroJacAlg;  // m_NonZeroJac equals number of non-zero entries in the Jacobian matrix dF^a/dx^a
	int m_NonZeroJacDiff; // m_NonZeroJac equals number of non-zero entries in the Jacobian matrix dF^a/dx^d
	Nixe::Array<double> m_Parameters;
	Nixe::Array<double> m_Rhs;
	Nixe::Array<double> m_RhsSens;
   	Nixe::Array<double> m_States; // initial states of differential variables and current states of algebraical variables
	Nixe::Array<double> m_Sens;  // initial sensitivities of differential variables and current sensitivities of algebraical variables
	Nixe::Array<double> m_RhsStatesDtime; // dpartial f/ dpartial t
	Nixe::Array<int> m_VarAlg;  // stores the indices of the algebraical variables among the m_Nx variables
	Nixe::Array<int> m_VarDiff; // stores the indices of the differential variables among the m_Nx variables
	Nixe::Array<int> m_EqnsAlg; // stores the indices of the algebraic equations among the m_Nx equations
	Nixe::Array<int> m_EqnsDiff; // stores the indices of the differential equations along the m_Nx equations
	NIXE_DECLSPEC void ComputeJacobian(double* DFX, int* IROW, int* ICOL, int& NFILL, NIXE_SHARED_PTR<Nixe::CompressedColumn> StateJacobian);
	double m_CurrentTime;
}; 
#endif
