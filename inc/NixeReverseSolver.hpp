#ifndef _NIXE_REVERSE_SOLVER_INCLUDE_
#define _NIXE_REVERSE_SOLVER_INCLUDE_

#include "NixeUtil.hpp"
#include "NixeReverseExtrapolationTab.hpp"
#include "NixeMatrix.hpp"
#include "NixeDaeInterface.hpp"
#include "NixeDenseColumnMajor.hpp"
#include <cmath>
#include <cfloat>
#include <iostream>
#include <cstdlib>
#include "NixeAdjointData.hpp"
#include "NixeSharedPtr.hpp"
#include "NixeOptions.hpp"

namespace Nixe {

	template<typename Engine>
	class ReverseSolver {
	public:
		ReverseSolver(NIXE_SHARED_PTR<DaeInterface<typename Engine::TypeInterface> > dae, NIXE_SHARED_PTR<Checkpoints> checkpoints);
		~ReverseSolver();
		Status Solve();
		NIXE_SHARED_PTR<Array<double> > GetDerivatives() const;
		NIXE_SHARED_PTR<DenseMatrix<double> > GetAdjoints() const;
		NIXE_SHARED_PTR<Array<double> > GetStates() const;
		NIXE_SHARED_PTR<DenseMatrix<double> > GetSens() const;
		NIXE_SHARED_PTR<Checkpoints> GetCheckpoints() {return m_Checkpoints;}
		void OneStep(double time,  double stepSize, int order);
		double Initialize();
	private:

		NIXE_SHARED_PTR<DaeInterface<typename Engine::TypeInterface> > m_Dae;
		NIXE_SHARED_PTR<ProblemInfo> m_ProblemInfo;
		NIXE_SHARED_PTR<Checkpoints> m_Checkpoints;


		// Mass Matrix M, Jacobian and right Handside
		Matrix<Engine> *m_MassMatrix;
		Matrix<Engine> *m_Jacobian;

		double *m_RightHandSide;
		double *m_SensRightHandSide;
		double *m_RhsStatesDt;


		double *m_CurrentState;
		double *m_Parameters;
		double *m_CurrentSens;
		double *m_CurrentAdjoints;
		double *m_CurrentDers;

		double m_CurrentTime;



	  ReverseExtrapolationTab<Engine> *m_ExtrapolationTab;

	};


  template<typename Engine>
	ReverseSolver<Engine>::~ReverseSolver() {
		delete	 m_ExtrapolationTab;
    delete[] m_CurrentDers ;
		delete[] m_CurrentAdjoints;
		delete[] m_CurrentSens;
		delete[] m_Parameters;
		delete[] m_CurrentState;

		delete[] m_RhsStatesDt;
		delete[] m_SensRightHandSide;
		delete[] m_RightHandSide;
		delete	 m_Jacobian;
		delete   m_MassMatrix;		
	}

	template <typename Engine>
		ReverseSolver<Engine>::ReverseSolver(NIXE_SHARED_PTR<DaeInterface<typename Engine::TypeInterface> > dae, NIXE_SHARED_PTR<Checkpoints> checkpoints) :
		m_Dae(dae),
		m_Checkpoints(checkpoints)

	{
			m_ProblemInfo=dae->GetProblemInfo();
			const int numSens= m_ProblemInfo->NumSaveSens();
			const int numStates= m_ProblemInfo->NumStates();
			const int numAdjoints =m_ProblemInfo->NumAdjoints();
			const int numParameters=m_ProblemInfo->NumParams();
			const int numDers=m_ProblemInfo->NumDers();
			m_ProblemInfo->NumNonZerosMass();
			m_ProblemInfo->SetIsForwardSweep(false);
			m_ExtrapolationTab = new ReverseExtrapolationTab<Engine>(*m_Dae,*m_ProblemInfo,*m_Checkpoints);



			// storage for function evaluations
			m_MassMatrix = new Matrix<Engine>(numStates,numStates,m_ProblemInfo->NumNonZerosMass());
			m_Jacobian =   new Matrix<Engine>(numStates,numStates,m_ProblemInfo->NumNonZerosJac());
			m_RightHandSide = new double[numStates];
			m_SensRightHandSide = new double[Max(numSens*numStates,1)];
			for (int i=0; i<Max(numSens*numStates,1);i++)
				m_SensRightHandSide[i]=0.0;
			m_CurrentState = new double[numStates];
			m_Parameters = new double[Max(numParameters,1)];
			m_CurrentSens = new double[Max(numSens*numStates,1)];
			m_CurrentAdjoints = new double[numStates*numAdjoints];
			m_CurrentDers = new double[Max(numDers,1)];
			VectorCopy(numDers,m_CurrentDers,0.0);
			// Extrapolation Table, setting of default sequence
		
			m_RhsStatesDt = new double[numStates];
			VectorCopy(numStates,m_RhsStatesDt,0.0);


	}
	template<typename Engine>
	NIXE_SHARED_PTR<Array<double> > ReverseSolver<Engine>::GetDerivatives() const {
		const int numDers=m_ProblemInfo->NumDers();
		NIXE_SHARED_PTR<Array<double> > temp(new Array<double>(numDers));
		VectorCopy(numDers,temp->Data(),m_CurrentDers);
		return temp;
	}

	template<typename Engine>
	NIXE_SHARED_PTR<DenseMatrix<double> > ReverseSolver<Engine>::GetAdjoints() const {
		const int numStates   = m_ProblemInfo->NumStates();
		const int numAdjoints = m_ProblemInfo->NumAdjoints();
		NIXE_SHARED_PTR<DenseMatrix<double> > temp(new DenseMatrix<double>(numStates,numAdjoints));
		VectorCopy(numStates*numAdjoints,temp->Data(),m_CurrentAdjoints);
		return temp;
	}


	/* this routine computes constistent final values for differntial adjoints by means
	of an adhoc extrapolation, it returns the scaledError, if the scaledError is below 1, the initialization was
	successfull */

	template<typename Engine>
	double  ReverseSolver<Engine>::Initialize() {

		const int top=m_Checkpoints->top;
		const int numStates=m_ProblemInfo->NumStates();
		const int numSens = m_ProblemInfo->NumSaveSens();
		const int numSensStates = numSens*numStates;
		const int numDers    = m_ProblemInfo->NumDers();
		const int numAdjoints = m_ProblemInfo->NumAdjoints();
		const int numStatesAdjoints= numStates*numAdjoints;
		
		int index=top;

		m_CurrentTime= m_Checkpoints->storage[index]->startTime;

		VectorCopy(numStates,m_CurrentState,m_Checkpoints->storage[index]->states);
		VectorCopy(numSensStates,m_CurrentSens,m_Checkpoints->storage[index]->sens);
		m_Dae->EvalMass(m_MassMatrix->Interface());
		m_Dae->EvalParameters(m_Parameters,*m_ProblemInfo);
		m_Dae->EvalFinalValues(m_CurrentTime, m_CurrentState, m_Parameters, 
			m_CurrentSens, m_CurrentAdjoints, *m_ProblemInfo);

		// Eval state jacobian
		const bool evalJac=true;
		const double smallH=1e-14;

		// eval Jacobian
		m_Dae->EvalForward(evalJac,m_CurrentTime,m_CurrentState,m_Parameters,
						m_RightHandSide,m_CurrentSens,m_Jacobian->Interface(),
						m_RhsStatesDt,m_SensRightHandSide,*m_ProblemInfo);

		const int maxOrder=4;

		NIXE_SHARED_PTR<StepSequence> stepSequence(new StepSequence(m_Checkpoints->typeSequence,maxOrder));

		Nixe::RowTable rowTable(stepSequence,numStatesAdjoints);

		DMatrix solution(numStates,numAdjoints,0.0);
		DMatrix rhs(numStates,numAdjoints,0.0);
		DMatrix rhsAdjoints(numStates,numAdjoints,0.0);
		Array<double> rhsDers(m_ProblemInfo->NumDers());
		VectorCopy(numStatesAdjoints,rhs.Data(),m_CurrentAdjoints);

		



		std::unique_ptr<typename Engine::TypeLuReuse> reuseInfo(new typename Engine::TypeLuReuse(numStates));
		for (int i=0; i<maxOrder; i++) {
			VectorCopy(numStatesAdjoints,m_CurrentAdjoints,rhs.Data());
			double hjInvers=(double) (*stepSequence)[i] / smallH;
			std::unique_ptr<Matrix<Engine> >  iterMatrix(new Matrix<Engine>(hjInvers,*m_MassMatrix,-1.0,*m_Jacobian));
			bool success=false;
			bool transpose=true;
			(*iterMatrix).Solve(solution,rhs,reuseInfo.get(),&success,transpose);
			if(!success) {
				std::cerr << "Reverse solver intialization failed due to linear algebra" << std::endl;
				exit(1);
			}
			m_Dae->EvalReverse(m_CurrentTime,m_CurrentState,m_Parameters,m_CurrentSens,solution.Data(),
				rhsAdjoints.Data(),rhsDers.Data(),*m_ProblemInfo);
			VectorPlusEqual(numStatesAdjoints,m_CurrentAdjoints,rhsAdjoints.Data());
			rowTable.InsertValues(i,m_CurrentAdjoints);
			if(i >=1) {
				rowTable.Extrapolate(i);
			}
		}
		
		Nixe::Array<double> absTol(numStatesAdjoints,1e-8);
		Nixe::Array<double> relTol(numStatesAdjoints,1e-8);
		Nixe::Array<double> scal(numStatesAdjoints,0);

		rowTable.GetExtrapolatedValues(m_CurrentAdjoints);

		DoScaling(numStatesAdjoints,scal.Data(),m_CurrentAdjoints,absTol.Data(),relTol.Data());

		double scaledError = rowTable.ComputeError(scal.Data());
		
		return scaledError;

	}


	template<typename Engine>
	void ReverseSolver<Engine>::OneStep(double time,  double stepSize, int order){

		const bool evalJac=true;
		const int numStates=m_ProblemInfo->NumStates();
		const int numAdjoints = m_ProblemInfo->NumAdjoints();
		const int numDers     = m_ProblemInfo->NumDers();
		const int numAdjointsStates = numAdjoints*numStates;

		m_Dae->EvalForward(evalJac,m_CurrentTime,m_CurrentState,m_Parameters,
						m_RightHandSide,m_CurrentSens,m_Jacobian->Interface(),
						m_RhsStatesDt,m_SensRightHandSide,*m_ProblemInfo);

    
    m_ExtrapolationTab->ResetOrder();

    for (int i=0; i<order; i++){
			m_ExtrapolationTab->IncrementOrder(m_MassMatrix,m_Jacobian,m_RhsStatesDt,m_RightHandSide,
				m_SensRightHandSide,m_CurrentState,m_Parameters,m_CurrentSens,this->m_CurrentAdjoints,
				this->m_CurrentDers,time,time+stepSize,stepSize);
    }

    m_ExtrapolationTab->GetAdjoints(numAdjointsStates,m_CurrentAdjoints);
    m_ExtrapolationTab->GetDerivatives(numDers,m_CurrentDers);
	} // end OneStep

	
	

	template<typename Engine>
	Status ReverseSolver<Engine>::Solve() {
		Status status= Stat_Error;
		const int top=m_Checkpoints->top;
		const int numStates=m_ProblemInfo->NumStates();
		const int numSens = m_ProblemInfo->NumSaveSens();
		const int numSensStates = numSens*numStates;
		const int numDers    = m_ProblemInfo->NumDers();
		int index=top;

		m_CurrentTime= m_Checkpoints->storage[index]->startTime;

		VectorCopy(numStates,m_CurrentState,m_Checkpoints->storage[index]->states);
		VectorCopy(numSensStates,m_CurrentSens,m_Checkpoints->storage[index]->sens);
		m_Dae->EvalMass(m_MassMatrix->Interface());
		m_Dae->EvalParameters(m_Parameters,*m_ProblemInfo);
		m_Dae->EvalFinalValues(m_CurrentTime, m_CurrentState, m_Parameters, 
			m_CurrentSens, m_CurrentAdjoints, *m_ProblemInfo);



		for (int index=top-1; index>=0; index--){
			m_CurrentTime = m_Checkpoints->storage[index]->startTime;
	//		std::cout << "Reversing from " << m_Checkpoints->storage[index+1]->startTime;
			double stepSize = m_Checkpoints->storage[index+1]->startTime - m_CurrentTime;

			VectorCopy(numStates,m_CurrentState,m_Checkpoints->storage[index]->states);
			VectorCopy(numSensStates,m_CurrentSens,m_Checkpoints->storage[index]->sens);

			int order = m_Checkpoints->storage[index]->order;

			OneStep(m_CurrentTime,stepSize,order);
//			std::cout << "  to " << m_Checkpoints->storage[index]->startTime << std::endl;
		}



		status = Stat_Success;
		return status;
	}
	template<typename Engine>
	NIXE_SHARED_PTR<Array<double> > ReverseSolver<Engine>::GetStates() const {
		const int numStates=m_ProblemInfo->NumStates();
		NIXE_SHARED_PTR<Array<double> > temp(new Array<double>(numStates));
		VectorCopy(numStates,temp->Data(),m_CurrentState);
		return temp;
	}

	template<typename Engine>
	NIXE_SHARED_PTR<DenseMatrix<double> > ReverseSolver<Engine>::GetSens() const {
		const int numStates=m_ProblemInfo->NumStates();
		const int numSens  =m_ProblemInfo->NumSens();
		NIXE_SHARED_PTR<DenseMatrix<double> > temp(new DenseMatrix<double>(numStates,numSens));
		VectorCopy(numStates*numSens,temp->Data(),m_CurrentSens);
		return temp;
	}

} //namespace Nixe









#endif
