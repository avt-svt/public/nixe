#ifndef _NIXE_OPTIONS_INCLUDE_ /* allow multiple inclusions */
#define _NIXE_OPTIONS_INCLUDE_

#include "NixeUtil.hpp"
#include "NixeSharedPtr.hpp"

namespace Nixe {
	typedef enum {Seq_Harmonic, Seq_ShiftedHarmonic, Seq_Bulirsch, Seq_ShiftedBulirsch} TypeSequences;
		/* Seq_Harmonic:			1,2,3,4,5,6,7,8,9,10,...
	   Seq_ShiftedHarmonic:		2,3,4,5,6,7,8,9,10,11,...
	   Seq_Bulirsch:			1,2,3,4,6,8,12,16,24,32,48,...
	   Seq_ShiftedBulirsch:		2,3,4,6,8,12,16,24,32,48,64,...
	*/

	class Options {
	public:
		Options() {*this=Options(12,Seq_ShiftedHarmonic,1e-9,1e-9,0.1,1e-6,false,true);}
		Options(int maxOrder, TypeSequences stepSequence,double absTolScalar,
			double relTolScalar,double initialStepSize,double tolSwitch,
			bool forceLocationChange, bool computeCIV) : 
			m_MaxOrder(maxOrder),
			m_StepSequence(stepSequence),
			m_AbsTolIsScalar(true),
			m_RelTolIsScalar(true),
			m_AbsTolScalar(absTolScalar),
			m_RelTolScalar(relTolScalar),
			m_InitialStepSize(initialStepSize),
			m_TolSwitch(tolSwitch),
			m_ForceLocationChange(forceLocationChange),
			m_ComputeCIV(computeCIV),
            m_ToleranceNLEQ1S(1e-10),
            m_MaxStepSize(1e20)
			{}
			int MaxOrder() const {return m_MaxOrder;}
			TypeSequences TypeSequence() const  {return m_StepSequence;}
		bool AbsTolIsScalar() const {return m_AbsTolIsScalar;}
		bool RelTolIsScalar() const {return m_RelTolIsScalar;}
		bool ComputeCIV()  const {return m_ComputeCIV;}
		void SetComputeCIV (bool computeCIV) {m_ComputeCIV=computeCIV;}
		void SetAbsTolScalar(double absTolScalar) { m_AbsTolScalar=absTolScalar;}
		void SetRelTolScalar(double relTolScalar) { m_RelTolScalar=relTolScalar;}
		double AbsTolScalar() const {return m_AbsTolScalar;}
		double RelTolScalar() const {return m_RelTolScalar;}
        double MaxStepSize() const {return m_MaxStepSize;}
        void SetMaxStepSize(const double size) { m_MaxStepSize = size;}
		void SetInitialStepSize(double initialStepSize) {m_InitialStepSize =initialStepSize;}
		double InitialStepSize() const {return m_InitialStepSize;}
		int LambdaDenseOutput () const {return 0;}
		void SetTypeSequence(TypeSequences sequence) {m_StepSequence=sequence;}

		void SetAbsTolVec(NIXE_SHARED_PTR<Nixe::Array<double> > absTolVec){
			m_AbsTolVec=absTolVec;
			m_AbsTolIsScalar=false;
		};
		void SetRelTolVec(NIXE_SHARED_PTR<Nixe::Array<double> > relTolVec){
			m_RelTolVec=relTolVec;
			m_RelTolIsScalar=false;
		};
		NIXE_SHARED_PTR<Nixe::Array<double> > GetAbsTolVec() {
			return m_AbsTolVec;
		}
		NIXE_SHARED_PTR<Nixe::Array<double> > GetRelTolVec() {
			return m_RelTolVec;
		}
		void SetTolSwitch(const double tolSwitch) {m_TolSwitch=tolSwitch;}
		double TolSwitch() const {return m_TolSwitch;}
		bool ForceLocationChange() const {return m_ForceLocationChange;}
		void SetForceLocationChange(const bool forceLocationChange) {m_ForceLocationChange=forceLocationChange;}
		void SetToleranceNLEQ1S(const double tol) {m_ToleranceNLEQ1S=tol;}
		const double GetToleranceNLEQ1S() const {return m_ToleranceNLEQ1S;}
	private:
		int m_MaxOrder; /* this is the maximal number of columns of the extrapolation tableau. 
		Typically this number lies between 5 and 12 */

		TypeSequences m_StepSequence; /* this choice sets the sequence of the extrapolation, options are harmonic (LIMEX), the 
									  shifted harmonic sequence starting from 2, the Bulirsch and shifted Bulirsch sequence, the default
									  sequence is the shifted harmonic one*/ 

		bool m_AbsTolIsScalar; // this flag is true if the absolute tolerance is a scalar, otherwise (vector) it is false) 
		bool m_RelTolIsScalar;  // this flag is true if the relative tolerance is a scalar, otherwise (vector) it is false) 
		double m_AbsTolScalar;  // absolute tolerance as scalar
		double m_RelTolScalar;  // relative tolerance as scalar
		double m_InitialStepSize;  // initial stepsize
		NIXE_SHARED_PTR<Nixe::Array<double> > m_AbsTolVec; //  absolute tolerance as a vector
		NIXE_SHARED_PTR<Nixe::Array<double> > m_RelTolVec; //  relative tolerance as a vector
		double m_TolSwitch;  // tolerance for root location of switching function
		bool m_ForceLocationChange; // if a switching point is detected, this options forces a sign change in the switching function
		bool m_ComputeCIV; // decide, whether to compute consistent intial values
		double m_ToleranceNLEQ1S;
        double m_MaxStepSize;
	} ;

} // end namespace Nixe



#endif
