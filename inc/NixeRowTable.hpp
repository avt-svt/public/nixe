#ifndef _NIXE_ROW_TABLE_INCLUDE_
#define _NIXE_ROW_TABLE_INCLUDE_

#include "NixeStepSequence.hpp"
#include "NixeSharedPtr.hpp"
#include "NixeUtil.hpp"

namespace Nixe {

	class RowTable {
	public:
		NIXE_DECLSPEC RowTable(NIXE_SHARED_PTR<StepSequence> stepSeq,int numValues); 
		RowTable() {}
		NIXE_DECLSPEC ~RowTable();
		void InsertValues(int rowIndex, double *values) {
			VectorCopy(m_NumValues,m_T[rowIndex],values);
		};
		NIXE_DECLSPEC void Extrapolate(int rowIndex);
		NIXE_DECLSPEC double ComputeError(double *scal);
		void GetExtrapolatedValues(double *values){
			VectorCopy(m_NumValues,values,m_T[0]);
		}
	private:
		const NIXE_SHARED_PTR<StepSequence> m_StepSequence;
		int m_NumValues;
		double **m_T;
	};




}


#endif
