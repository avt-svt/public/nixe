#ifndef __NIXE_NLEQ1S_WRAPPER_HPP__
#define __NIXE_NLEQ1S_WRAPPER_HPP__

#include <memory>
#include "NixeUtil.hpp"
#include "NixeConfig.hpp"

namespace Nixe {

	class EquationSystem {
	public:
		virtual void Func(const int& N,const double *X, double *FX, int& IFAIL) = 0;
		virtual void Jacobian(bool isFirstCall,const int& N,
			const double *X,double *DFX,int *IROW,int *ICOL,int& NFILL,int& IFAIL)=0;
		virtual void InitialGuess(const int N, double *X0) {VectorCopy(N,X0,0.1);} 
		virtual void Scaling (const int N, double *XSCAL) {VectorCopy(N,XSCAL,0.0);} 
		virtual int GetDimension()=0;
		NIXE_DECLSPEC virtual void SetOptions(Array<int>& IOPT);


	};

	class Nleq1sWrapper {
	public:
		NIXE_DECLSPEC Nleq1sWrapper(EquationSystem *equationSystem); 		
		NIXE_DECLSPEC ~Nleq1sWrapper();
		NIXE_DECLSPEC void Solve();
		NIXE_DECLSPEC void Print();
		void GetSolution(double* solution) {VectorCopy(m_N,solution,m_Solution.Data());}
		bool SuccesfulSolved(){return m_Success;}
		void SetTolerance(const double tol) {m_Tolerance=tol;}
		const double GetTolerance() const {return m_Tolerance;}
	private:
		EquationSystem *m_EquationSystem;
		int m_N;
		int m_Nfill;
		Array<double> m_Solution;
		Array<int> m_IOPT;
		bool m_Success;
		double m_Tolerance;

	};



}








#endif