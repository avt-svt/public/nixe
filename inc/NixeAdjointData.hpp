#ifndef _NIXE_ADJOINTDATA_INCLUDE_ /* allow multiple inclusions */
#define _NIXE_ADJOINTDATA_INCLUDE_ 

#include <vector>
#include <fstream>
#include <iostream>
#include <iomanip>
#include <ios>
#include <string>

#include "NixeUtil.hpp"
#include "NixeOptions.hpp"

namespace Nixe {
	class Storage {
	public:
		double startTime;
		double stepSize;
		int order;
		int numStates;
		int numSens;
		double *states;
		double *sens;
	public:
		Storage (int nStates, int nSens) : startTime(0), stepSize(0), order(0), numStates(nStates), 
			numSens(nSens), states(new double [nStates]), sens(new double [Max(1,nStates*nSens)]){};
		~Storage () { delete[] states; delete[] sens; }

	};

	class Checkpoints {
	public:
		TypeSequences typeSequence;
		int numStates;
		int numSens;
		int top;
		std::vector<Storage*> storage;
	public:
		int MaxOrder() const {
			int maxOrder=0;
			for (int i=0; i<= top; i++)
				maxOrder=Max(maxOrder,storage[i]->order);
			return maxOrder;
		}
        int NumCheckPoints() const {
            return (int) storage.size();
        }
		Checkpoints (int nStates, int nSens) : 
			        numStates(nStates), numSens(nSens),
						top(-1),storage(0) {}
				~Checkpoints() {
					for (std::size_t  i=storage.size(); i>0; i--)
						delete storage[i-1];
				}
				void PrintStatesToFile(const std::string& fileName){
					std::ofstream outFile(fileName.c_str());
					outFile.setf(std::ios_base::scientific, std::ios_base::floatfield);
					for (int i=0; i<=top; i++){
						outFile << storage[i]->startTime;
						for (int j=0; j < numStates; j++)
							outFile << "  " << storage[i]->states[j];
						outFile << std::endl;
					} 
					outFile.close();
				}

	};




} // namespace Nixe



#endif
