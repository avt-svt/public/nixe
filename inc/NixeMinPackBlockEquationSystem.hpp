/*
 * NixeMinPackBlockEquationSystem.hpp
 *
 *  Created on: 08.10.2010
 *      Author: Moritz Schmitz
 */
#ifndef __MY_NIXE_MINPACK_BLOC_EQUATION_SYSTEM_HPP__
#define __MY_NIXE_MINPACK_BLOC_EQUATION_SYSTEM_HPP__

#include "NixeDaeInterface.hpp"
#include "NixeUtil.hpp"
#include "NixeProblemInfo.hpp"
#include "NixeCompressedColumn.hpp"
#include "NixeConfig.hpp"
#include <memory>
#include "cs.h"

namespace Nixe {

	class MinPackBlockEquationSystem{
	public:
	NIXE_DECLSPEC ~MinPackBlockEquationSystem();
	NIXE_DECLSPEC MinPackBlockEquationSystem(DaeInterface<CCSInterface> *dae, const Nixe::ProblemInfo *dae_problemInfo, const std::vector<int>& sparsityPattern,
									   int numVars, int *eqIndices, int *varIndices, double *tempStates, int nonZerosReducedJac);
	NIXE_DECLSPEC void FuncJac(const int *nvar,const  double x[], double f[], double jac[],
				  const int *ldjac, int *userflag, void *comm);
	NIXE_DECLSPEC void InitialGuess(const int N, double *X0);
	int GetDimension(){return m_numVars;};
	int GetVarIndices(int inum){return m_VarIndices[inum];};
	private:
		DaeInterface<CCSInterface> *myDae;
		NIXE_SHARED_PTR<Nixe::ProblemInfo> m_Dae_problemInfo;
		int m_Nx; // number of total states of dae-system
		int m_numVars; // number of actual variables and equations treated by bloc system
		int m_nonZerosReducedJac;
		Nixe::Array<double> m_X0;
		Nixe::Array<int> m_EqIndices;
		Nixe::Array<int> m_VarIndices;
   		Nixe::Array<double> m_States;
		Nixe::Array<int> m_SparsityPattern;
		double m_CurrentTime;
	};

	class MinPackWrapper {
		public:
			NIXE_DECLSPEC MinPackWrapper(MinPackBlockEquationSystem *equationSystem); 		
			NIXE_DECLSPEC ~MinPackWrapper();
			NIXE_DECLSPEC void Solve();
			NIXE_DECLSPEC void Print();
			void GetSolution(double* solution) {VectorCopy(m_N,solution,m_Solution.Data());}
			bool SuccesfulSolved(){return m_Success;}
		private:
			MinPackBlockEquationSystem *m_EquationSystem;
			int m_N;
			Array<double> m_Solution;
			bool m_Success;
	};

}
#endif
