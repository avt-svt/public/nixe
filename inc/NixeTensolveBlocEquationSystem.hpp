/*
 * MyBlocEquationSystemTENSOLVE.hpp
 *
 *  Created on: 22.09.2010
 *      Author: Moritz Schmitz and Ralf Hannemann
 */
#ifndef __MY_NIXE_TENSOLVE_BLOC_EQUATION_SYSTEM_HPP__
#define __MY_NIXE_TENSOLVE_BLOC_EQUATION_SYSTEM_HPP__

#include "NixeTensolveWrapper.hpp"
#include "NixeDaeInterface.hpp"
#include "NixeUtil.hpp"
#include "NixeProblemInfo.hpp"
#include "NixeCompressedColumn.hpp"
#include "NixeConfig.hpp"
#include <memory>
#include "cs.h"

using namespace Nixe;


class MyBlocEquationSystemTENSOLVE : public EquationSystemTENSOLVE {
public:
	NIXE_DECLSPEC ~MyBlocEquationSystemTENSOLVE();
	NIXE_DECLSPEC MyBlocEquationSystemTENSOLVE(DaeInterface<CCSInterface> *dae, const Nixe::ProblemInfo *dae_problemInfo,
		int numVars, int *eqIndices, int *varIndices, double *tempStates, int nonZerosReducedJac);
	int MyBlocEquationSystemTENSOLVE::GetVarIndices(int inum){return m_VarIndices[inum];};
	int MyBlocEquationSystemTENSOLVE::GetDimension(){return m_numVars;};
	NIXE_DECLSPEC void InitialGuess(const int N, double *X0);
	/*--------------------------------------------------
	Calculates the Right-Hand-Side in X
	--------------------------------------------------*/
	NIXE_DECLSPEC void MyBlocEquationSystemTENSOLVE::Func(const double *X, double *FX, const int& M, const int& N);
	/*--------------------------------------------------------------------------
	Calculates the Jacobian of the Rhs with respect to X
	--------------------------------------------------------------------------*/
	NIXE_DECLSPEC void MyBlocEquationSystemTENSOLVE::Jacobian(const double *X, double *JAC, const int& MAXM, const int& M, const int& N);
	
private:
	DaeInterface<CCSInterface> *myDae;
	NIXE_SHARED_PTR<Nixe::ProblemInfo> m_Dae_problemInfo;
	int m_Nx; // number of total states of dae-system
	int m_numVars; // number of actual variables and equations treated by bloc system
	int m_nonZerosReducedJac;
	Nixe::Array<double> m_X0;
	Nixe::Array<double> m_Parameters;
	Nixe::Array<int> m_EqIndices;
	Nixe::Array<int> m_VarIndices;
	Nixe::Array<double> m_Rhs;
	Nixe::Array<double> m_RhsSens;
	Nixe::Array<double> m_Sens;
	Nixe::Array<double> m_RhsStatesDtime; // dpartial f/ dpartial t
   	Nixe::Array<double> m_States;
	double m_CurrentTime;
}; 
#endif