#ifndef _NIXE_DENSE_COLUMN_MAJOR_INCLUDE_
#define _NIXE_DENSE_COLUMN_MAJOR_INCLUDE_

#include "NixeDCMInterface.hpp"
#include "NixeLapackLu.hpp"
#include <iostream>
#include "NixeConfig.hpp"

namespace Nixe {

  class DenseColumnMajor : public DCMInterface {
  public:
    typedef DCMInterface  TypeInterface;
    typedef LapackLuReuse TypeLuReuse;
    NIXE_DECLSPEC DenseColumnMajor (int m, int n, int nnz);
    NIXE_DECLSPEC DenseColumnMajor (const double alpha, const DenseColumnMajor& A,
	const double beta,const DenseColumnMajor& B);
    NIXE_DECLSPEC ~DenseColumnMajor ();
    NIXE_DECLSPEC void Add(double *result, const int mm, const int nn);
    NIXE_DECLSPEC void Multiply(const double coeff, double *result, const double *toMult, const int m, const int n);
    NIXE_DECLSPEC void TransposeMultiply(const double coeff, double *result, const double *toMult, const int m, const int n);
    void Solve(DMatrix& solution,const DMatrix& rhs,
    		LapackLuReuse * reuse, bool *success, bool transpose) {
    	if(m_Lu) delete m_Lu;
      m_Lu = new LapackLu(this);
      m_Lu->Solve(solution,rhs,reuse,success,transpose);
    }
    void SolveAgain(DMatrix& solution, const DMatrix& rhs,bool transpose) {
    	m_Lu->SolveAgain(solution,rhs,transpose);
    }
    TypeInterface* Interface() {return this;}
  private:
    LapackLu *m_Lu;
  };

    class FastDenseColumnMajor : public DCMInterface {
  public:
    typedef DCMInterface  TypeInterface;
    typedef LapackLuReuse TypeLuReuse;
    NIXE_DECLSPEC FastDenseColumnMajor (int m, int n, int nnz);
    NIXE_DECLSPEC FastDenseColumnMajor (const double alpha, const FastDenseColumnMajor& A,
	const double beta,const FastDenseColumnMajor& B);
    NIXE_DECLSPEC ~FastDenseColumnMajor ();
    NIXE_DECLSPEC void Add(double *result, const int mm, const int nn);
    NIXE_DECLSPEC void Multiply(const double coeff, double *result, const double *toMult, const int m, const int n);
    NIXE_DECLSPEC void TransposeMultiply(const double coeff, double *result, const double *toMult, const int m, const int n);
    void Solve(DMatrix& solution,const DMatrix& rhs,
    		LapackLuReuse * reuse, bool *success, bool transpose) {
    	if(m_Lu) delete m_Lu;
      m_Lu = new LapackLu2(this);
      m_Lu->Solve(solution,rhs,reuse,success,transpose);
    }
    void SolveAgain(DMatrix& solution, const DMatrix& rhs,bool transpose) {
    	m_Lu->SolveAgain(solution,rhs,transpose);
    }
    TypeInterface* Interface() {return this;}
  private:
    LapackLu2 *m_Lu;
  };

  


} //namespace Nixe

#endif
