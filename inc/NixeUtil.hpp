#ifndef _NIXE_UTIL_INCLUDE_
#define _NIXE_UTIL_INCLUDE_

#include "NixeConfig.hpp"
#include <fstream>
#include <vector>
#include <cmath>
#include <iostream>



namespace Nixe {

inline void DoScaling(int n,  double *scal, double* y, double *absTol, double *relTol){
			for (int i=0; i < n; i++)
				scal[i] = absTol[i]+relTol[i]*fabs(y[i]);
		};


NIXE_DECLSPEC double timer();

typedef enum  {Stat_Success, Stat_Error, Stat_RootFound} Status;


inline double Max(double a, double b){
	return (a>b)?a:b;
}
inline double Min(double a, double b){
	return (a<b)?a:b;
}

inline int	Min(int a, int b) {return (a<b)?a:b;}

inline int Max(int a, int b) { return (a>b)?a:b ;}


inline	void VectorCopy(int n, int * NIXE_RESTRICT dest, const int * NIXE_RESTRICT source){
	for (int i=0; i < n; i++)
		dest[i]=source[i];
}


inline void VectorCopy(int n, std::vector<double>& dest, const double* NIXE_RESTRICT source){
	for (int i=0; i < n; i++)
		dest[i]=source[i];
}
	
NIXE_DECLSPEC void VectorCopy(int n, double * NIXE_RESTRICT dest, const double* NIXE_RESTRICT source);



NIXE_DECLSPEC void VectorCopy(int n, double* NIXE_RESTRICT dest, double source);

NIXE_DECLSPEC void VectorPlusEqual(int n, double* NIXE_RESTRICT dest, double* NIXE_RESTRICT toAdd);

class Faculty{
public:
	Faculty(int maxArg) : m_MaxArg(maxArg), m_Store(new double[maxArg+1]) {
		m_Store[0]=1;
		for (int i=1; i<=m_MaxArg; i++)
			m_Store[i]=m_Store[i-1]*i;
	}
	~Faculty() { delete[] m_Store;}
	double operator() (int arg) {return m_Store[arg];}
private:
//	Faculty();
//	Faculty(const Faculty& rhs);
	int m_MaxArg;
	double *m_Store;
};

template<typename T>
class Array{
public:
  Array(const int size) : m_Data(new T[Max(1,size)]), m_Size(size) {}
  Array(const int size, T value) : m_Data(new T[Max(1,size)]), m_Size(size) {
	  for(int i=0; i<m_Size; i++){
			m_Data[i] = value;
	  }}
  Array(const Array& rhs) : m_Data(new T[rhs.m_Size]), m_Size(rhs.m_Size){
    for (int i=0; i< m_Size; ++i)
      m_Data[i]=rhs.m_Data[i];
  }
  Array& operator=(const Array& rhs) {
    for(int i=0; i<m_Size; i++)
      m_Data[i] = rhs.m_Data[i];
	return *this;
  }
  T* Data() {return m_Data;}
  int Size() const {return m_Size;}
  T& operator[](const int i) {return m_Data[i];}
  const T operator[] (const int i) const {return m_Data[i];}
  ~Array() {delete[] m_Data;}
  void PrintToFile(const char* filename=0){
		if(filename){
			std::ofstream of(filename);
			of.precision(15);
			for(int i=0; i < Size(); i++){
				of << std::scientific << (*this)[i] << std::endl;
			}
			of.close();
		}
		else{
			std::cout.precision(15);
			for(int i=0; i < Size(); i++){
				std::cout << std::scientific << (*this)[i] << std::endl;
			}
		}

	}
private:
  Array();
  T* m_Data;
  int m_Size;
};

template<>
NIXE_DECLSPEC Array<double>::Array(const int size);
template<>
NIXE_DECLSPEC Array<double>::Array(const int size, double value);
template<>
NIXE_DECLSPEC Array<double>::Array(const Array& rhs);
template<>
NIXE_DECLSPEC Array<double>::~Array();


template<>
NIXE_DECLSPEC Array<int>::Array(const int size);
template<>
NIXE_DECLSPEC Array<int>::Array(const int size, int value);
template<>
NIXE_DECLSPEC Array<int>::Array(const Array& rhs);
template<>
NIXE_DECLSPEC Array<int>::~Array();


template<typename T>
class Array2D{
public:
  Array2D(const int size1, const int size2): m_Size1(size1), m_Size2(size2) {
    m_Data = new T*[size1];
		const int mysize2=Max(1,size2);
    for (int i=0; i<size1; i++)
      m_Data[i]= new T[mysize2];
  }

  ~Array2D() { 
    for (int i=m_Size1-1; i>=0; i--)
      delete[] m_Data[i];
    delete[] m_Data;
  }
  int Size1() const {return m_Size1;}
  int Size2() const {return m_Size2;}
  T** Data() {return m_Data;}
	T* operator() (int index) {return m_Data[index];}
	T& operator() (int i, int j) {return m_Data[i][j];}  
  T*  Data(int index) {return m_Data[index];}
private:
  Array2D();
  T** m_Data;
  int m_Size1;
  int m_Size2;
};


template<typename T>
class DenseMatrix{
public:
	 DenseMatrix(const int numRows, const int numCols, T value) : m_Data(new T[Max(numRows*numCols,1)]), m_NumRows(numRows), m_NumCols(numCols) {
	  for(int i=0; i<m_NumRows*m_NumCols; i++)
			m_Data[i] = value;
	  }
	 DenseMatrix(const int numRows, const int numCols) :m_Data(new T[Max(numRows*numCols,1)]), m_NumRows(numRows), m_NumCols(numCols) {}
	 DenseMatrix(const DenseMatrix& rhs) : m_Data(new T[rhs.m_NumRows*rhs.m_NumCols]), m_NumRows(rhs.m_NumRows),
		m_NumCols(rhs.m_NumCols) {
		for (int i=0; i< Size(); ++i)
			m_Data[i]=rhs.m_Data[i];
	}
	 DenseMatrix& operator=(const DenseMatrix& rhs) {
		for(int i=0; i<Size(); i++)
			m_Data[i] = rhs.m_Data[i];
		  return *this;
    }
    ~DenseMatrix() {
		delete[] m_Data;
	}
	T& operator()(const int i, const int j) {return m_Data[i+j*m_NumRows];}
	T* Col(int j) {return &m_Data[j*m_NumRows];}
	const T* Col(int j) const  {return &m_Data[j*m_NumRows];}
	T* Data() {return m_Data;}
	const T* Data() const {return m_Data;} 
	int NumRows() const {return m_NumRows;}
	int NumCols() const {return m_NumCols;}
	int Size() const {return m_NumRows*m_NumCols;}
private:
  DenseMatrix();
  T* m_Data;
  int m_NumRows;
  int m_NumCols;
};


template<>
class DenseMatrix<double> {
public:
	 NIXE_DECLSPEC DenseMatrix(const int numRows, const int numCols, double value) ;
	 NIXE_DECLSPEC DenseMatrix(const int numRows, const int numCols) ;
	 NIXE_DECLSPEC DenseMatrix(const DenseMatrix<double>& rhs) ;
	 NIXE_DECLSPEC DenseMatrix<double>& operator=(const DenseMatrix<double>& rhs) ;
     NIXE_DECLSPEC ~DenseMatrix() ;
	double& operator()(const int i, const int j) {return m_Data[i+j*m_NumRows];}
	double* Col(int j) {return &m_Data[j*m_NumRows];}
	const double* Col(int j) const  {return &m_Data[j*m_NumRows];}
	double* Data() {return m_Data;}
	const double* Data() const {return m_Data;} 
	int NumRows() const {return m_NumRows;}
	int NumCols() const {return m_NumCols;}
	int Size() const {return m_NumRows*m_NumCols;}
	 void PrintToFile(const char* filename=0){
			if(filename){
				std::ofstream of(filename);
				of.precision(15);
				for(int i=0; i < NumRows(); i++){
					for(int j=0; j< NumCols()-1; j++){
						of << std::scientific << (*this)(i,j) << "\t";
					}
					of << std::scientific << (*this)(i,NumCols()-1) <<  std::endl;
				}
				of.close();
			}
			else{
				std::cout.precision(15);
				for(int i=0; i < NumRows(); i++){
					for(int j=0; j< NumCols()-1; j++){
						std::cout << std::scientific << (*this)(i,j) << "\t";
					}
					std::cout << std::scientific << (*this)(i,NumCols()-1) <<  std::endl;
				}
			}

	 }

private:
  DenseMatrix();
  double* m_Data;
  int m_NumRows;
  int m_NumCols;
};

typedef DenseMatrix<double> DMatrix;






  class Statistics {
	public:
		NIXE_DECLSPEC void Initialize(); //sets all variables to zero
		void IncrementNumberRightHandSideCalls(){m_NumberRightHandSideCalls++;}
		void IncrementNumberJacobianCalls(){m_NumberJacobianCalls++;}
		void IncrementNumberDecompositions(){m_NumberDecompositions++;}
		void IncrementNumberBackSubstitutions(){m_NumberBackSubstitutions++;}
		void IncrementNumberSteps(){m_NumberSteps++;}
		void IncrementNumberAcceptedSteps(){m_NumberAcceptedSteps++;}
		void IncrementNumberRejectedSteps(){m_NumberRejectedSteps++;}
		int GetNumberRightHandSideCalls (){ return m_NumberRightHandSideCalls;} // only f 
		int GetNumberJacobianCalls (){return m_NumberJacobianCalls;}
		int GetNumberDecompositions () {return m_NumberDecompositions;} // LU decomposition
		int GetNumberBackSubstitutions () {return m_NumberBackSubstitutions;}
		int GetNumberSteps () {return m_NumberSteps;}
		int GetNumberAcceptedSteps () {return m_NumberAcceptedSteps;}
		int GetNumberRejectedSteps () {return m_NumberRejectedSteps;}

	private:
		int m_NumberRightHandSideCalls; // only f 
		int m_NumberJacobianCalls;
		int m_NumberDecompositions; // LU decomposition
		int m_NumberBackSubstitutions;
		int m_NumberSteps;
		int m_NumberAcceptedSteps;
		int m_NumberRejectedSteps;
	};

	class ParaStepSizeControl {
	public:
		double Safe1;
		double Safe2;
		double Fac1;
		double Fac2;
		double Fac3;
		double Fac4;
		ParaStepSizeControl(){
			Safe1 = 0.6;
			Safe2 = 0.93;
			Fac1=0.1;
			Fac2=4.0;
			Fac3=0.7;
			Fac4=0.9;
		}
	};



}









#endif
