#ifndef _NIXE_REVERSE_EXTRAPOLATION_TAB_INCLUDE_
#define _NIXE_REVERSE_EXTRAPOLATION_TAB_INCLUDE_

#include "NixeConfig.hpp"
#include "NixeMatrix.hpp"
#include "NixeDenseColumnMajor.hpp"
#include "NixeDaeInterface.hpp"
#include "NixeUtil.hpp"
#include "NixeOptions.hpp"
#include "NixeStepSequence.hpp"
#include "NixeRowTable.hpp"
#include "NixeProblemInfo.hpp"
#include "NixeAdjointData.hpp"
#include <iostream>
#include <memory>
#include <vector>

namespace Nixe{

template<typename Engine>
	class ReverseExtrapolationTab {
	public:
		ReverseExtrapolationTab(const DaeInterface<typename Engine::TypeInterface>& dae, 
			const ProblemInfo& problemInfo,
			const Checkpoints& checkpoints);
		~ReverseExtrapolationTab();
		void IncrementOrder(Matrix<Engine> *massMatrix,Matrix<Engine> *jacobian, double *rhsStatesDt, double *rightHandSide, double* sensRightHandSide,
			 double *currentState, double* parameters, double* currentSens, double* 
			finalAdjoints,double* finalDers, double startTime, double finalTime, double stepSize);
		void GetDerivatives(int numDers, double *ders) {
			m_RowTabDers->GetExtrapolatedValues(ders);
		}
		void GetAdjoints(int numAdjointStates,double *adjoints){ 
			m_RowTabAdjoints->GetExtrapolatedValues(adjoints);
		}


		void ResetOrder () {m_Order=0;}
		int  GetCurrentOrder() {return m_Order;}

	private:
		ReverseExtrapolationTab(); // no default constructor
		const DaeInterface<typename Engine::TypeInterface>& m_Dae;
		const ProblemInfo& m_ProblemInfo;
		int m_MaxOrder;
		typename Engine::TypeLuReuse *m_ReuseInfo;
		int m_Order;
		

		NIXE_SHARED_PTR<StepSequence> m_StepSequence;
		RowTable *m_RowTabDers;
		RowTable *m_RowTabAdjoints;

		// for Increment Order
		
	};

template<typename Engine>
ReverseExtrapolationTab<Engine>::ReverseExtrapolationTab(const Nixe::DaeInterface<typename Engine::TypeInterface> &dae, 
																												 const Nixe::ProblemInfo &problemInfo, const Checkpoints& checkpoints) :
m_Dae(dae), m_ProblemInfo(problemInfo)
{
	const int numStates = problemInfo.NumStates();
	const int numAdjoints = problemInfo.NumAdjoints();
	const int numDers = problemInfo.NumDers();
	const TypeSequences typeSeq = checkpoints.typeSequence;
	m_MaxOrder= checkpoints.MaxOrder();
	m_StepSequence= NIXE_SHARED_PTR<StepSequence>(new StepSequence(typeSeq,m_MaxOrder));
	m_RowTabDers = new RowTable(m_StepSequence,numDers);
	m_RowTabAdjoints = new RowTable(m_StepSequence,numStates*numAdjoints);
	m_ReuseInfo = new typename Engine::TypeLuReuse(numStates);

}

template<typename Engine>
ReverseExtrapolationTab<Engine>::~ReverseExtrapolationTab() {
	delete m_RowTabAdjoints;
	delete m_RowTabDers;
	delete m_ReuseInfo;
}


template<typename Engine>
void ReverseExtrapolationTab<Engine>::IncrementOrder(Matrix<Engine> *massMatrix,Matrix<Engine> *jacobian, double *rhsStatesDt, double *rightHandSide, double* sensRightHandSide,
			 double *currentState, double* parameters, double* currentSens, double* 
			 finalAdjoints,double* finalDers, double startTime, double finalTime, double stepSize) {


		++m_Order;
		const int jj=m_Order-1; //C index style
    const int nj=(*m_StepSequence)[jj];
		const int numSens=m_ProblemInfo.NumSaveSens();
		const int numStates=m_ProblemInfo.NumStates();
		const int numAdjoints=m_ProblemInfo.NumAdjoints();
		const int numDers = m_ProblemInfo.NumDers();
		const int numSensStates=numSens*numStates;
		const int numAdjointStates=numAdjoints*numStates;
		const double H = stepSize;
    double hj = H/static_cast<double>(nj);
    double hjInvers = 1/hj;
		Array<double> time(nj+1);
			bool successLU=false;
		 
		time[0] = startTime;
  
   //Array2D<double> storeStates(nj,numStates);
   //Array2D<double> storeSens(nj,numSensStates);
	 DMatrix deltaStates(numStates,1);
	 DMatrix deltaSens(numStates,numSens);
	 std::vector<DMatrix> storeStates(nj,DMatrix(numStates,1,0.0));
	 std::vector<DMatrix> storeSens(nj,DMatrix(numStates,numSens,0.0));
	 DMatrix rhsStates(numStates,1);
	 DMatrix rhsSens(numStates,numSens);




   VectorCopy(numStates,storeStates[0].Data(),currentState);
	 VectorCopy(numSensStates,storeSens[0].Data(),currentSens);
	 /* Forward sweep */
	 // computer Iter matrix
    	std::unique_ptr<Matrix<Engine> >  iterMatrix(new Matrix<Engine>(hjInvers,*massMatrix,-1.0,*jacobian));

	// Solve (M/h-Jacobian) delta = f for the first time
      if (nj > 1) {
   
				VectorCopy(numStates,rhsStates.Data(),rightHandSide);
				// corrector for non autonomous systems
				for (int i=0; i<numStates; i++)
						rhsStates.Data()[i] += hj*rhsStatesDt[i];
					
				VectorCopy(numSensStates,rhsSens.Data(),sensRightHandSide);

				(*iterMatrix).Solve(deltaStates,rhsStates,m_ReuseInfo, &successLU,false);
				(*iterMatrix).SolveAgain(deltaSens,rhsSens,false);

				VectorCopy(numStates,storeStates[1].Data(),storeStates[0].Data());
				VectorCopy(numSensStates,storeSens[1].Data(),storeSens[0].Data());

				VectorPlusEqual(numStates,storeStates[1].Data(),deltaStates.Data());
				VectorPlusEqual(numSensStates,storeSens[1].Data(),deltaSens.Data());


		     for(int ii=1; ii<=nj-2;ii++) {
		    // eval right handside;
    		    time[ii]=startTime+static_cast<double>(ii)*hj;
						m_Dae.EvalForward(false,time[ii],storeStates[ii].Data(),parameters,
							rhsStates.Data(),storeSens[ii].Data(),0,0,rhsSens.Data(),m_ProblemInfo);

						// corrector for non autonomous systems
						for (int i=0; i<numStates; i++)
							rhsStates.Data()[i] += hj*rhsStatesDt[i];

					 (*iterMatrix).SolveAgain(deltaStates,rhsStates,false);
					 (*iterMatrix).SolveAgain(deltaSens,rhsSens,false);
				
						VectorCopy(numStates,storeStates[ii+1].Data(),storeStates[ii].Data());
						VectorCopy(numSensStates,storeSens[ii+1].Data(),storeSens[ii].Data());

						VectorPlusEqual(numStates,storeStates[ii+1].Data(),deltaStates.Data());
						VectorPlusEqual(numSensStates,storeSens[ii+1].Data(),deltaSens.Data());

				 }
			}
	  time[nj-1]=startTime + static_cast<double>(nj-1)*hj;	  
	  time[nj]=startTime + static_cast<double>(nj)*hj;
      
		// end forward sweep
		/* Reverse Sweep */
		
    
    DMatrix rhsAdjoints(numStates,numAdjoints);
		DMatrix deltaAdjoints(numStates,numAdjoints,0.0);

		DMatrix adjoints(numStates,numAdjoints);
		Array<double> ders(numDers);
		Array<double> rhsDers(numDers);


		VectorCopy(numAdjointStates,adjoints.Data(),finalAdjoints);
		VectorCopy(numDers,ders.Data(),finalDers);

	

		for(int ii=nj-1; ii >= 0; ii--){

			
			if (nj==1)
				(*iterMatrix).Solve(deltaAdjoints,adjoints,m_ReuseInfo, &successLU,true);
			else
				(*iterMatrix).SolveAgain(deltaAdjoints,adjoints,true);
			
			m_Dae.EvalReverse(time[ii],storeStates[ii].Data(),parameters,storeSens[ii].Data(),
				deltaAdjoints.Data(),rhsAdjoints.Data(),
				rhsDers.Data(),m_ProblemInfo);

			VectorPlusEqual(numAdjointStates,adjoints.Data(),rhsAdjoints.Data());
			VectorPlusEqual(numDers,ders.Data(),rhsDers.Data());
		}
	
		 m_RowTabAdjoints->InsertValues(jj,adjoints.Data());
		 m_RowTabDers->InsertValues(jj,ders.Data());

		// Polynomial Extrapolation
    	if (m_Order == 1)  return;

			m_RowTabAdjoints->Extrapolate(jj);
			m_RowTabDers->Extrapolate(jj);

	


		return;

}


} // end namespace Nixe

#endif