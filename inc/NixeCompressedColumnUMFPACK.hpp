#ifndef _NIXE_COMPRESSED_COLUMN_UMFPACK_INCLUDE_
#define _NIXE_COMPRESSED_COLUMN_UMFPACK_INCLUDE_

#include "NixeConfig.hpp"
#include "NixeCCSInterface.hpp"
#include "NixeSuperLU.hpp"
#include "umfpack.h"
#include <iostream>

struct cs_sparse;

namespace Nixe {
class UmfpackReuseInfo {
  public:
    NIXE_DECLSPEC UmfpackReuseInfo (int dimension); // Constructor which allocates memory of dimeinsion n and sets IsFirstCall=true 
    NIXE_DECLSPEC ~UmfpackReuseInfo ();
	void SetSymbolic(void* symbolic) {m_Symbolic=symbolic;}
	void GetSymbolic(void*& symbolic) {symbolic=m_Symbolic;} 
    bool GetIsFirstCall() {return m_IsFirstCall;}
    void SetIsFirstCall( bool IsFirstCall) {m_IsFirstCall=IsFirstCall;}
  private:
    bool m_IsAllocatedMemory; // Keep track, whether memory is allocated
    void *m_Symbolic;
    bool m_IsFirstCall;  
  };
  
  class CompressedColumnUMFPACK : public CCSInterface {
  public:
    typedef CCSInterface  		TypeInterface;
    typedef UmfpackReuseInfo 	TypeLuReuse;
//    typedef SuperLU       TypeLinearSolver;
    NIXE_DECLSPEC CompressedColumnUMFPACK(int m, int n, int nnz);
    NIXE_DECLSPEC CompressedColumnUMFPACK(const double alpha, const CompressedColumnUMFPACK& A,
    		const double beta, const CompressedColumnUMFPACK& B);
    NIXE_DECLSPEC ~CompressedColumnUMFPACK();
    NIXE_DECLSPEC void Add(double *result, const int m, const int n);
    NIXE_DECLSPEC void Multiply(const double coeff, double *result,const double *toMult,const int m,const int n);
    NIXE_DECLSPEC void TransposeMultiply(const double coeff, double *result,const double *toMult,const int m,const int n);
    TypeInterface* Interface() {return this; }
    NIXE_DECLSPEC void Solve(DMatrix& solution,const DMatrix& rhs,
    		UmfpackReuseInfo * reuse, bool *success, bool transpose);
    NIXE_DECLSPEC void SolveAgain(DMatrix& solution,const DMatrix& rhs, bool transpose);
	NIXE_DECLSPEC void Transpose();
	NIXE_DECLSPEC int GetMaximalBlockSize() const;
  private:
    struct cs_sparse* m_Imp;
    void *m_Symbolic, *m_Numeric;
  };
} // namespace Nixe
#endif

