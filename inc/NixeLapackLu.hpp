/*
 * NixeLapackLu.hpp
 *
 *  Created on: 17.09.2009
 *      Author: ralf
 */

#ifndef _NIXE_LAPACK_INCLUDE_
#define _NIXE_LAPACK_INCLUDE_

#include "NixeConfig.hpp"
#include "NixeDCMInterface.hpp"
#include "NixeUtil.hpp"

namespace Nixe {
	class LapackLuReuse {
	public:
		LapackLuReuse(int dimension) {};
	};

	class LapackLu {
	public:
		NIXE_DECLSPEC LapackLu(DCMInterface *linearSystem);
		NIXE_DECLSPEC ~LapackLu();
		NIXE_DECLSPEC void Solve(DMatrix& solution, const DMatrix& rhs,
				LapackLuReuse *reuse, bool *success, bool transpose);
		NIXE_DECLSPEC void SolveAgain(DMatrix& solution, const DMatrix& rhs, bool transpose);
	private:
		bool m_IsComputedLU;

		char m_Fact;
		char m_Trans;
		int m_N;
		int m_Nrhs;
		double* NIXE_RESTRICT m_A;
		int m_Lda;
		double * NIXE_RESTRICT m_Af;
		int m_Ldaf;
		int * NIXE_RESTRICT m_Ipiv;
		char m_Equed;
		double *NIXE_RESTRICT m_R;
		double * NIXE_RESTRICT m_C;
		double * NIXE_RESTRICT m_B;
		int m_Ldb;
		double * NIXE_RESTRICT m_X;
		int m_Ldx;
		double m_Rcond;
		double * NIXE_RESTRICT m_Ferr;
		double * NIXE_RESTRICT m_Berr;
		double * NIXE_RESTRICT m_Work;
		int * NIXE_RESTRICT m_Iwork;
		int m_Info;

	};

	class LapackLu2 {
	public:
		NIXE_DECLSPEC LapackLu2(DCMInterface *linearSystem);
		NIXE_DECLSPEC ~LapackLu2();
		NIXE_DECLSPEC void Solve(DMatrix& solution, const DMatrix& rhs,
				LapackLuReuse *reuse, bool *success, bool transpose);
		NIXE_DECLSPEC void SolveAgain(DMatrix& solution, const DMatrix& rhs, bool transpose);
	private:
		bool m_IsComputedLU;

		char m_Trans;
		int m_N;
		int m_Nrhs;
		double* NIXE_RESTRICT m_A;
		int m_Lda;
		int * NIXE_RESTRICT m_Ipiv;
		double * NIXE_RESTRICT m_B;
		int m_Ldb;
		double * NIXE_RESTRICT m_X;
		int m_Ldx;
		int m_Info;

	};

} // namespace Nixe

#endif /* _NIXE_LAPACK_INCLUDE_ */
