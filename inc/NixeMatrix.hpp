#ifndef _NIXE_MATRIX_INCLUDE_
#define _NIXE_MATRIX_INCLUDE_

#include "NixeBlasLapack.hpp"
#include "NixeDCMInterface.hpp"
#include "NixeUtil.hpp"



namespace Nixe {
  template<typename Engine>
  class Matrix{
  public: 
    Matrix(int m, int n, int nnz) : m_Engine(m,n,nnz) {};
    Matrix(const double alpha,const  Matrix<Engine> & A,
	  const double beta,const Matrix<Engine>& B): m_Engine(alpha, A.m_Engine,beta, B.m_Engine) {};
    void Add(double *result, const int mm, const int nn) {m_Engine.Add(result,mm,nn);}
    void Multiply(const double coeff,double* result,const double *toMult,const int m,const int n) {
      m_Engine.Multiply(coeff, result, toMult,m,n);}
    void TransposeMultiply(const double coeff,double* result, const double *toMult,const int m,const int n) {
      m_Engine.TransposeMultiply(coeff, result, toMult,m,n);}
   // void Add(double *result, const int m, const int n) {m_Engine.Add(result,m,n);
    void Solve(DMatrix& solution,const DMatrix& rhs,
			   typename Engine::TypeLuReuse *reuse, bool *success,bool transpose){
					 if (rhs.NumCols() <=0) return;
				m_Engine.Solve(solution, rhs, reuse, success,transpose);
    	}
    void SolveAgain(DMatrix& solution,const DMatrix& rhs, bool transpose) {
			if (rhs.NumCols() <=0) return;
    	m_Engine.SolveAgain(solution, rhs, transpose);
    }
    typename Engine::TypeInterface *Interface() {
      return m_Engine.Interface();}
  private:
    Engine m_Engine;
  };

 

  


} // namespace NIXE

#endif
