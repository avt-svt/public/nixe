/*
 * NixeDCMInterface.hpp
 *
 *  Created on: 18.09.2009
 *      Author: ralf
 */

#ifndef _NIXE_DCM_INTERFACE_HPP_
#define _NIXE_DCM_INTERFACE_HPP_

namespace Nixe {
class DCMInterface {
   // DenseMatrix in column major format
 public:
   int m; //number of rows
   int n; //number of columns
   double *values; // numerical values, size m*n
 };


} //namespace Nixe

#endif /* _NIXE_DCM_INTERFACE_HPP_ */
