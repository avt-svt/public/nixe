#ifndef _NIXE_EXTRAPOLATION_TAB_INCLUDE_
#define _NIXE_EXTRAPOLATION_TAB_INCLUDE_

#include "NixeConfig.hpp"
#include "NixeMatrix.hpp"
#include "NixeDenseColumnMajor.hpp"
#include "NixeDaeInterface.hpp"
#include "NixeUtil.hpp"
#include "NixeOptions.hpp"
#include "NixeStepSequence.hpp"
#include "NixeRowTable.hpp"
#include "NixeDenseOutput.hpp"
#include <iostream>
#include <memory>
#include <vector>
#include <cmath>



namespace Nixe {

	/* ExtrapolationTab: This class provides an interface for computing
	the j-th line of the extrapolation table an provides an estimate 
	of the optimal step size. addtionally a stability check is
	peformed, which checks if a simplified Newton step would 
	converge         */
	template<typename Engine>
	class ExtrapolationTab {
	public:
		ExtrapolationTab(const DaeInterface<typename Engine::TypeInterface>& dae, const ProblemInfo& problemInfo,
			const Options& options, const NIXE_SHARED_PTR<ParaStepSizeControl> stepControl);
		~ExtrapolationTab();
		void IncrementOrder( Nixe::Matrix<Engine> *jacobian, const double *rhsStatesDt,
			double* rightHandSide, double* sensRightHandSide,
			Matrix<Engine> *massMatrix,double currentTime,double* currentState,double *parameters, double* currentSens,
			double* scal, double currentStepSize,
			double maximumStepSize,Statistics *statistic);
		double GetError() {return m_Error;}
		void GetStates(int numStates, double *states) {
			m_RowTabStates->GetExtrapolatedValues(states);
		}
		void GetSens(int numSensStates,double *sens){ 
			m_RowTabSens->GetExtrapolatedValues(sens);
		}
		
		NIXE_SHARED_PTR<StepSequence> GetSequence() {return m_StepSequence;}
		NIXE_SHARED_PTR<Array<double> > GetPoly(const int index, const int maxOrder) {
			return m_DenseOutputVec[index].GetPoly(maxOrder);
		}

		void ResetOrder () {m_Order=0;}
		int  GetCurrentOrder() {return m_Order;}
		Nixe::TypeSequences GetTypeSequence() {return m_StepSequence->GetTypeSequence();}
		double* GetPerformance() {return m_StatePerformance.Data();}
		double* GetOptimalStepSizes() {return m_StateOptimalStepSize.Data();}
		double* GetRowWork() {return m_StateWork.Data();}
		bool IsUnstable () {return m_IsUnstable;}

	private:
		ExtrapolationTab(); // no default constructor
		bool InStabilityCheck(Matrix<Engine>* iterMatrix,Matrix<Engine>* massMatrix, double* scal, double hjInvers, Statistics *statistic);
		typename Engine::TypeLuReuse *m_ReuseInfo;
		double m_ErrorOld;
		double m_Error;
		bool   m_IsUnstable;
		int m_Order;
		int m_MaxOrder;
		const DaeInterface<typename Engine::TypeInterface>& m_Dae;
		const ProblemInfo& m_ProblemInfo;
		const Options& m_Options;
		const NIXE_SHARED_PTR<ParaStepSizeControl>  m_StepControl;

		// for Increment Order

		double * NIXE_RESTRICT m_Wh;
		double * NIXE_RESTRICT m_Yh;
		double * NIXE_RESTRICT m_SensYh;
		DMatrix  m_RhsTmp;
		DMatrix  m_SensRhsTmp;
		DMatrix   m_Rhs;
		DMatrix   m_SensRhs;
		DMatrix   m_Delta;
		DMatrix   m_SensDelta;

		RowTable *m_RowTabStates;
		RowTable *m_RowTabSens;
		NIXE_SHARED_PTR<StepSequence> m_StepSequence;
		Array<double> m_StatePerformance; // performance of row, small values indicate good performance
		Array<double> m_StateWork; // costs per row
		Array<double> m_StateOptimalStepSize; // optimal stepszie of row
		std::vector<DenseOutput> m_DenseOutputVec;
		int m_CounterDenseOutput;
	};


	template< typename Engine>
	ExtrapolationTab<Engine>::ExtrapolationTab(const DaeInterface<typename Engine::TypeInterface>& dae,
		const ProblemInfo& problemInfo, const Options& options, 
	    const NIXE_SHARED_PTR<ParaStepSizeControl> stepControl) : 
	m_Dae(dae), m_ProblemInfo(problemInfo),m_Options(options),
		m_StepControl(stepControl), m_RhsTmp(problemInfo.NumStates(),1), 
		m_SensRhsTmp(problemInfo.NumStates(),problemInfo.NumSens(),0.0),
		m_Rhs(problemInfo.NumStates(),1),
		m_SensRhs(problemInfo.NumStates(),problemInfo.NumSens()),
		m_Delta(problemInfo.NumStates(),1),
		m_SensDelta(problemInfo.NumStates(),problemInfo.NumSens()),
		m_StepSequence(new StepSequence(options.TypeSequence(),options.MaxOrder() ) ),
		m_StatePerformance(options.MaxOrder()), // performance of row, small values indicate good performance
		m_StateWork(options.MaxOrder()), // costs per row
		m_StateOptimalStepSize(options.MaxOrder()), // optimal stepszie of row
		m_DenseOutputVec(problemInfo.NumSwitchFctns(),DenseOutput(m_StepSequence)),
		m_CounterDenseOutput(0)
	{
		const int numStates=problemInfo.NumStates();
		const int numSens=problemInfo.NumSens();
		m_MaxOrder = m_Options.MaxOrder();
		m_ReuseInfo = new typename Engine::TypeLuReuse(numStates);
		m_ErrorOld=0.0;
		m_Error=0.0;
		m_Order = 0;
		m_Yh = new double[numStates];
		m_SensYh = new double[Max(1,numStates*numSens)];
		m_Wh = new double[numStates];
		m_RowTabStates = new RowTable(m_StepSequence,numStates);
		const int numValues=numStates*numSens;
		m_RowTabSens = new RowTable(m_StepSequence,numValues); 
		m_StatePerformance[0]=1e40;
		// Set Work Table
		const double workRhs=1;
		const double workSol=1;
		const double workDec=1;
		const double workJac=5;
		m_StateWork[0]=static_cast<double>((*m_StepSequence)[0])*(workRhs + workSol) + workDec + workJac;
		for (int i=1; i < m_MaxOrder; i++)
			m_StateWork[i] = m_StateWork[i-1]+ static_cast<double>((*m_StepSequence)[i]-1)*(workRhs + workSol) + workDec;
	}

	NIXE_DECLSPEC double StabilityChecksum(int n, double* delta, double* scal);


	template <class Engine>
	ExtrapolationTab<Engine>::~ExtrapolationTab() {
		delete m_RowTabSens;
		delete m_RowTabStates;
		delete m_ReuseInfo;
		delete[] m_Wh;
		delete[] m_SensYh;
		delete[] m_Yh;

	}


template<typename Engine>
bool ExtrapolationTab<Engine>::InStabilityCheck(Matrix<Engine>* iterMatrix,Matrix<Engine>* massMatrix, double* scal, double hjInvers, Statistics *statistic) {
	const int numStates=m_ProblemInfo.NumStates();
	double del1=StabilityChecksum(numStates,m_Delta.Data(),scal);
	VectorCopy(numStates,m_Wh,m_Delta.Data());
	massMatrix->Multiply(0.0,m_Delta.Data(),m_Wh,numStates,1);


	m_Rhs=m_RhsTmp;
	for(int i=0; i<numStates; i++)
		m_Rhs.Data()[i]-=hjInvers * m_Delta.Data()[i];
	iterMatrix->SolveAgain(m_Delta,m_Rhs,false);

	statistic->IncrementNumberBackSubstitutions();
	double del2=StabilityChecksum(numStates,m_Delta.Data(),scal);
	double theta=del2/Nixe::Max(1.0,del1);
	if (theta > 2)
		return true;
	else
		return false;
}


	template<typename Engine>
	void ExtrapolationTab<Engine>::IncrementOrder( Matrix<Engine> *jacobian, const double *rhsStatesDt, 
		double* rightHandSide, double* sensRightHandSide,
		Matrix<Engine> *massMatrix,double currentTime,double* currentState,double *parameters, double* currentSens,
		double* scal, double currentStepSize,
		double maximumStepSize,Statistics *statistic){


			++m_Order;
			if (m_Order > m_MaxOrder)
				std::cout << "Fehler zum Zeitpunkt " << currentTime << std::endl;


			m_IsUnstable=false;

			const int jj=m_Order-1; //C index style
			const int nj=(*m_StepSequence)[jj];
			const double H = currentStepSize;
			double hj = H/static_cast<double>(nj);
			double hjInvers = 1/hj;
			double time=currentTime;



			bool evalJac=false;

			const int numStates=m_ProblemInfo.NumStates();
			const int numSens=m_ProblemInfo.NumSens();
			const int numSwitchFctns=m_ProblemInfo.NumSwitchFctns();

			Array<double> switchFct1(numSwitchFctns);

			if (numSwitchFctns>0 && m_Order == 1) {
				m_CounterDenseOutput=0;
				m_Dae.EvalSwitchingFunction(time,currentState,parameters,switchFct1.Data(),m_ProblemInfo);
				for(int i=0; i<numSwitchFctns;i++){
					m_DenseOutputVec[i].InsertSave(m_CounterDenseOutput,switchFct1[i]);
				}
				++m_CounterDenseOutput;
			}


			if (numSwitchFctns>0 && m_Order == nj) {
				m_Dae.EvalSwitchingFunction(time,currentState,parameters,switchFct1.Data(),m_ProblemInfo);
				for(int i=0; i<numSwitchFctns;i++){
					m_DenseOutputVec[i].InsertSave(m_CounterDenseOutput,switchFct1[i]);
				}
				++m_CounterDenseOutput;
			}
			// computer Iter matrix
			std::unique_ptr<Matrix<Engine> >  iterMatrix(new Matrix<Engine>(hjInvers,*massMatrix,-1.0,*jacobian));


			// Solve (M/h-Jacobian) delta = f for the first time
			VectorCopy(numStates,m_Rhs.Data(),rightHandSide);

			// corrector for non autonomous systems
			for (int i=0; i<numStates; i++)
				m_Rhs.Data()[i] += hj*rhsStatesDt[i];


			bool successLU;
			(*iterMatrix).Solve(m_Delta,m_Rhs,m_ReuseInfo, &successLU,false);
			statistic->IncrementNumberDecompositions();
			statistic->IncrementNumberBackSubstitutions();
			if(!successLU) {
				m_IsUnstable = true;
				return;
			} 

			double *y = currentState;

			VectorCopy(numStates,m_Yh,y);

			if(numSens>0) {
				VectorCopy(numStates*numSens,m_SensRhs.Data(),sensRightHandSide);
				(*iterMatrix).SolveAgain(m_SensDelta,m_SensRhs,false);
				VectorCopy(numStates*numSens,m_SensYh,currentSens);
			}




			if(nj>1) {
				for(int ii=1; ii<nj;ii++){
					VectorPlusEqual(numStates,m_Yh,m_Delta.Data());

					// eval right handside;
					time=currentTime+static_cast<double>(ii)*hj;

					if(numSens>0) {
						VectorPlusEqual(numStates*numSens,m_SensYh,m_SensDelta.Data());
					}
					if (numSwitchFctns>0 && ii>= nj-m_Order) {
						m_Dae.EvalSwitchingFunction(time,m_Yh,parameters,switchFct1.Data(),m_ProblemInfo);
						for(int i=0; i<numSwitchFctns;i++){
							m_DenseOutputVec[i].InsertSave(m_CounterDenseOutput,switchFct1[i]);
						}
						++m_CounterDenseOutput;
					}


					m_Dae.EvalForward(evalJac,time,m_Yh,parameters,m_RhsTmp.Data(),
						m_SensYh,0,0,m_SensRhsTmp.Data(),m_ProblemInfo);
					statistic->IncrementNumberRightHandSideCalls();

					// stabilitiy check
					if (ii==1 && m_Order <= 2) {
						m_IsUnstable=InStabilityCheck(iterMatrix.get(),massMatrix,scal,hjInvers,statistic);
						if(m_IsUnstable)
							return;
					} // end if stability check
					

					// corrector for non autonomous systems
					for (int i=0; i<numStates; i++)
						m_RhsTmp.Data()[i] += hj*rhsStatesDt[i];
					iterMatrix->SolveAgain(m_Delta,m_RhsTmp,false);
					if(numSens>0){
						iterMatrix->SolveAgain(m_SensDelta,m_SensRhsTmp,false);
					}
					statistic->IncrementNumberBackSubstitutions();
				} // end for
			} // end if
			VectorPlusEqual(numStates,m_Yh,m_Delta.Data());
			m_RowTabStates->InsertValues(jj,m_Yh);
			if (numSens>0) {
				VectorPlusEqual(numStates*numSens,m_SensYh,m_SensDelta.Data());
				m_RowTabSens->InsertValues(jj,m_SensYh);
			}

			if (numSwitchFctns>0) {
				m_Dae.EvalSwitchingFunction(time,m_Yh,parameters,switchFct1.Data(),m_ProblemInfo);
				for(int i=0; i<numSwitchFctns;i++){
					m_DenseOutputVec[i].InsertSave(m_CounterDenseOutput,switchFct1[i]);
				}
				++m_CounterDenseOutput;
			}


			// Polynomial Extrapolation
			if (m_Order == 1){
				return;
			}
			m_RowTabStates->Extrapolate(jj);
			if (numSens>0)
				m_RowTabSens->Extrapolate(jj);

			// Computation of Error

			m_Error = m_RowTabStates->ComputeError(scal);
			// Another stability test
			if(m_Error >= 1e15) {
				m_IsUnstable = true;
			}
			// and the third and last stabilitiy test
			if (m_Order > 2 && m_Error >= Max(4.0*m_ErrorOld,1.0)) {
				m_IsUnstable = true;
			}
			m_ErrorOld =  m_Error;

			// Compute optimal step sizes
			double fac1=m_StepControl->Fac1;
			double fac2=m_StepControl->Fac2;
			double safe1=m_StepControl->Safe1;
			double safe2=m_StepControl->Safe2;

			double expo = 1.0 / static_cast<double>(m_Order);
			double facmin = pow( fac1, expo);
			double fac = Min(fac2/facmin,Max(facmin,pow(m_Error/safe1,expo)/safe2));
			fac=1/fac;
			m_StateOptimalStepSize[jj]=Min(fabs(H)*fac,maximumStepSize);
			m_StatePerformance[jj] = m_StateWork[jj] / m_StateOptimalStepSize[jj];

			return;

	}

} //namespace Nixe


#endif
