#ifndef _NIXE_DENSE_OUTPUT_INCLUDE_
#define _NIXE_DENSE_OUTPUT_INCLUDE_

#include "NixeUtil.hpp"
#include "NixeStepSequence.hpp"
#include "NixeSharedPtr.hpp"
#include "NixeConfig.hpp"

namespace Nixe {

	class DenseOutput {
	public:
		NIXE_DECLSPEC ~DenseOutput();
		NIXE_DECLSPEC DenseOutput(NIXE_SHARED_PTR<StepSequence> stepSequence);
		void InsertSave(const int index, const double value) {
			m_Save[index]=value;
		}

		NIXE_DECLSPEC NIXE_SHARED_PTR<Array<double> > GetPoly(const int maxOrder);
		
	private:
		double& Set(const int i, const int j) { return m_Save[(i*(i+1))/2+j];}
		double  Get(const int i, const int j) { return m_Save[(i*(i+1))/2+j];}
		void ComputeDifferences(const int maxOrder);
		void ComputeExtrapolates(const int maxOrder);
		void ComputePolyCoefficients(const int maxOrder);

		NIXE_SHARED_PTR<StepSequence> m_StepSequence;
		int m_Lambda;
		Array<double> m_Save;
		NIXE_SHARED_PTR<Array<double> > m_Poly;

	};

	class Poly{
	public:
	  Poly(const int order) : m_Data(new double[order+1]), m_Order(order) {
		 for(int i=0; i<m_Order; i++){
				m_Data[i] = 0.0;
		 }
	  }

	  Poly(const Poly& rhs) : m_Data(new double[rhs.m_Order+1]), m_Order(rhs.Order()){
		for (int i=0; i<= m_Order; ++i)
		  m_Data[i]=rhs.m_Data[i];
	  }
	  Poly& operator=(const Poly& rhs) {
		for(int i=0; i<=Order(); i++)
		  m_Data[i] = rhs.m_Data[i];
		return *this;
	  }
	  double* Data() {return m_Data;}
	  int Order() const {return m_Order;}
	  double& operator[](const int i) {return m_Data[i];}
	  const double operator[] (const int i) const {return m_Data[i];}
	  ~Poly() {delete[] m_Data;}
	private:
	  Poly();
	  double* m_Data;
	  int m_Order;
	};




} //end namespace Nixe

#endif
