#ifndef _NIXE_FORWARD_SOLVER_INCLUDE_
#define _NIXE_FORWARD_SOLVER_INCLUDE_

#include "NixeUtil.hpp"
#include "NixeExtrapolationTab.hpp"
#include "NixeMatrix.hpp"
#include "NixeDaeInterface.hpp"
#include "NixeDenseColumnMajor.hpp"
#include <cmath>
#include <cfloat>
#include <iostream>
#include "NixeAdjointData.hpp"
#include "NixeOptions.hpp"
#include "NixeZerosPoly.hpp"
#include "NixeComputeCIV.hpp"
#include "NixeSharedPtr.hpp"

namespace Nixe {

	template<typename Engine>
	class ForwardSolver {
	public:
		ForwardSolver(NIXE_SHARED_PTR<DaeInterface<typename Engine::TypeInterface> > dae, NIXE_SHARED_PTR<Options> options);
		~ForwardSolver();
		Status Solve();
		void Initialize();
		NIXE_SHARED_PTR<Array<double> > GetStates() const;
		NIXE_SHARED_PTR<Array<int> > GetIndizesRoots() const;
		NIXE_SHARED_PTR<DenseMatrix<double> > GetSens() const;
		NIXE_SHARED_PTR<Checkpoints> GetCheckpoints() {return m_Checkpoints;}
		void SetStepSequence (TypeSequences stepSequence){m_ExtrapolationTab->SetStepSequence(stepSequence);}
		void SetInitialStepSize(double initialStepSize);
		void OneStep(double *time,  double *stepSize,
			int* guessOrder,bool *success,bool *foundRoot);
		double GetCurrentTime() const {return m_CurrentTime;}
	private:
		ForwardSolver(){}
		void ComputeOptimalOrder();
		void AcceptStep(double *stepSize, bool *isAcceptedThisStep, bool *proceed, bool *foundRoot);
		void RejectStep(double *stepSize, bool *proceed, bool *foundRoot);
		void RejectUnstableStep(double *stepSize, bool *proceed);
		int GuessInitialOrder();


		//Statistics 
		Statistics m_Statistics;

		// Default Step sequence


		double *m_AbsTol, *m_RelTol; // internally always vectors
		double *m_Scal; // scaled states
		NIXE_SHARED_PTR<DaeInterface<typename Engine::TypeInterface> > m_Dae;
		NIXE_SHARED_PTR<Options> m_Options;
		NIXE_SHARED_PTR<ProblemInfo> m_ProblemInfo;
		NIXE_SHARED_PTR<Checkpoints> m_Checkpoints;
		bool m_Save;

		// Data associated with the Extrapolation Tableau


		// Mass Matrix M, Jacobian and right Handside
		Matrix<Engine> *m_MassMatrix;
		Matrix<Engine> *m_Jacobian;

		double *m_RightHandSide;
		double *m_SensRightHandSide;
		double *m_RhsStatesDt;


		double *m_CurrentState;
		double *m_NextState;
		double *m_SwitchingFctns;
		double *m_SwitchingFctnsOld;
		int    *m_IndizesRoots;
		int     m_NumFoundRoots;
		double *m_Parameters;
		double *m_CurrentSens;

		double m_CurrentTime;
		double m_StartTime;
		double m_FinalTime;

		// StepSizes 
		double m_CurrentStepSize;

		int		m_CurrentOrder;
		int		m_OptimalOrder;
		int		m_GuessOrder;


		bool m_IsLastStep; // guess if stopTime is reached in next step
		bool m_IsFirstStep;
		bool m_IsCalledJacRhs;
		bool m_Reject; // keep track whether last step was rejected
		bool m_IsUnstable; //stability check due to Deuflhard 


		// ExtrapolationTab
		ExtrapolationTab<Engine> *m_ExtrapolationTab;

		// StepSize and Order Control
		NIXE_SHARED_PTR<ParaStepSizeControl> m_StepOrderControl;
		double  m_Error;
	};


  template<typename Engine>
	ForwardSolver<Engine>::~ForwardSolver() {
		delete	 m_ExtrapolationTab;
		delete[] m_Scal;
		delete[] m_CurrentSens;
		delete[] m_Parameters;
		delete[] m_IndizesRoots;
		delete[] m_SwitchingFctnsOld;
		delete[] m_SwitchingFctns;
		delete[] m_NextState;
		delete[] m_CurrentState;
		delete[] m_SensRightHandSide;
		delete[] m_RightHandSide;
		delete	 m_Jacobian;
		delete   m_MassMatrix;
		delete[] m_RelTol;
		delete[] m_AbsTol;
		delete[] m_RhsStatesDt;
	}



	template <typename Engine>
	ForwardSolver<Engine>::ForwardSolver(NIXE_SHARED_PTR<DaeInterface<typename Engine::TypeInterface> > dae, 
		NIXE_SHARED_PTR<Options> options) : m_Dae(dae), m_Options(options),m_StepOrderControl(new ParaStepSizeControl){

			m_ProblemInfo=dae->GetProblemInfo();
			const int numSens= m_ProblemInfo->NumSens();
			const int numStates= m_ProblemInfo->NumStates();
			const int numSwitchFctns=m_ProblemInfo->NumSwitchFctns();
			const int numParameters=m_ProblemInfo->NumParams();
			m_ProblemInfo->NumNonZerosMass();

			m_Save = m_ProblemInfo->Save();

			if (m_Save) {
				m_Checkpoints=NIXE_SHARED_PTR<Checkpoints>(new Checkpoints(numStates,m_ProblemInfo->NumSaveSens()));
			}

			// Tolerances, set to default values
			this->m_AbsTol = new double[numStates];
			this->m_RelTol = new double[numStates];
			if (m_Options->AbsTolIsScalar())
				VectorCopy(numStates,m_AbsTol,m_Options->AbsTolScalar());
			else {
				NIXE_SHARED_PTR<Nixe::Array<double> > absTolVec= m_Options->GetAbsTolVec();
				VectorCopy(numStates,m_AbsTol,absTolVec->Data());
			}

			if (m_Options->RelTolIsScalar())
				VectorCopy(numStates,m_RelTol,m_Options->RelTolScalar());
			else {
				NIXE_SHARED_PTR<Nixe::Array<double> > relTolVec= m_Options->GetRelTolVec();
				VectorCopy(numStates,m_RelTol,relTolVec->Data());
			}


			// Step and Order Control
			this->SetInitialStepSize(m_Options->InitialStepSize());

			// storage for function evaluations
			m_MassMatrix = new Matrix<Engine>(numStates,numStates,m_ProblemInfo->NumNonZerosMass());
			m_Jacobian =   new Matrix<Engine>(numStates,numStates,m_ProblemInfo->NumNonZerosJac());
			m_RightHandSide = new double[numStates];
			m_SensRightHandSide = new double[Max(numSens*numStates,1)];
			for (int i=0; i<Max(numSens*numStates,1);i++)
				m_SensRightHandSide[i]=0.0;
			m_CurrentState = new double[numStates];
			m_SwitchingFctns = new double[Nixe::Max(1,numSwitchFctns)];
			m_SwitchingFctnsOld = new double[Nixe::Max(1,numSwitchFctns)];
			m_IndizesRoots   = new int[Nixe::Max(1,numSwitchFctns)];
			for (int i=0; i<numSwitchFctns; i++)
				m_IndizesRoots[i]=-1;
			m_NumFoundRoots=0;
			m_NextState    = new double[numStates];
			m_Parameters = new double[Max(numParameters,1)];
			m_CurrentSens = new double[Max(numSens*numStates,1)];
			m_Scal = new double[numStates];
			// Extrapolation Table, setting of default sequence
			m_ExtrapolationTab = new ExtrapolationTab<Engine>(*m_Dae.get(), *m_ProblemInfo.get(), *m_Options.get(),m_StepOrderControl);

			m_RhsStatesDt = new double[numStates];
			VectorCopy(numStates,m_RhsStatesDt,0.0);

			m_IsFirstStep=true;
			m_Reject = false;

	}
	template<typename Engine>
	NIXE_SHARED_PTR<Array<double> > ForwardSolver<Engine>::GetStates() const {
		const int numStates=m_ProblemInfo->NumStates();
		NIXE_SHARED_PTR<Array<double> > temp(new Array<double>(numStates));
		VectorCopy(numStates,temp->Data(),m_CurrentState);
		return temp;
	}

	template<typename Engine>
	NIXE_SHARED_PTR<Array<int> > ForwardSolver<Engine>::GetIndizesRoots() const {
		NIXE_SHARED_PTR<Array<int> > temp(new Array<int> (m_NumFoundRoots));
		VectorCopy(m_NumFoundRoots,temp->Data(),m_IndizesRoots);
		return temp;
	}

	template<typename Engine>
	NIXE_SHARED_PTR<DenseMatrix<double> > ForwardSolver<Engine>::GetSens() const {
		const int numStates=m_ProblemInfo->NumStates();
		const int numSens  =m_ProblemInfo->NumSens();
		NIXE_SHARED_PTR<DenseMatrix<double> > temp(new DenseMatrix<double>(numStates,numSens));
		VectorCopy(numStates*numSens,temp->Data(),m_CurrentSens);
		return temp;
	}




	template<typename Engine>
	void ForwardSolver<Engine>::SetInitialStepSize(double initialStepSize) {
		m_CurrentStepSize=initialStepSize;
	}

	

	template<typename Engine>
	void ForwardSolver<Engine>::OneStep(double *time,  double *stepSize,
		int* guessOrder, bool *success, bool *foundRoot){
			// INOUT time: on Entry. currentTime, on Exit time after step
			// INOUT stepSize:
			// INOUT guessorder
			// INOUT states
			// OUT success


			double h=*stepSize;
			NIXE_SHARED_PTR<StepSequence> n(m_ExtrapolationTab->GetSequence());


			const int numStates = m_ProblemInfo->NumStates();
			const int numParameters = m_ProblemInfo->NumParams();
			const int numSens       = m_ProblemInfo->NumSens();
			m_GuessOrder = *guessOrder;

			// Keep track, whether step is first or last one
			m_IsLastStep = false;

			// Keep track whethe
			m_IsCalledJacRhs = false;

			m_CurrentTime = *time;
            const double maxStepSize = m_Options->MaxStepSize();
            double hmax = Min(m_FinalTime-m_CurrentTime,maxStepSize);
            h=Min(h,hmax);
			m_Error = 0.0;

			//Scaling
			DoScaling(numStates,m_Scal,m_CurrentState,m_AbsTol,m_RelTol);

			// call mass matrix
			if(m_IsFirstStep) m_Dae->EvalMass(m_MassMatrix->Interface());


			bool isUnstable = false;


			// is stopTime reached in the next step

			if ((m_CurrentTime + 1.01 * h -  m_FinalTime) > 0) {
				h = m_FinalTime- m_CurrentTime;
				m_IsLastStep = true;
			}


			bool isAcceptedThisStep = false;

			while (!isAcceptedThisStep){
				bool proceed = true;
				m_ExtrapolationTab->ResetOrder();
				m_Statistics.IncrementNumberSteps();

				// Eval rhs and Jacobian
				if(!m_IsCalledJacRhs) {

					const bool evalJac=true;
					m_Dae->EvalForward(evalJac,m_CurrentTime,m_CurrentState,m_Parameters,
						m_RightHandSide,m_CurrentSens,m_Jacobian->Interface(),
						m_RhsStatesDt,m_SensRightHandSide,*m_ProblemInfo);


					//			m_Dae->EvalRightHandSide(m_CurrentTime,m_CurrentState,parameters, m_RightHandSide);
					m_Statistics.IncrementNumberRightHandSideCalls();

					//			m_Dae->EvalJacobian(m_CurrentTime,m_CurrentState,parameters,m_Jacobian->Interface());
					m_Statistics.IncrementNumberJacobianCalls();

					m_IsCalledJacRhs=true;
				} // end if !isCalledJacRhs
				for (int j=0; j<  m_GuessOrder-1; j++) {
					m_ExtrapolationTab->IncrementOrder(m_Jacobian,m_RhsStatesDt,m_RightHandSide,m_SensRightHandSide,
						m_MassMatrix,m_CurrentTime,m_CurrentState,m_Parameters,m_CurrentSens,m_Scal,h,hmax,&m_Statistics);

					isUnstable=m_ExtrapolationTab->IsUnstable();
					m_Error = m_ExtrapolationTab->GetError();
					m_CurrentOrder = m_ExtrapolationTab->GetCurrentOrder();
					if(isUnstable){
						RejectUnstableStep(&h, &proceed);
						break;
					}
					if (m_IsFirstStep || m_IsLastStep) {
						if (m_CurrentOrder > 1 && m_Error <= 1.0){
							AcceptStep(&h,&isAcceptedThisStep,&proceed,foundRoot);
							break;
						}
					}
				} // end for


				int k=m_GuessOrder-1;
				if(proceed && (!(m_GuessOrder == 2 || m_Reject)) ) {
					if (m_Error <= 1.0)
						AcceptStep(&h,&isAcceptedThisStep,&proceed,foundRoot);
					// convergence monitor
					else if (m_Error >= (static_cast<double>((*n)[k+1] *  (*n)[k])*4.0))
						RejectStep(&h,&proceed, foundRoot);
					else
						; // Do nothing
				}

				// hope for convergence in line k
				if (proceed) {
					m_ExtrapolationTab->IncrementOrder(m_Jacobian,m_RhsStatesDt,m_RightHandSide,m_SensRightHandSide,m_MassMatrix,m_CurrentTime,
						m_CurrentState,m_Parameters,m_CurrentSens,m_Scal,h,hmax,&m_Statistics);
					isUnstable=m_ExtrapolationTab->IsUnstable();
					m_Error = m_ExtrapolationTab->GetError();
					m_CurrentOrder = m_ExtrapolationTab->GetCurrentOrder();
					if(isUnstable)
						RejectUnstableStep(&h, &proceed);
					if (m_Error <= 1.0)
						AcceptStep(&h,&isAcceptedThisStep,&proceed,foundRoot);
				}


				// hope for convergence in line k+1
				// second convergence monitor
				if (proceed)
					if (m_Error >= static_cast<double>((*n)[m_GuessOrder])*2.0)
						RejectStep(&h, &proceed, foundRoot);

				if (proceed) {
					m_ExtrapolationTab->IncrementOrder(m_Jacobian,m_RhsStatesDt,m_RightHandSide,m_SensRightHandSide,m_MassMatrix,m_CurrentTime,
						m_CurrentState,m_Parameters,m_CurrentSens,m_Scal,h,hmax,&m_Statistics);
					isUnstable=m_ExtrapolationTab->IsUnstable();
					m_Error = m_ExtrapolationTab->GetError();
					m_CurrentOrder = m_ExtrapolationTab->GetCurrentOrder();
					if(isUnstable)
						RejectUnstableStep(&h, &proceed);
				}
				if (proceed) {
					if(m_Error > 1.0)
						RejectStep(&h,&proceed,foundRoot);
					else
						AcceptStep(&h,&isAcceptedThisStep,&proceed,foundRoot);
				} // if proceed
			} // end while

			m_ExtrapolationTab->GetStates(numStates,m_CurrentState);
			if(m_ProblemInfo->NumSens()>0) {
				m_ExtrapolationTab->GetSens(numStates*numSens,m_CurrentSens);
			}
			*stepSize = h;
			*guessOrder = m_GuessOrder;
			*time    = m_CurrentTime;
			if (isAcceptedThisStep) *success = true;
			else *success = false;


	} // end OneStep

	template<typename Engine>
	int ForwardSolver<Engine>::GuessInitialOrder() {
		int initialOrder;
		const int maxOrder=m_Options->MaxOrder();
		initialOrder = Nixe::Max(2,Nixe::Min(maxOrder-2,
#ifdef WIN32
#pragma warning( suppress : 4244 )
#endif
			static_cast<int>(-log10(m_AbsTol[0]+m_RelTol[0])*0.6 + 1.5)));
		return initialOrder;
	}

	template<typename Engine>
	void ForwardSolver<Engine>::ComputeOptimalOrder() {
		int kc=m_CurrentOrder-1; // C index style
		const int maxOrder = m_Options->MaxOrder();
		double *w = m_ExtrapolationTab->GetPerformance();
		if (m_CurrentOrder==2) {
			m_OptimalOrder = Min(3, maxOrder-1);
			if(m_Reject){
				m_OptimalOrder=2;
			}
			return;
		}

		if (m_CurrentOrder <= m_GuessOrder){
			m_OptimalOrder = m_CurrentOrder;
			if (w[kc-1] < w[kc]*m_StepOrderControl->Fac3)
				m_OptimalOrder = m_CurrentOrder - 1;
			if (w[kc] < w[kc-1] *m_StepOrderControl->Fac4)
				m_OptimalOrder = Min(m_CurrentOrder+1,maxOrder-1);
		}
		else{
			m_OptimalOrder = m_CurrentOrder-1;
			if (m_CurrentOrder > 3 && w[kc-2] < w[kc-1] *m_StepOrderControl->Fac3)
				m_OptimalOrder = m_CurrentOrder-2;
			if (w[kc] < w[m_OptimalOrder-1] *m_StepOrderControl->Fac4)
				m_OptimalOrder=Min(m_CurrentOrder,maxOrder-1);
		}
	}
	
	
	template<typename Engine>
	void ForwardSolver<Engine>::AcceptStep(double *stepSize, bool *isAcceptedThisStep, bool *proceed,bool *foundRoot) {

		const int numStates=m_ProblemInfo->NumStates();
		const int numSwitchFctns = m_ProblemInfo->NumSwitchFctns();
		// Test switching function
		double time=m_CurrentTime + *stepSize;
		m_ExtrapolationTab->GetStates(numStates,m_NextState);
		if(numSwitchFctns > 0){
			m_Dae->EvalSwitchingFunction(time,m_NextState,m_Parameters,m_SwitchingFctns,*m_ProblemInfo); 
		}

		const double myeps=m_Options->TolSwitch();
		//check sign change
		if(!(*foundRoot)) {
			m_NumFoundRoots=0; 
			for(int i=0; i<numSwitchFctns; i++){
				if(m_SwitchingFctns[i]*m_SwitchingFctnsOld[i] <= 0) {
					m_IndizesRoots[m_NumFoundRoots]=i;
					++m_NumFoundRoots;
					*foundRoot=true;
				}
			}

		   // always reject first time, roots are found
			if (*foundRoot) {
				RejectStep(stepSize,proceed,foundRoot);
				return;
			}
		}
		if(*foundRoot){
			bool accept=false;
			const bool forceLocCh=m_Options->ForceLocationChange();
			int newNumFoundRoots=0;
			for (int i=0; i< m_NumFoundRoots; i++) {
				// check, whether roots are exactly matched
				if (fabs(m_SwitchingFctns[m_IndizesRoots[i]]) <= myeps){
					bool signChange =(m_SwitchingFctns[m_IndizesRoots[i]]*m_SwitchingFctnsOld[m_IndizesRoots[i]])<0?true:false;
					if(!forceLocCh || (forceLocCh && signChange)) {
						accept=true;
						m_IndizesRoots[newNumFoundRoots]=m_IndizesRoots[i];
						++newNumFoundRoots;
					}
				}
			}
			if (accept) {
				m_NumFoundRoots=newNumFoundRoots;
			}
			else {
				RejectStep(stepSize,proceed,foundRoot);
				return;
			}



		}




		m_Statistics.IncrementNumberAcceptedSteps();
		m_CurrentTime += *stepSize;
		m_CurrentStepSize= *stepSize;
		m_IsFirstStep = false;

		// compute optimal order
		ComputeOptimalOrder();

		double *optimalStepSizes =m_ExtrapolationTab->GetOptimalStepSizes();
		// after a reject step
		*proceed = false;
		*isAcceptedThisStep = true;
		m_IsCalledJacRhs  = false;
		if (m_Reject) {
			m_GuessOrder = Min(m_OptimalOrder,m_CurrentOrder);
			*stepSize=Min(*stepSize,optimalStepSizes[m_GuessOrder-1]);
			m_Reject = false;
			return;
		}
		// COMPUTE STEP SIZE FOR NEXT STEP
		int kc=m_CurrentOrder-1; // C index style
		double *w = m_ExtrapolationTab->GetPerformance();
		double *rowWork = m_ExtrapolationTab->GetRowWork();

		if(m_OptimalOrder <= m_CurrentOrder)
			*stepSize=optimalStepSizes[m_OptimalOrder-1];
		else{
			if (m_CurrentOrder < m_GuessOrder && w[kc] < w[kc-1]*m_StepOrderControl->Fac4)
				*stepSize=optimalStepSizes[kc]*rowWork[m_OptimalOrder]/rowWork[kc];
			else
				*stepSize=optimalStepSizes[kc]*rowWork[m_OptimalOrder-1]/rowWork[kc];

		}
		m_GuessOrder = m_OptimalOrder;
		return;
	}

	template<typename Engine>
	void ForwardSolver<Engine>::RejectStep(double *stepSize, bool *proceed, bool *foundRoot) {

		double stepSizeNew=*stepSize;
		if(*foundRoot) {
			Array<double> s(m_CurrentOrder,1.0);
			s[0]=0;
			
			for (int i=0; i< this->m_NumFoundRoots; i++){
				NIXE_SHARED_PTR<Array<double> > poly=m_ExtrapolationTab->GetPoly(m_IndizesRoots[i],m_CurrentOrder);
				if (m_Options->ForceLocationChange()) {
					int alpha =(m_SwitchingFctnsOld[m_IndizesRoots[i]] < 0)?-1:1;
					(*poly)[0]+=alpha* 0.5*m_Options->TolSwitch();
				}
				const int degreePoly = poly->Size()-1;
				Array<double> re_z(degreePoly,0.0);
				Array<double> im_z(degreePoly,0.0);
				int ifail=0;
				Nixe::ZerosShiftedPoly(degreePoly,poly->Data(),s.Data(),re_z.Data(),im_z.Data(),&ifail);
				for (int i=0; i<degreePoly; i++){
					if(re_z[i]>0 && re_z[i] <=1.001 && fabs(im_z[i]) <1e-12){
						stepSizeNew=Min(stepSizeNew,(*stepSize) * re_z[i]); 
					}
				}
			}
			*stepSize=stepSizeNew;
			m_GuessOrder=m_CurrentOrder;
			m_Reject=true;
			m_Statistics.IncrementNumberRejectedSteps();
			m_IsLastStep=false;
			m_IsFirstStep = false;
			*proceed = false;
			return;
		}

		double *w = m_ExtrapolationTab->GetPerformance();
		double *optimalStepSizes =m_ExtrapolationTab->GetOptimalStepSizes();
		const int maxOrder = m_Options->MaxOrder();
		m_GuessOrder = Min(m_GuessOrder,Min(m_CurrentOrder,maxOrder-1));
		if (m_GuessOrder > 2 && w[m_GuessOrder-2] < w[m_GuessOrder-1] * m_StepOrderControl->Fac3)
			m_GuessOrder--;
		m_Statistics.IncrementNumberRejectedSteps();
		*stepSize=optimalStepSizes[m_GuessOrder-1];
		m_Reject=true;
		m_IsLastStep=false;
		m_IsFirstStep = false;
		*proceed = false;
	}

	template<typename Engine>
	void ForwardSolver<Engine>::RejectUnstableStep(double *stepSize, bool *proceed){
		*stepSize *=0.5;
		m_Reject=true;
		m_Statistics.IncrementNumberRejectedSteps();
		*proceed =false;
	}

	template<typename Engine>
	void ForwardSolver<Engine>::Initialize(){
		m_Dae->EvalParameters(m_Parameters,*m_ProblemInfo);

		double time = m_ProblemInfo->StartTime();
	
		m_Dae->EvalInitialValues(time,m_Parameters,m_CurrentState,m_CurrentSens,*m_ProblemInfo);
		
		ComputeConsistentInitialValues(m_Dae,1,m_CurrentState,m_CurrentSens,m_Options->GetToleranceNLEQ1S());

		if(m_Save){
			m_Checkpoints->typeSequence = this->m_ExtrapolationTab->GetTypeSequence();
			const int numSaveSens=m_ProblemInfo->NumSaveSens();
			const int numStates  =m_ProblemInfo->NumStates();
			Storage *storage;
			storage=new Storage(numStates,numSaveSens);
			storage->startTime=time;
			VectorCopy(numStates,storage->states,m_CurrentState);
			VectorCopy(numStates*numSaveSens,storage->sens,m_CurrentSens);
			storage->stepSize=0;
			storage->order = 0;
			m_Checkpoints->storage.push_back(storage);
			++m_Checkpoints->top;
		}
	}

	template<typename Engine>
	Status ForwardSolver<Engine>::Solve() {
		m_StartTime = m_ProblemInfo->StartTime();
		m_FinalTime = m_ProblemInfo->FinalTime();
		
		double time = m_StartTime;
		double h = m_Options->InitialStepSize();
        if (h > m_Options->MaxStepSize()) h= m_Options->MaxStepSize();
		int guessOrder = GuessInitialOrder();
		bool foundRoot=false;

		m_Statistics.Initialize(); // set to zero



		m_Dae->EvalParameters(m_Parameters,*m_ProblemInfo);
	
		m_Dae->EvalInitialValues(time,m_Parameters,m_CurrentState,m_CurrentSens,*m_ProblemInfo);
		
		if (m_Options->ComputeCIV()){
			ComputeConsistentInitialValues(m_Dae,1,m_CurrentState,m_CurrentSens,m_Options->GetToleranceNLEQ1S());
		}
		const int numSwitchFctns=m_ProblemInfo->NumSwitchFctns();
		if(numSwitchFctns > 0){
			m_Dae->EvalSwitchingFunction(time,m_CurrentState,m_Parameters,m_SwitchingFctnsOld,*m_ProblemInfo);
		}

		const double uround = DBL_EPSILON;

		bool success=false;
		Status status= Stat_Error;

		if(m_Save) m_Checkpoints->typeSequence = this->m_ExtrapolationTab->GetTypeSequence();
		const int numSaveSens=m_ProblemInfo->NumSaveSens();
		const int numStates  =m_ProblemInfo->NumStates();
		Storage *storage;

		while (0.1*(m_FinalTime-time) > fabs(time)*uround) {
			if(m_Save) {
				storage=new Storage(numStates,numSaveSens);
				storage->startTime=time;
				VectorCopy(numStates,storage->states,m_CurrentState);
				VectorCopy(numStates*numSaveSens,storage->sens,m_CurrentSens);
			}
//			std::cout << "Advancing from " << time;
			OneStep(&time, &h,&guessOrder, &success, &foundRoot);
//			std::cout << "  to " << time << std::endl;
			if(!success) break;
			if(m_Save){
				storage->stepSize=m_CurrentStepSize;
				storage->order = this->m_ExtrapolationTab->GetCurrentOrder();
				m_Checkpoints->storage.push_back(storage);
				++m_Checkpoints->top;
			}
			if(foundRoot) break;

		}
		if (success && m_Save) {
				storage=new Storage(numStates,numSaveSens);
				storage->startTime=time;
				storage->order=0;
				storage->stepSize=0;
				VectorCopy(numStates,storage->states,m_CurrentState);
				VectorCopy(numStates*numSaveSens,storage->sens,m_CurrentSens);
				m_Checkpoints->storage.push_back(storage);
				++m_Checkpoints->top;
		}

		if(success) {
			status=Stat_Success;
			if(foundRoot)
				status=Nixe::Stat_RootFound;
		}

		return status;
	}


} //namespace Nixe








#endif
