/*
 * MyBlocEquationSystem.hpp
 *
 *  Created on: 16.08.2010
 *      Author: Moritz Schmitz and Ralf Hannemann
 */
#ifndef __MY_NIXE_NLEQ1S_BLOC_EQUATION_SYSTEM_HPP__
#define __MY_NIXE_NLEQ1S_BLOC_EQUATION_SYSTEM_HPP__

#include "NixeNleq1sWrapper.hpp"
#include "NixeDaeInterface.hpp"
#include "NixeUtil.hpp"
#include "NixeProblemInfo.hpp"
#include "NixeCompressedColumn.hpp"
#include "NixeConfig.hpp"
#include <memory>


using namespace Nixe;


class MyBlocEquationSystem : public EquationSystem {
public:
	NIXE_DECLSPEC ~MyBlocEquationSystem();
	NIXE_DECLSPEC MyBlocEquationSystem(DaeInterface<CCSInterface> *dae, const Nixe::ProblemInfo *dae_problemInfo, const std::vector<int>& sparsityPattern,
		int numVars, int *eqIndices, int *varIndices, double *tempStates, int nonZerosReducedJac);
	int GetDimension(){return m_numVars;};
	int GetVarIndices(int inum){return m_VarIndices[inum];};
	NIXE_DECLSPEC void InitialGuess(const int N, double *X0);
	/*--------------------------------------------------
	Calculates the Right-Hand-Side in X
	--------------------------------------------------*/
	NIXE_DECLSPEC void Func(const int& N,const double *X, double *FX, int& IFAIL);
	/*--------------------------------------------------------------------------
	Calculates the Jacobian of the Rhs with respect to X
	--------------------------------------------------------------------------*/
	NIXE_DECLSPEC void Jacobian(bool isFirstCall,const int& N,
		const double *X,double *DFX,int *IROW,int *ICOL,int& NFILL,int& IFAIL);
	
private:
	DaeInterface<CCSInterface> *myDae;
	NIXE_SHARED_PTR<Nixe::ProblemInfo> m_Dae_problemInfo;
	int m_Nx; // number of total states of dae-system
	int m_numVars; // number of actual variables and equations treated by bloc system
	int m_nonZerosReducedJac;
	Nixe::Array<double> m_X0;
	Nixe::Array<int> m_EqIndices;
	Nixe::Array<int> m_VarIndices;
   	Nixe::Array<double> m_States;
	Nixe::Array<int> m_SparsityPattern;
	double m_CurrentTime;
}; 
#endif
