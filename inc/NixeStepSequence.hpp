#ifndef _NIXE_STEP_SEQUENCE_INCLUDE_
#define _NIXE_STEP_SEQUENCE_INCLUDE_

#include "NixeOptions.hpp"
#include "NixeUtil.hpp"
#include "NixeConfig.hpp"

namespace Nixe {
class StepSequence {
public:
	NIXE_DECLSPEC ~StepSequence();
	NIXE_DECLSPEC StepSequence(const TypeSequences typeSeq,const int maxOrder);
	const TypeSequences GetTypeSequence() const {return m_TypeSequence;}
	const int operator[] (const int i) const  {return m_N[i];}
	const int MaxOrder() const {return m_MaxOrder;}
private:
	TypeSequences m_TypeSequence;
	int *m_N;
	int m_MaxOrder;
};

}


#endif
