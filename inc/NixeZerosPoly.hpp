#ifndef _NIXE_ZEROS_POLY_INCLUDE_ /* allow multiple inclusions */
#define _NIXE_ZEROS_POLY_INCLUDE_

#include "NixeConfig.hpp"

namespace Nixe {

NIXE_DECLSPEC void ZerosShiftedPoly(int n, double a[],double s[], double re_z[], double im_z[], int *ifail);
// Computes all roots of a polynomial with real coefficients
//	 p(X) = a[0] + a[1]*(X-s[0]) + a[2]*(X-s[0])*(X-s[1]) + ... + a[n]*(X-s[0])*(X-s[1])*...*(X-s[n-1])

// n	  : input, the degree of the polynomial. Constraint: n >= 1 
// a[n+1] : input, vector of polynomial coefficients. Constraint:       
//		    a[n] != 0 must hold
// s[n]   : input, vector of shifts
// re_z[n]: output, the real part of the roots
// im_z[n]: output, the imaginary part of the roots
// ifail  : output, negative values indicate errors
//		   =0  :  successful computation of roots
//         =-1 :  wrong input n is not >= 1
//		   =-2 :  wrong input a[n]==0
//	       =-3 :  internal error

}


#endif