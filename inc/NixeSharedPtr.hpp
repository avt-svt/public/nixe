/*
 * NixeSharedPtr.hpp
 *
 *  Created on: 05.06.2012
 *      Author: ralf
 */

#ifndef NIXE_SHARED_PTR_HPP_
#define NIXE_SHARED_PTR_HPP_

#include <memory>


#define NIXE_SHARED_PTR std::shared_ptr
#define NIXE_ENABLE_SHARED_FROM_THIS std::enable_shared_from_this


#endif /* NIXE_SHARED_PTR_HPP_ */
