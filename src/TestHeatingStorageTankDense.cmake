
Set(HST_DENSE_DIR Test/HeatingStorageTankDense)

INCLUDE_DIRECTORIES(${HST_DENSE_DIR})

Set(HST_DENSE_HEADER_FILES  
	${HST_DENSE_DIR}/HeatingStorageTankDense.hpp
	${HST_DENSE_DIR}/dcc_log.hpp)
	
SOURCE_GROUP("Header Files" FILES ${CARACOTSIOS_SPARSE_HEADER_FILES} )

Set(HST_DENSE_SOURCE_FILES 
	${HST_DENSE_DIR}/main.cpp
	${HST_DENSE_DIR}/EvalForward.cpp
	${HST_DENSE_DIR}/EvalReverse.cpp
	${HST_DENSE_DIR}/EvalInitialValues.cpp
	${HST_DENSE_DIR}/EvalFinalValues.cpp
	${HST_DENSE_DIR}/EvalParameters.cpp
	${HST_DENSE_DIR}/GetProblemInfo.cpp
	${HST_DENSE_DIR}/EvalMass.cpp
	${HST_DENSE_DIR}/dcc_log.cpp
	${HST_DENSE_DIR}/icd.cpp
	${HST_DENSE_DIR}/t1_icd.cpp
	${HST_DENSE_DIR}/a1_rhs.cpp
	${HST_DENSE_DIR}/t2_a1_rhs.cpp )
	
add_executable(HeatingStorageTankDense ${HST_DENSE_HEADER_FILES} 
	${HST_DENSE_SOURCE_FILES} )

target_link_libraries(HeatingStorageTankDense Nixe)



if(MSVC)
SET_PROPERTY(TARGET HeatingStorageTankDense PROPERTY LINK_FLAGS_DEBUG  "/NODEFAULTLIB:MSVCRT" )
endif()

ADD_TEST(TestHeatingStorageTankDense ${EXECUTABLE_OUTPUT_PATH}/HeatingStorageTankDense ${CMAKE_CURRENT_SOURCE_DIR}/${HST_DENSE_DIR}/ReferenceHessian.dat)

