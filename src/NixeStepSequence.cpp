#include "NixeStepSequence.hpp"

namespace Nixe {
	StepSequence::~StepSequence() {delete[] m_N;}

	StepSequence::StepSequence(const TypeSequences typeSeq,const int maxOrder) :
		m_TypeSequence(typeSeq), m_N(new int[maxOrder]),m_MaxOrder(maxOrder) 
	{

		const int maxOrd=MaxOrder();
		switch (m_TypeSequence) 
		{
		case Nixe::Seq_Harmonic:
			for (int j=0; j< maxOrd; j++)
				m_N[j]=j+1;
			break;
		case Nixe::Seq_ShiftedHarmonic:
			for (int j=0; j< maxOrd; j++)
				m_N[j]=j+2;
			break;
		case Nixe::Seq_Bulirsch:
			m_N[0]=1;
			m_N[1]=2;
			m_N[2]=3;
			for (int j=3; j< maxOrd; j++)
				m_N[j]=2*m_N[j-2];
			break;
		case Nixe::Seq_ShiftedBulirsch:
			m_N[0]=2;
			m_N[1]=3;
			for (int j=2; j< maxOrd; j++)
				m_N[j]=2*m_N[j-2];
			break;
		default:
			//std::cerr << "Curious Input: StepSequence;
			;
		}

	}

}  // end namespace Nixe