#include "NixeExtrapolationTab.hpp"
#include "NixeMatrix.hpp"
#include "NixeDaeInterface.hpp"
#include <cmath>
#include <iostream>


namespace Nixe {







double StabilityChecksum(int n, double* delta, double* scal){
	double checksum=0;
	for(int i=0; i < n; i++)
		checksum+=(delta[i]/scal[i])*(delta[i]/scal[i]);
	checksum=sqrt(checksum);
	return checksum;
}



}