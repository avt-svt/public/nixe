#include "NixeDaeInterface.hpp"

#include "cs.h"

namespace Nixe {

template<>
void DaeInterface<Nixe::CCSInterface>::EvalStateJac(double time, double* states, int dimStates, int* rows, int* cols, int dim, int* SparsityPattern, int* jacRowInd, int* jacColInd, double* jacVal, int len) {
			// default implementation (inefficient because no use of "block-evaluation" of residual function):
			
			NIXE_SHARED_PTR<Nixe::ProblemInfo> oldProblemInfo=this->GetProblemInfo();
			int dimTotalSystem = dimStates;
			int nnzJac = oldProblemInfo->NumNonZerosJac();; // we take an arbitrary big number because we don't know the number of nonzeros of totalSystem-jacobian
			// we provide TempStateJacobian, because "block-evaluation" of residual function is ESO-dependent
			std::unique_ptr<Nixe::CompressedColumn> TempStateJacobian(new Nixe::CompressedColumn(dimTotalSystem,dimTotalSystem,nnzJac));
			Nixe::ProblemInfo problemInfo;


			problemInfo.SetNumSens(0);
			problemInfo.SetNumSaveSens(0);
			const int n_p = oldProblemInfo->NumParams();
			problemInfo.SetNumParams(n_p);
			Nixe::Array<double> params(n_p);
			this->EvalParameters(params.Data(),*oldProblemInfo);

			problemInfo.SetNumStates(dimTotalSystem);
			Nixe::Array<double> rhsStates(dimTotalSystem);


			this->EvalForward(true, time, states, params.Data(), rhsStates.Data(), 0, TempStateJacobian.get(), 0, 0, problemInfo);
			
			// To use cspare utilities we have to wrap to cs struct
			cs A;
			A.nzmax=TempStateJacobian->nzmax;
			A.m    =TempStateJacobian->m;
			A.n    =TempStateJacobian->n;
			A.p    =TempStateJacobian->pc;
			A.i	   =TempStateJacobian->ir;
			A.x	   =TempStateJacobian->values;
			A.nz   =-1;
				
			// permute the Jacobian to 2 x 2 block structure. the upper left block of dimension length(rows) x length(cols) contains 
			// the "reduced jacobian"

			int Nx = dimStates;
			int complDim = Nx-dim;

			Nixe::Array<int> p(Nx);
			Nixe::Array<int> q(Nx);

			// A(p,q) will contain the 2 x 2 block structure

			Nixe::Array<int> complCols(complDim); // variable indices that are not in cols
			Nixe::Array<int> complRows(complDim); // equation indices that are not in rows

			bool isInCols = false;
			int counter = 0;
			for(int inum=0; inum < Nx; inum++){
				for(int i=0; i<dim; i++){
					if(inum == cols[i])
						isInCols = true;
				}
				if(!isInCols){
					complCols[counter] = inum;
					counter ++;
				}
				isInCols = false;
			}
			bool isInRows = false;
			counter = 0;
			for(int inum=0; inum < Nx; inum++){
				for(int i=0; i<dim; i++){
					if(inum == rows[i])
						isInRows = true;
				}
				if(!isInRows){
					complRows[counter] = inum;
					counter ++;
				}
				isInRows = false;
			}
				
			Nixe::VectorCopy(dim,q.Data(),cols);
			Nixe::VectorCopy(complDim,q.Data()+dim,complCols.Data());

			Nixe::VectorCopy(dim,p.Data(),rows);
			Nixe::VectorCopy(complDim,p.Data()+dim,complRows.Data());

			// the cs_permute required the inverte
			int* pinv= cs_pinv(p.Data(),Nx);

			cs* B= cs_permute(&A,pinv,q.Data(),1);


			// cut derivatives with respect to variables defined in complCols
			B->n=dim;

			
			// convert B to compressed row storage to cut equations defined in complRows
			cs* C = cs_transpose(B,1);

			// cut equations defined in complRows
			C->n=dim;

			// Here, C contains the "reduced" Jacobian in compressed row storage
				
			// convert compressed row storage to coordinate formate
			int nz=0;
			for (int i=0; i< C->n; i++){
				for(int j=C->p[i]; j< C->p[i+1];j++){
					jacRowInd[nz]=i;
					jacColInd[nz]=C->i[nz];
					jacVal[nz] = C->x[nz];
					++nz;
				}
			}

			// just free dynamically allocated csparse stuff
			cs_spfree(C);
			cs_spfree(B);
			cs_free(pinv);
		
}


template<>		
void DaeInterface<Nixe::CCSInterface>::GetStateJacobianStruct(int& nNonZeroDfdx, int* rows, int* cols) {
			// default implementation (inefficient because no use of "block-evaluation" of residual function):
			
			NIXE_SHARED_PTR<Nixe::ProblemInfo> oldProblemInfo=this->GetProblemInfo();
			int nnzJac = oldProblemInfo->NumNonZerosJac();; 
			int dimStates= oldProblemInfo->NumStates();
			int dimTotalSystem = dimStates;
			// we provide TempStateJacobian, because "block-evaluation" of residual function is ESO-dependent
			std::unique_ptr<Nixe::CompressedColumn> TempStateJacobian(new Nixe::CompressedColumn(dimTotalSystem,dimTotalSystem,nnzJac));
			Nixe::ProblemInfo problemInfo;


			problemInfo.SetNumSens(0);
			problemInfo.SetNumSaveSens(0);
			const int n_p = oldProblemInfo->NumParams();
			problemInfo.SetNumParams(n_p);
			Nixe::Array<double> params(n_p);
			this->EvalParameters(params.Data(),*oldProblemInfo);

			problemInfo.SetNumStates(dimTotalSystem);
			Nixe::Array<double> rhsStates(dimTotalSystem);
			
			double time = oldProblemInfo->StartTime();

			Nixe::Array<double> states(dimStates,0);

			this->EvalInitialValues(time,params.Data(),states.Data(),0,problemInfo);


			this->EvalForward(true, time, states.Data(), params.Data(), rhsStates.Data(), 0, TempStateJacobian.get(), 0, 0, problemInfo);
			
			// To use cspare utilities we have to wrap to cs struct
			cs A;
			A.nzmax=TempStateJacobian->nzmax;
			A.m    =TempStateJacobian->m;
			A.n    =TempStateJacobian->n;
			A.p    =TempStateJacobian->pc;
			A.i	   =TempStateJacobian->ir;
			A.x	   =TempStateJacobian->values;
			A.nz   =-1;
				
	
				
			// convert compressed row storage to coordinate formate
			int nz=0;
			for (int i=0; i< A.n; i++){
				for(int j=A.p[i]; j< A.p[i+1];j++){
					rows[nz]=i;
					cols[nz]=A.i[nz];
					++nz;
				}
			}
			nNonZeroDfdx=nz;

}

}; //namespace Nixe
