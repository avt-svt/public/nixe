/*
 * NixeLapackLu.cpp
 *
 *  Created on: 18.09.2009
 *      Author: ralf
 */
#include "NixeBlasLapack.hpp"
#include "NixeLapackLu.hpp"
#include "NixeUtil.hpp"

namespace Nixe {
LapackLu::LapackLu(DCMInterface *linearSystem){
	m_N = linearSystem->m;
	m_Fact = 'N';
	m_Trans = 'N';
	m_A = linearSystem->values;
	m_Lda = linearSystem->m;
	m_Af  = new double[m_N*m_N];
	m_Ldaf = m_N;
	m_Ipiv = new int[m_N];
	m_Equed = 'N';
	m_R = new double[m_N];
	m_C = new double[m_N];
	m_Work = new double[4*m_N];
	m_Iwork = new int[m_N];
	m_IsComputedLU = false;
}

void LapackLu::Solve(DMatrix&  solutionMatrix, const DMatrix& rightHandSideMatrix,
	LapackLuReuse *reuseInfo, bool *success, bool transpose) {
	m_Nrhs = rightHandSideMatrix.NumCols();
	m_B = const_cast<DMatrix&>(rightHandSideMatrix).Data(); // only feasible if m_Equed='N'
	m_Ldb = rightHandSideMatrix.NumRows();
	m_X = solutionMatrix.Data();
	m_Ldx =solutionMatrix.NumRows();
	m_Ferr = new double[m_Nrhs];
	m_Berr = new double[m_Nrhs];

	if (transpose)
		m_Trans='T';
	else
		m_Trans='N';

	dgesvx_(&m_Fact, &m_Trans, &m_N, &m_Nrhs,
			m_A, &m_Lda, m_Af, &m_Ldaf, m_Ipiv,
			&m_Equed, m_R, m_C, m_B, &m_Ldb,
			m_X, &m_Ldx, &m_Rcond, m_Ferr, m_Berr, m_Work,
			m_Iwork, &m_Info );

	if (m_Info == 0 || m_Info==m_N+1)
		*success = true;
	else
		*success = false;

	// Keep track of storage for L and U
	m_IsComputedLU=true;

	delete[] m_Berr;
	delete[] m_Ferr;
}

void LapackLu::SolveAgain(DMatrix&  solutionMatrix, const DMatrix& rightHandSideMatrix,
		bool transpose){
	m_Fact='F';
	m_Nrhs = rightHandSideMatrix.NumCols();
	m_B = const_cast<DMatrix&>(rightHandSideMatrix).Data(); // only feasible if m_Equed='N'
	m_Ldb = rightHandSideMatrix.NumRows();
	m_X = solutionMatrix.Data();
	m_Ldx =solutionMatrix.NumRows();
					m_Ferr = new double[m_Nrhs];
	m_Berr = new double[m_Nrhs];

	if (transpose)
		m_Trans='T';
	else
		m_Trans='N';

	dgesvx_(&m_Fact, &m_Trans, &m_N, &m_Nrhs,
			m_A, &m_Lda, m_Af, &m_Ldaf, m_Ipiv,
			&m_Equed, m_R, m_C, m_B, &m_Ldb,
			m_X, &m_Ldx, &m_Rcond, m_Ferr, m_Berr, m_Work,
			m_Iwork, &m_Info );

	delete[] m_Berr;
	delete[] m_Ferr;
}

LapackLu::~LapackLu(){
	delete[] m_Iwork;
	delete[] m_Work;
	delete[] m_C;
	delete[] m_R;
	delete[] m_Ipiv;
	delete[] m_Af;
}

LapackLu2::LapackLu2(DCMInterface *linearSystem){
	m_N = linearSystem->m;
	m_Trans = 'N';
	m_A = linearSystem->values;
	m_Lda = linearSystem->m;
	m_Ipiv = new int[m_N];
	m_IsComputedLU = false;
}

void LapackLu2::Solve(DMatrix& solutionMatrix, const DMatrix& rightHandSideMatrix,
		LapackLuReuse *reuseInfo, bool *success, bool transpose) {
	m_Nrhs = rightHandSideMatrix.NumCols();
	m_B = const_cast<DMatrix&>(rightHandSideMatrix).Data(); // only feasible if m_Equed='N'
	m_Ldb = rightHandSideMatrix.NumRows();
	m_X = solutionMatrix.Data();
	m_Ldx =solutionMatrix.NumRows();


	if (transpose)
		m_Trans='T';
	else
		m_Trans='N';


	dgetrf_(&m_N,&m_N,m_A,&m_N,m_Ipiv,&m_Info);
	VectorCopy(m_N*m_Nrhs,m_X,m_B);
	dgetrs_(&m_Trans,&m_N,&m_Nrhs,m_A,&m_N,m_Ipiv,m_X,&m_Ldb,&m_Info); 

	if (m_Info == 0 )
		*success = true;
	else
		*success = false;

	// Keep track of storage for L and U
	m_IsComputedLU=true;


}

void checkArguments(char* trans, const int *n, const int *nrhs, const double *a, const int *lda, const int *ipiv, 
					double* b, const int *ldb, int *info){

	char checkTrans=*trans;
	int checkN=*n;
	int checkNrhs=*nrhs;
	int checkLda=*lda;
	DMatrix checkA(checkLda,checkN,17.0);
	VectorCopy(checkLda*checkN,checkA.Data(),a);
	Array<int> checkIpiv(checkN,0);
	VectorCopy(checkN,checkIpiv.Data(),ipiv);
	int checkLdb=*ldb;
	DMatrix checkB(checkLdb,checkNrhs,17.0);
	VectorCopy(checkLdb*checkNrhs,checkB.Data(),b);
	int checkInfo=*info;
	char *ctrans=trans;
	const int *cn=n;
	const int *cnrhs=nrhs;
	const double *ca=a;
	const int *clda=lda;
	const int *cipiv=ipiv;
	double* cb=b;
	const int *cldb=ldb;
	int *cinfo=info;


}


void LapackLu2::SolveAgain(DMatrix& solutionMatrix, const DMatrix& rightHandSideMatrix,
		bool transpose){
	m_Nrhs = rightHandSideMatrix.NumCols();
	//m_B = const_cast<DMatrix&>(rightHandSideMatrix).Data(); // only feasible if m_Equed='N'
	m_Ldb = rightHandSideMatrix.NumRows();
	m_X = solutionMatrix.Data();
	m_Ldx =solutionMatrix.NumRows();


	if (transpose)
		m_Trans='T';
	else
		m_Trans='N';


	VectorCopy(m_N*m_Nrhs,m_X,rightHandSideMatrix.Data());
//	checkArguments(&m_Trans,&m_N,&m_Nrhs,m_A,&m_N,m_Ipiv,m_X,&m_Ldb,&m_Info); 
	dgetrs_(&m_Trans,&m_N,&m_Nrhs,m_A,&m_N,m_Ipiv,m_X,&m_Ldb,&m_Info); 


}

LapackLu2::~LapackLu2(){
	delete[] m_Ipiv;
}


} //namespace Nixe
