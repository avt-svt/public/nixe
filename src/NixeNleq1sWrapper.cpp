#include "NixeUtil.hpp"
#include "NixeNleq1sWrapper.hpp"
#include "NixeCnames.hpp"
#include <iostream>
#include <cstdlib>

//#define FORTRANCALL __stdcall
#define FORTRANCALL

extern "C" void FORTRANCALL NLEQ1S(int&,
			     int&,
			     void (*) (const int&, const double *, double *, int&),
			     void(*)(const int&, const double *, double *, int *, int*, int &, int&),
			     double*,
			     double*,
			     double&,
			     int*,
			     int&,
			     int&,
			     int*,
			     int&,
			     double*);

 
namespace Nixe {
	EquationSystem *g_EquationSystem; // Global Variable

	extern "C" void FuncStatic(const int& N, const double *X, double *FX, int& IFAIL) {
				g_EquationSystem->Func(N,X,FX,IFAIL);
		}
	extern "C"	 void JacobianStatic(const int& N,
			const double *X,double *DFX,int *IROW,int *ICOL,int& NFILL,int& IFAIL){
				g_EquationSystem->Jacobian(false,N,X,DFX,IROW,ICOL,NFILL,IFAIL);
		}
		
	Nleq1sWrapper::Nleq1sWrapper(EquationSystem *equationSystem) : m_EquationSystem(equationSystem),m_N(equationSystem->GetDimension()),
			m_Nfill(0), m_Solution(equationSystem->GetDimension()), m_IOPT(50) { m_Tolerance=1e-13; /* default tolerance */}	
	Nleq1sWrapper::~Nleq1sWrapper() {}


	void Nleq1sWrapper::Solve() {
		m_Success=false;
		Array<double> XSCAL(m_N);
		bool isFirstCall=true;
		int ifail;

		m_EquationSystem->InitialGuess(m_N,m_Solution.Data());

		// Determin m_Nfill = number of nonzeros in Jacobian
		m_EquationSystem->Jacobian(isFirstCall,m_N,m_Solution.Data(),0,0,0,m_Nfill,ifail);
		isFirstCall=false;

		double EPS=1e-14;
		
		int NFMAX=m_Nfill;
		int IERR=-1;
		int NBROY=0;
		int LRWK=6*NFMAX+(12+NBROY)*m_N+68;
		int LIWK=12*NFMAX+11*m_N+62;
		int LI2WK=12*NFMAX+5*m_N;
		Array<double> RW(LRWK,0);
		Array<int> IW(LIWK,0);
		Array<int> IW2(LI2WK,0);

		m_EquationSystem->Scaling(m_N,XSCAL.Data());
		m_EquationSystem->SetOptions(m_IOPT);

		g_EquationSystem=m_EquationSystem;
		NLEQ1S(m_N,NFMAX,&FuncStatic,&JacobianStatic,m_Solution.Data(),
			XSCAL.Data(),EPS,m_IOPT.Data(),IERR,LIWK,IW.Data(),
			LRWK,RW.Data());

		if(IERR == 0) m_Success=true;
	


	}

	void Nleq1sWrapper::Print() {
		std::cout << "Solution vector is" << std::endl;
		for (int i=0; i<m_N; i++)
			std::cout << m_Solution[i] << std::endl;
	}

	void EquationSystem::SetOptions(Nixe::Array<int> &IOPT) {
		for (int i=0; i<IOPT.Size();i++)
			IOPT[i]=0;
		IOPT[37-1]=1; // Same Jacobian pattern


	}

}



void NLEQ1S(int&,
			     int&,
			     void (*) (const int&, const double *, double *, int&),
			     void(*)(const int&, const double *, double *, int *, int*, int &, int&),
			     double*,
			     double*,
			     double&,
			     int*,
			     int&,
			     int&,
			     int*,
			     int&,
			     double*)
{
	std::cout << "This is a dummy stub, NLEQ1S does not really exist in NIXE_without_Fortran" << std::endl;
	exit(1);
	
}
