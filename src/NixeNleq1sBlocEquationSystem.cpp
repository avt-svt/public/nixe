/*
 *
 *  Created on: 16.08.2010
 *      Author: Moritz Schmitz and Ralf Hannemann
 */


#include "NixeNleq1sBlocEquationSystem.hpp"


using namespace Nixe;

MyBlocEquationSystem::~MyBlocEquationSystem() {};
MyBlocEquationSystem::MyBlocEquationSystem(DaeInterface<CCSInterface> *dae, const Nixe::ProblemInfo *dae_problemInfo, const std::vector<int>& sparsityPattern,
								   int numVars, int *eqIndices, int *varIndices, double *tempStates, int nonZerosReducedJac) : 
			m_States(dae_problemInfo->NumStates()),
			myDae(dae),
			m_Nx(dae_problemInfo->NumStates()),
			m_numVars(numVars),
			m_nonZerosReducedJac(nonZerosReducedJac),
			m_EqIndices(numVars),
			m_VarIndices(numVars),
			m_X0(numVars),
			m_SparsityPattern(nonZerosReducedJac){	
		
		VectorCopy(numVars,m_EqIndices.Data(),eqIndices);
		VectorCopy(numVars,m_VarIndices.Data(),varIndices);
		VectorCopy(nonZerosReducedJac, m_SparsityPattern.Data(), &sparsityPattern[0]);
		m_Dae_problemInfo = myDae->GetProblemInfo();
		m_CurrentTime=m_Dae_problemInfo->StartTime();
		VectorCopy(dae_problemInfo->NumStates(), m_States.Data(), tempStates);
		for(int i=0;i<m_numVars;i++){ // save initial values for actual variables because others will be overwritten by tempStates
			m_X0[i] = m_States[m_VarIndices[i]];
		}
		VectorCopy(m_Nx,m_States.Data(),tempStates); // to assign already initialized variables
	};


	void MyBlocEquationSystem::InitialGuess(const int N, double *X0){
		for(int i=0;i<m_numVars;i++){
			X0[i] = m_X0[i];
			m_States[m_VarIndices[i]] = m_X0[i];
		}
	};
	/*--------------------------------------------------
	Calculates the Right-Hand-Side in X
	--------------------------------------------------*/
	void MyBlocEquationSystem::Func(const int& N, const double *X, double *FX, int& IFAIL){
		for(int i=0;i<m_numVars;i++){
			m_States[m_VarIndices[i]]=X[i];
		}
		Nixe::Array<long> EqIndices(m_numVars);
		for(int i=0; i<m_numVars; i++){
			EqIndices[i] = m_EqIndices[i];
		}
		myDae->EvalRhs(m_numVars, EqIndices.Data(), m_CurrentTime, m_States.Data(), m_Nx, FX);
	};	

	/*--------------------------------------------------------------------------
	Calculates the Jacobian of the Rhs with respect to X
	--------------------------------------------------------------------------*/
	void MyBlocEquationSystem::Jacobian(bool isFirstCall,const int& N,
		const double *X,double *DFX,int *IROW,int *ICOL,int& NFILL,int& IFAIL){
		const int nnzJac=m_Dae_problemInfo->NumNonZerosJac();
		if(isFirstCall){
			
			NFILL=m_nonZerosReducedJac;
			return;
		}

		for(int i=0;i<m_numVars;i++){
			m_States[m_VarIndices[i]]=X[i];
		}

		myDae->EvalStateJac(m_CurrentTime, m_States.Data(), m_Nx, m_EqIndices.Data(), m_VarIndices.Data(), m_numVars, m_SparsityPattern.Data(), IROW, ICOL, DFX, m_nonZerosReducedJac);

		for (int i=0; i<m_nonZerosReducedJac; i++){ // Fortran-style indexing
			IROW[i]=IROW[i]+1;
			ICOL[i]=ICOL[i]+1;
		}
	};
