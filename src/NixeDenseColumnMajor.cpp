#include "NixeDenseColumnMajor.hpp"
#include "NixeBlasLapack.hpp"
#include "NixeUtil.hpp" 
#include <iostream>

namespace Nixe {

DenseColumnMajor::DenseColumnMajor (const double alpha,const DenseColumnMajor& A,
		const double beta,const DenseColumnMajor& B) : m_Lu(0)  {
  this->m=A.m;
  this->n=A.n;
  int dim=m*n;
  int inca=1;
  int incb=1;
  int incc =1;
  double *a = A.values;
  double myalpha=alpha;
  double *b = B.values;
  double mybeta= beta;
  this->values=new double[Max(m*n,1)];
  double *c = this->values;

  // c = alpha * a + beta * b;
  dcopy_(&dim,b,&incb,c,&incc);
  dscal_(&dim,&mybeta,c,&incc);
  daxpy_(&dim,&myalpha,a,&inca,c,&incc);


}

void DenseColumnMajor::Add(double *result, const int mm, const int nn) {
  const int dim=m*n;
  for (int i=0; i<dim; i++) 
    result[i]+=this->values[i];
  //double alpha=1;
  //double beta=0;
  //const double *a=this->values;
  //double *b=result;
  //int inca=1;
  //int incb=1;
  //daxpy_(&dim,&alpha,a,&inca,b,&incb);
}

void DenseColumnMajor::Multiply(const double coeff, double *result,const double *toMult,
				     const int mm,const int nn){

  // result = this * toMult + coeff * result
  int lda=m;
  int ldc=m;
  int ldb=mm;
  int k=n;
  double alpha=1.0, beta=coeff;
  char transa='N';
  char transb='N';
  const double * a = values;
  const double * b = toMult;
  double * c = result;
  dgemm_(&transa, &transb, &m, &nn, &k, &alpha, a, 
	 &lda, b, &ldb, &beta, c, &ldc);
}

void DenseColumnMajor::TransposeMultiply(const double coeff, double *result,const  double *toMult,
				      const int mm, const int nn){

  // result = this^T * toMult + coeff * result

  double alpha=1.0, beta=coeff;
  char transa='T';
  char transb='N';
  const double * a = values;
  const double * b = toMult;
  double * c = result;
  
  int my_m = this->n;  
  int my_k = this->m;
  int my_n = nn;
  int lda=this->m;
  int ldb= mm;
  int ldc= my_m;


 

  dgemm_(&transa, &transb, &my_m, &my_n, &my_k, &alpha, a,
	 &lda, 
	 b, 
	 &ldb, 
	 &beta, 
	 c, 
	 &ldc);
}


DenseColumnMajor::DenseColumnMajor(int m, int n, int nnz) : m_Lu(0)
{
    this->m=m;
	this->n=n; 
	values=new double[Max(m*n,1)];
}

DenseColumnMajor::~DenseColumnMajor() {
  delete[] values;
  if (m_Lu) 
	  delete m_Lu;
}

FastDenseColumnMajor::FastDenseColumnMajor (const double alpha,const FastDenseColumnMajor& A,
		const double beta,const FastDenseColumnMajor& B) : m_Lu(0)  {
  this->m=A.m;
  this->n=A.n;
  int dim=m*n;
  int inca=1;
  int incb=1;
  int incc =1;
  double *a = A.values;
  double myalpha=alpha;
  double *b = B.values;
  double mybeta= beta;
  this->values=new double[Max(m*n,1)];
  double *c = this->values;

  // c = alpha * a + beta * b;
  dcopy_(&dim,b,&incb,c,&incc);
  dscal_(&dim,&mybeta,c,&incc);
  daxpy_(&dim,&myalpha,a,&inca,c,&incc);


}

void FastDenseColumnMajor::Add(double *result, const int mm, const int nn) {
  const int dim=m*n;
  for (int i=0; i<dim; i++) 
    result[i]+=this->values[i];
  //double alpha=1;
  //double beta=0;
  //const double *a=this->values;
  //double *b=result;
  //int inca=1;
  //int incb=1;
  //daxpy_(&dim,&alpha,a,&inca,b,&incb);
}

void FastDenseColumnMajor::Multiply(const double coeff, double *result,const double *toMult,
				     const int mm,const int nn){

  // result = this * toMult + coeff * result
  int lda=m;
  int ldc=m;
  int ldb=mm;
  int k=n;
  double alpha=1.0, beta=coeff;
  char transa='N';
  char transb='N';
  const double * a = values;
  const double * b = toMult;
  double * c = result;
  dgemm_(&transa, &transb, &m, &nn, &k, &alpha, a, 
	 &lda, b, &ldb, &beta, c, &ldc);
}

void FastDenseColumnMajor::TransposeMultiply(const double coeff, double *result,const  double *toMult,
				      const int mm, const int nn){

  // result = this^T * toMult + coeff * result

  double alpha=1.0, beta=coeff;
  char transa='T';
  char transb='N';
  const double * a = values;
  const double * b = toMult;
  double * c = result;
  
  int my_m = this->n;  
  int my_k = this->m;
  int my_n = nn;
  int lda=this->m;
  int ldb= mm;
  int ldc= my_m;


 

  dgemm_(&transa, &transb, &my_m, &my_n, &my_k, &alpha, a,
	 &lda, 
	 b, 
	 &ldb, 
	 &beta, 
	 c, 
	 &ldc);
}


FastDenseColumnMajor::FastDenseColumnMajor(int m, int n, int nnz) : m_Lu(0)
{
    this->m=m;
	this->n=n; 
	values=new double[Max(m*n,1)];
}

FastDenseColumnMajor::~FastDenseColumnMajor() {
  delete[] values;
  if (m_Lu) 
	  delete m_Lu;
}
} // namespace Nixe
