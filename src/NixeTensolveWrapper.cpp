#include "NixeUtil.hpp"
#include "NixeTensolveWrapper.hpp"
#include <math.h>
#include <iostream>

#define FORTRANCALL

extern "C" void FORTRANCALL TSNECI(int&, int&, int&, double*, int&, int&,
			     double*, double*, int&, int&, double&, double&,
				 double&, int&, int&, double&, double&, int&,
				 double*, int&, double*, int&, double*, int&,
				 int*, int&, 
			     void(*)(const double *, double *, const int&, const int&),
			     void(*)(const double *, double *, const int&, const int&, const int&),
			     int&,
				 double*, double*, double*, int&);

extern "C" void FORTRANCALL TSDFLT(int&, int&, int&, int&, double&, double&,
								   double&, int&, int&, double&, double&,
								   double*, double*, int&, int&);

namespace Nixe {
	EquationSystemTENSOLVE *g_EquationSystem; // Global Variable

	extern "C" static void FunStatic(const double *X, double *FX, const int& M, const int& N) {
				g_EquationSystem->Func(X,FX,M,N);
		}
	extern "C" static void JacStatic(const double *X, double *JAC, const int& MAXM, const int& M, const int& N){
				g_EquationSystem->Jacobian(X,JAC,MAXM,M,N);
		}
		
	TensolveWrapper::TensolveWrapper(EquationSystemTENSOLVE *equationSystem) : m_EquationSystem(equationSystem),m_N(equationSystem->GetDimension()),
			m_M(equationSystem->GetDimension()), m_Solution(equationSystem->GetDimension(),0) {}	
	TensolveWrapper::~TensolveWrapper() {}


	void TensolveWrapper::Solve() {
		int itnlim, jacflg, method, global, ipr, msg, termcd;
		double gradtl, steptl, ftol, stepmx, dlt;
		int m = m_M;
		int n = m_N;
		double dsqrtN = sqrt((double) m_N);
		int zusatz = 0;
		int isqrtN = (int) dsqrtN;
		if (dsqrtN - isqrtN >=0.5)
			isqrtN = isqrtN + 1;
		// MAXM >= M+N+2, MAXN >= N+2,  MAXP >= NINT(sqrt(N)), where NINT is a function that rounds to the nearest integer
		int maxm = 100; //m_M+m_N+2;
		int maxn = 30; //m_N+2;
		int maxp = 6; //isqrtN;
		// LUNC >= 2*NINT(sqrt(N))+4
		int lunc = 14; //2*isqrtN+4  +zusatz;
		// LNEM >= N+2*NINT(sqrt(N))+11
		int lnem = 51; //m_N + 2*isqrtN+11;
		// LNEN >= 2*NINT(sqrt(N))+9
		int lnen = 19;//2*isqrtN+9;
		// LIN >= 3
		int lin = 3;
		Array<int> iwrkn(maxn*lin,0);
		Array<double> x0(n,0);
		Array<double> fp(m,0); //maxm
		Array<double> gp(n,0); //maxn
		Array<double> typx(n,0); //maxn
		Array<double> typf(m,0); //maxm
		Array<double> wrknen(maxn*lnen,0); // an 2D-Array(k,l) in FORTRAN must be declared as an 1D-Array in C++ with size (k*l)
		Array<double> wrkunc(maxp*lunc,0);
		Array<double> wrknem(maxm*lnem,0);
    
		m_EquationSystem->InitialGuess(m_N,x0.Data());
		Array<double> fx(m,0);
		m_EquationSystem->Func(x0.Data(), fx.Data(), m, n);
		VectorCopy(m_N, m_Solution.Data(), x0.Data());

		// Set default values for the TENSOLVE parameters

		TSDFLT(m, n, itnlim, jacflg, gradtl, steptl,
			   ftol, method, global, stepmx, dlt,
			   typx.Data(), typf.Data(), ipr, msg);

		// Alter some of the parameters

		//gradtl = 1.0e-12;
		ftol   = 1.0e-9;
		steptl = 1.0e-12;
		stepmx = 1.0e9;
		//itnlim = 200; // iteration limit, default = 150
		jacflg = 1; // 1: analytic Jacobian provided
		global = 0; // 1: two-dimensional trust region method, 0: line search method
		msg = 10; // 2: no check of analytic Jacobian against finite difference Jacobian, 
				 // 8: no output printed on screen; 10: 8+2: both options are switched on

		// Scaling:

		if(n>1){ // typx
			for(int i=0; i<n; i++){
				if(abs(x0[i])<1.0 && abs(x0[i])>0.0){
					double val = abs(x0[i]);
					double k=1.0;
					do{
						k = k*10.0;
						val = abs(x0[i])*k;
					}while(val<1.0);
					if(1.0/k != 0)
						typx[i] = 1.0/k;
					else
						typx[i] = 1.0;
				}
				if(abs(x0[i])>=10.0){
					double val = abs(x0[i]);
					double k=1.0;
					do{
						k = k*1e-1;
						val = abs(x0[i])*k;
					}while(val>=10.0);
					if(1.0/k != 0)
						typx[i] = 1.0/k;
					else
						typx[i] = 1.0;
				}
			}
		}
		//if(n>1){ // typf
		//	for(int i=0; i<n; i++){
		//		if(abs(fx[i])<1.0 && abs(fx[i])>0.0){
		//			double val = abs(fx[i]);
		//			double k=1.0;
		//			do{
		//				k = k*10.0;
		//				val = abs(fx[i])*k;
		//			}while(val<1.0);
		//			if(1.0/k != 0)
		//				typf[i] = 1.0/k;
		//			else
		//				typf[i] = 1.0;
		//		}
		//		if(abs(fx[i])>=10.0){
		//			double val = abs(fx[i]);
		//			double k=1.0;
		//			do{
		//				k = k*1e-1;
		//				val = abs(fx[i])*k;
		//			}while(val>=10.0);
		//			if(1.0/k != 0)
		//				typf[i] = 1.0/k;
		//			else
		//				typf[i] = 1.0;
		//		}
		//	}
		//}

		// Call TENSOLVE
		
		g_EquationSystem=m_EquationSystem;
		TSNECI(maxm, maxn, maxp, x0.Data(), m, n,
			   typx.Data(), typf.Data(), itnlim, jacflg, gradtl, steptl, 
			   ftol, method, global, stepmx, dlt, ipr, 
			   wrkunc.Data(), lunc, wrknem.Data(), lnem, wrknen.Data(), lnen,
			   iwrkn.Data(), lin, &FunStatic, &JacStatic, msg, 
			   m_Solution.Data(), fp.Data(), gp.Data(), termcd);
		
		//Rescaling:

		if(n>1){
			for(int i=0; i<n; i++){
				m_Solution[i] = m_Solution[i] * typx[i];
			}
		}

	if(termcd != 0 && termcd != 5) m_Success=true;

	}

	void TensolveWrapper::Print() {
		std::cout << "Solution vector is" << std::endl;
		for (int i=0; i<m_N; i++)
			std::cout << m_Solution[i] << std::endl;
	}


}



