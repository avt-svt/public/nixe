#include "NixeRowTable.hpp"
#include <cmath>


namespace Nixe {

	RowTable::RowTable(NIXE_SHARED_PTR<StepSequence> stepSeq,int numValues) :
		m_StepSequence(stepSeq), m_NumValues(numValues)		
	{
		const int maxOrder=stepSeq->MaxOrder();
		m_T = new double*[maxOrder];
		const int size=Max(numValues,1);
		for (int i=0; i<maxOrder; i++) {
			m_T[i]= new double[size];
		}
	}

	RowTable::~RowTable() {
		const int maxOrder=m_StepSequence->MaxOrder();
		for (int i=maxOrder-1; i>=0; i--) 
			delete[] m_T[i];
		delete[] m_T;
	}


	void RowTable::Extrapolate(int rowIndex) {
		const int jj=rowIndex;
		const StepSequence& n=*m_StepSequence;
		for (int l=jj;l>=1;l--){
			double fac=(static_cast<double>(n[jj]))/(static_cast<double>(n[l-1]))-1.0;
			for  (int i=0; i < m_NumValues; i++) {
				m_T[l-1][i]=m_T[l][i] + (m_T[l][i]-m_T[l-1][i])/fac;
			}
		}
	}

	double RowTable::ComputeError(double *scal) {
		double error=0;
		const double smallNumber=1e-15;
		for (int i=0; i < m_NumValues; i++)
			error+=pow(Nixe::Max(fabs(m_T[0][i]-m_T[1][i])/scal[i],smallNumber),2);
		error=sqrt(error/static_cast<double>(m_NumValues));
		return error;
	}


} // end namespace Nixe