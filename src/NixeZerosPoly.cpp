#include "NixeZerosPoly.hpp"
#include "NixeBlasLapack.hpp"
#include "NixeUtil.hpp"


void Nixe::ZerosShiftedPoly(int n, double a[], double s[], double re_z[], 
								   double im_z[], int *ifail) {
// for description, see header NixeZerosPoly.hpp
	using Nixe::Array;
  using Nixe::DMatrix;

    *ifail=-3; //default error status is internal error (ifail=-3)
//  check inputs
	if(n<1){
		*ifail=-1;
		return;
	}
	if (a[n]==0){
		*ifail=-2;
		return;
	}


//  create companion matrix A
	Array<double>  a0(n);
//  norming polynomial such that the leading cofficient is 1
	for(int i=0; i<n; i++)
		a0[i]=a[i]/a[n];
    DMatrix A(n,n,0);
	for (int i=1; i<n; i++)
		A(i,i-1)=1;
	for(int i=0; i<n; i++)
		A(i,n-1)=-a0[i];
	for(int i=0; i<n; i++)
		A(i,i)+=s[i];


//  balancing matrix A using LAPACK dgebal
	char JOB='B';  // permute and scale
    int N = n;   // dimension of matrix
	int LDA=  N;
	int ILO=0;
	int IHI=0;
	int INFO=-17;
	Array<double> SCALE(N);

	//LAPACK routine
	dgebal_(&JOB,&N,A.Data(),&LDA,&ILO,&IHI,SCALE.Data(),&INFO);
	if (INFO != 0){
		*ifail=-3;
		return;
	}


	//transform A to upper Hessenberg form H using LAPACK dgehrd
	Array<double> TAU(N-1);
	int LWORK=N;
	Array<double> WORK(LWORK);
	INFO=-17;

	// LAPACK
	dgehrd_(&N,&ILO,&IHI,A.Data(),&LDA,TAU.Data(),WORK.Data(),&LWORK,&INFO);
		if (INFO != 0){
		*ifail=-3;
		return;
	}

	// compute Eigenvalues of A by LAPACK dhseqr

	JOB='E';
    char COMPZ='N';
	DMatrix& H=A;
	int LDH=LDA;
	double *WR=re_z;
	double *WI=im_z;

	// since JOB='E' Z is a dummy argument
    int LDZ=1;
	Array<double> Z(LDZ);

	INFO=-17;

	//LAPACK routine
	dhseqr_(&JOB,&COMPZ,&N,&ILO,&IHI,H.Data(),&LDH,re_z,im_z,Z.Data(),&LDZ,WORK.Data(),&LWORK,&INFO);
	if (INFO != 0){
		*ifail=-3;
		return;
	}

	
	*ifail=0; //computation is successful ifail=0
	return;
}