/*
 *
 *  Created on: 08.01.2010
 *      Author: Moritz Schmitz and Ralf Hannemann
 */


#include "NixeNleq1sEquationSystem.hpp"
#include "cs.h"

using namespace Nixe;

MyEquationSystem::~MyEquationSystem() {}
MyEquationSystem::MyEquationSystem(DaeInterface<CCSInterface> *dae, const Nixe::ProblemInfo *dae_problemInfo) : myDae(dae), m_Parameters(dae_problemInfo->NumParams()), m_States(dae_problemInfo->NumStates()),
			m_Sens(dae_problemInfo->NumStates() * dae_problemInfo->NumSens()), m_Rhs(dae_problemInfo->NumStates()), 
			m_RhsSens(dae_problemInfo->NumStates() * dae_problemInfo->NumSens()), m_RhsStatesDtime(dae_problemInfo->NumStates()),
			m_Nx(dae_problemInfo->NumStates()), m_Nu(dae_problemInfo->NumSens()), 
			m_VarAlg(dae_problemInfo->NumStates()), 
			m_VarDiff(dae_problemInfo->NumStates()),
			m_EqnsAlg(dae_problemInfo->NumStates()),
			m_EqnsDiff(dae_problemInfo->NumStates()),
			m_NonZeroJacAlg(0), m_NonZeroJacDiff(0){
		
		m_Ndiff = 0;
		m_Nalg = 0;
		
		m_Dae_problemInfo = myDae->GetProblemInfo();
		m_CurrentTime=m_Dae_problemInfo->StartTime();
		FindAlgVar();
		FindAlgEqns();
		myDae->EvalParameters(m_Parameters.Data(), *dae_problemInfo);
		myDae->EvalInitialValues(m_CurrentTime, m_Parameters.Data(), m_States.Data(), m_Sens.Data(), *dae_problemInfo);
};
	void MyEquationSystem::SetStates(double* states){
		VectorCopy(m_Nx, m_States.Data(), states);
	};


	void MyEquationSystem::InitialGuess(const int N, double *X0){
		for(int i=0;i<m_Nalg;i++){
			X0[i] = m_States[m_VarAlg[i]];
		}
	};
	/*--------------------------------------------------
	Calculates the Right-Hand-Side in X
	--------------------------------------------------*/
	void MyEquationSystem::Func(const int& N, const double *X, double *FX, int& IFAIL){
		for(int i=0;i<m_Nalg;i++){
			m_States[m_VarAlg[i]]=X[i];
		}
		myDae->EvalForward(false, m_CurrentTime, m_States.Data(), m_Parameters.Data(), m_Rhs.Data(), m_Sens.Data(), (0), 
			m_RhsStatesDtime.Data(), m_RhsSens.Data(), *m_Dae_problemInfo.get());
		for(int i=0; i<m_Nalg; i++){
			FX[i] = m_Rhs[m_EqnsAlg[i]];
		}
	};	

	/*--------------------------------------------------------------------------
	Calculates the Jacobian of the Rhs with respect to X
	--------------------------------------------------------------------------*/
	void MyEquationSystem::Jacobian(bool isFirstCall,const int& N,
		const double *X,double *DFX,int *IROW,int *ICOL,int& NFILL,int& IFAIL){
		const int nnzJac=m_Dae_problemInfo->NumNonZerosJac();
		for(int i=0;i<m_Nalg;i++){
			m_States[m_VarAlg[i]]=X[i];
		}
		if(isFirstCall){
			
			NFILL=nnzJac;
			return;
		}

		
		NIXE_SHARED_PTR<Nixe::CompressedColumn> StateJacobian (new Nixe::CompressedColumn(m_Nx,m_Nx,nnzJac));
		myDae->EvalForward(true, m_CurrentTime, m_States.Data(), m_Parameters.Data(), m_Rhs.Data(), m_Sens.Data(), StateJacobian.get(), 
			m_RhsStatesDtime.Data(), m_RhsSens.Data(), *m_Dae_problemInfo.get());
		
		bool entry_exists = false;
		int entry_pos = 0;
		int nonzeroJacAlg = 0;
		int nonzeroJacDiff = 0;

		if(N==-1){ // output is dF^a/dx^d (a:algebraical, d:differential)
			for(int j=0; j<m_Ndiff; j++){
				for(int i=0; i<m_Nalg; i++){
					entry_exists = false;
					entry_pos = 0;
					for(int k = StateJacobian->pc[m_VarDiff[j]]; k < StateJacobian->pc[m_VarDiff[j]+1]; k++){ // test if dFi/dxj is different from 0
						if(StateJacobian->ir[k] == m_VarAlg[i]){
							entry_exists = true;
							entry_pos = k;
							k = StateJacobian->pc[m_VarDiff[j]+1];
						}
					}
					if(entry_exists){
						DFX[j*m_Nalg+i]=StateJacobian->values[entry_pos]; // column-major storage
						nonzeroJacDiff += 1;
					}
					else
						DFX[j*m_Nalg+i]=0.0;
					IROW[j*m_Nalg+i]=i;
					ICOL[j*m_Nalg+i]=j;
				}
			}
			for(int j=0; j<m_Ndiff; j++){	// Indices-transformation, because FORTRAN indices begin with 1
				for(int i=0; i<m_Nalg; i++){
				IROW[j*m_Nalg+i] += 1;
				ICOL[j*m_Nalg+i] += 1;
				}
			}
		}
		else{ // output is dF^a/dx^a (a:algebraical) (general case)
			ComputeJacobian(DFX,IROW,ICOL,NFILL,StateJacobian);
		}
		m_NonZeroJacAlg = nonzeroJacAlg;
		m_NonZeroJacDiff = nonzeroJacDiff;
	};

	/*--------------------------------------------------------------------------
	Finds the number of algebraical variables of the system defined by dae
	--------------------------------------------------------------------------*/
	//int MyEquationSystem::FindNumAlgVar(DaeInterface *dae){
	//	std::auto_ptr<Nixe::CompressedColumn> MassMatrix(new Nixe::CompressedColumn(dae->sizeInfo.numStates,dae->sizeInfo.numStates,dae->sizeInfo.numStates*dae->sizeInfo.numStates));
	//	myDae->EvalMass(MassMatrix.get());
	//	m_Ndiff = MassMatrix->pc[dae->sizeInfo.numStates];
	//	m_Nalg = dae->sizeInfo.numStates - m_Ndiff;
	//	return m_Nalg;
	//};

	/*--------------------------------------------------------------------------
	Finds the variable-indices of the algebraical variables
	--------------------------------------------------------------------------*/
	void MyEquationSystem::FindAlgVar(){
		std::unique_ptr<Nixe::CompressedColumn> MassMatrix(new Nixe::CompressedColumn(m_Nx,m_Nx,m_Dae_problemInfo->NumNonZerosMass()));
		myDae->EvalMass(MassMatrix.get());
		int varAlgCounter = 0;
		int varDiffCounter = 0;
		for(int i=0; i<m_Nx; i++){
			if (MassMatrix->pc[i+1] - MassMatrix->pc[i] <= 0){
				m_VarAlg[varAlgCounter] = i;
				varAlgCounter ++;
			}
			else{
				m_VarDiff[varDiffCounter] = i;
				varDiffCounter ++;
			}
		}
		m_Nalg=varAlgCounter;
		m_Ndiff=varDiffCounter;
		if(m_Nalg+m_Ndiff != m_Nx){
			std::cerr << "ERROR: mismatch in number of algebraic and differential variables!!!" << std::endl;
		}

	};

	void MyEquationSystem::FindAlgEqns() {
		std::unique_ptr<Nixe::CompressedColumn> MassMatrix(new Nixe::CompressedColumn(m_Nx,m_Nx,m_Dae_problemInfo->NumNonZerosMass()));
		myDae->EvalMass(MassMatrix.get());
		MassMatrix->Transpose();
		int eqnsAlgCounter=0;
		int eqnsDiffCounter=0;
		for(int i=0; i<m_Nx; i++){
			if (MassMatrix->pc[i+1] - MassMatrix->pc[i] <= 0){
				m_EqnsAlg[eqnsAlgCounter] = i;
				eqnsAlgCounter ++;
			}
			else{
				m_EqnsDiff[eqnsDiffCounter] = i;
				eqnsDiffCounter ++;
			}
		}

		if(m_Nalg != eqnsAlgCounter || m_Ndiff != eqnsDiffCounter){
			std::cerr << "ERROR: mismatch in number of algebraic equations and variables!!!" << std::endl;
		}
	}

	int MyEquationSystem::IsVarAlg(int inum){
		for(int i=0; i<m_Nalg; i++){
			if(inum == m_VarAlg[i])
				return i;
		}
		return -1;
	}

	int MyEquationSystem::IsEqnAlg(int inum){
		for(int i=0; i<m_Nalg; i++){
			if(inum == m_EqnsAlg[i])
				return i;
		}
		return -1;
	}

	void MyEquationSystem::ComputeJacobian(double* DFX, int* IROW, int* ICOL, int& NFILL, NIXE_SHARED_PTR<Nixe::CompressedColumn> StateJacobian){
		// Given the full Jacobian (StateJacobian), this routine extracts the "reduced" jacobian of the algebraic equations
		// with respect to the algebraic variables an converts the results in triplet form.
		// input: StateJacobian (full Jacobian of DAE system)
		// output: DFX	: values of "reduced" Jacobian in coordinate format
		// output: IROW : row indices of "reduced" Jacobian in coordinate format, Fortran-style indexes
		// output: ICOL : column indices of "reduced" Jacobian in coordinate format, Fortran-style indexes
		// output: NFILL: number of nonzeros of the "reduced" Jacobian
		
		// To use cspare utilities we have to wrap to cs struct
		cs A;
		A.nzmax=StateJacobian->nzmax;
		A.m    =StateJacobian->m;
		A.n    =StateJacobian->n;
		A.p    =StateJacobian->pc;
		A.i	   =StateJacobian->ir;
		A.x	   =StateJacobian->values;
		A.nz   =-1;
		
		// permute the Jacobian to 2 x 2 block structure. the upper left block of dimension n_alg x n_alg contains 
		// the "reduced jacobian"

		int* p=new int[m_Nx];
		int* q = new int[m_Nx];

		//Array<int> p(m_Nx);
		//Array<int> q(m_Nx);

		// A(p,q) will contain the 2 x 2 block structure
		
		VectorCopy(m_Nalg,q,m_VarAlg.Data());
		VectorCopy(m_Ndiff,q+m_Nalg,m_VarDiff.Data());

		VectorCopy(m_Nalg,p,m_EqnsAlg.Data());
		VectorCopy(m_Ndiff,p+m_Nalg,m_EqnsDiff.Data());

		// the cs_permute required the inverte
		int* pinv= cs_pinv(p,m_Nx);

		cs* B= cs_permute(&A,pinv,q,1);


		// cut derivatives with respect to differential variables
		B->n=m_Nalg;

	
		// convert B to compressed row storage to cut differential equations
		cs* C = cs_transpose(B,1);

		// cut differential equations
		C->n=m_Nalg;
		// Here, C contains the "reduced" Jacobian in compressed row storage

		// number of nonzereos of "reduced" Jacobian
		NFILL=C->p[C->n];
		
		// convert compressed row storage to coordinate formate (Fortran style indexing)
		int nz=0;
		for (int i=0; i< C->n; i++){
			for(int j=C->p[i]; j< C->p[i+1];j++){
				IROW[nz]=i+1;
				ICOL[nz]=C->i[nz] +1;
				DFX[nz] = C->x[nz];
				++nz;
			}
		}


		// just free dynamcally allocated csparse stuff
		cs_spfree(C);
		cs_spfree(B);
		cs_free(pinv);
		delete[] p;
		delete[] q;

	}

