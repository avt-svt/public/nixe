/*
 * InitInterface.cpp
 *
 *  Created on: 08.01.2010
 *      Author: Moritz Schmitz and Ralf Hannemann
 */


#include <memory>
#include "cs.h"
#include <fstream>
#include <iostream>
#include <iomanip>
#include <ios>
#include <string>
#include "NixeDaeInterface.hpp"
#include "NixeDenseColumnMajor.hpp"
#include "NixeSuperLU.hpp"
#include "NixeUtil.hpp"
#include "NixeNleq1sWrapper.hpp"
#include "NixeNleq1sEquationSystem.hpp"
#include "NixeNleq1sBlocEquationSystem.hpp"
#include "NixeMinPackBlockEquationSystem.hpp"
#include "NixeNleq1sInitialization.hpp"
// TEST
#include <stdio.h>

#include <vector>
#include <string>


namespace Nixe{


	InitInterface::~InitInterface() {}

	
	InitInterface::InitInterface(DaeInterface<CCSInterface> *dae, const double tol): m_Tol(tol),
			m_Dae(dae),
			m_SensAlg((dae->GetProblemInfo())->NumSens() * ((dae->GetProblemInfo())->NumStates() - (dae->GetProblemInfo())->NumNonZerosMass())),
			m_Sens((dae->GetProblemInfo())->NumStates() * (dae->GetProblemInfo())->NumSens(), 0){
		
		m_Dae_problemInfo = dae->GetProblemInfo();
		m_EqSystem = std::unique_ptr<MyEquationSystem>(new MyEquationSystem(dae,m_Dae_problemInfo.get()));
		m_Solver = std::unique_ptr<Nleq1sWrapper>(new Nleq1sWrapper(m_EqSystem.get()));
		m_numMaxNonZerosLinearSystem = m_Dae_problemInfo->NumNonZerosJac();
		m_LinearSystem = std::unique_ptr<Nixe::CompressedColumn>(new Nixe::CompressedColumn(m_EqSystem->GetDimension(),m_EqSystem->GetDimension(),m_numMaxNonZerosLinearSystem));
		m_SolMatrix = std::unique_ptr<Nixe::DMatrix>(new Nixe::DMatrix(m_EqSystem->GetDimension(),m_EqSystem->GetNumSens()));
	}
	/*--------------------------------------------------
	Calculates consistent x^a(0)
	--------------------------------------------------*/
	void InitInterface::InitializeAlgVar(double *states){

		int nAlg=m_EqSystem->GetDimension();
		if (nAlg == 0) 
			return;

		Array<double> solution(nAlg);

		double *tmpStates=m_EqSystem->GetStates();
		NIXE_SHARED_PTR<Nixe::ProblemInfo> problemInfo =m_Dae->GetProblemInfo();
		Nixe::VectorCopy(problemInfo->NumStates(),tmpStates,states);
		

		m_Solver->Solve();
		if(m_Solver->SuccesfulSolved()){
		//if(false){ // TEST

		//	// pass solution values

			Array<double> solution(nAlg);
			m_Solver->GetSolution(solution.Data());
			for(int i=0; i<nAlg;i++)
				states[m_EqSystem->GetVarAlg(i)]=solution[i];
		}
		else{
			std::ofstream logFile("NLEQ1S.log");
			logFile << "NLEQ1S failed, trying block decomposition" << std::endl;
			logFile.close();
		//	exit(1);
			double t0 = Nixe::timer();
			this->InitializeAlgVarBlockDec(states);
			double t1 = Nixe::timer();
			std::cout << "computation time for Blockdecomposition is: " << t1-t0 << std::endl;

			//FILE * pFile;
			//pFile = fopen ("daeInterface2.txt","w");
			//for(int i=0; i<1896; i++)
			//	fprintf (pFile, "%d  %10e \n",i, states[i]);
			//fclose (pFile);
		
		}
	};
	/*--------------------------------------------------
	Calculates consistent x^a(0) with block-decomposition
	--------------------------------------------------*/
	void InitInterface::InitializeAlgVarBlockDec(double *states){
		std::vector<std::string> names;
		int nAlg=m_EqSystem->GetDimension();
		if (nAlg == 0) 
			return;

		Array<int> rowsStateJac(m_numMaxNonZerosLinearSystem);
		Array<int> colsStateJac(m_numMaxNonZerosLinearSystem);
		Array<int> eqnsAlg(nAlg);
		Array<int> varsAlg(nAlg);
		int nNonZeroDfdx = 0;

		m_Dae->GetStateJacobianStruct(nNonZeroDfdx, rowsStateJac.Data(), colsStateJac.Data()); //row-major storage
		
		Array<int> rowsAlgEqJac(nNonZeroDfdx);
		Array<int> colsAlgStateJac(nNonZeroDfdx); //we need only the structure of dF^a/dx^a
		
		for(int i=0; i<nAlg; i++){
			eqnsAlg[i] = m_EqSystem->GetEqnsAlg(i); //eqnsAlg contains algebraic equation indices in ascending order
			varsAlg[i] = m_EqSystem->GetVarAlg(i);  //varsAlg contains algebraic variable indices in ascending order
		}
		int nonZerosAlgStateJacCount = 0;
		int varAlgpos = -1;
		int eqAlgpos = -1;
		// we run through each entry of the state-jacobian to check whether it belongs to dF^a/dx^a or not
		for(int i=0; i<nNonZeroDfdx; i++){
			eqAlgpos = m_EqSystem->IsEqnAlg(rowsStateJac[i]);
			varAlgpos = m_EqSystem->IsVarAlg(colsStateJac[i]);
			if((varAlgpos>-1) && (eqAlgpos>-1)){//then entries of dF^a/dx^a
				rowsAlgEqJac[nonZerosAlgStateJacCount] = rowsStateJac[i];
				colsAlgStateJac[nonZerosAlgStateJacCount] = colsStateJac[i];
				nonZerosAlgStateJacCount++;
			}
		}

		Array<int> rowsAlgEqJacPerm(nonZerosAlgStateJacCount); // permutation of row indices according to eqnsAlg[i]
		Array<int> colsAlgStateJacPerm(nonZerosAlgStateJacCount); // permutation of row indices according to varsAlg[i]
		varAlgpos = -1;
		int varEqnpos = -1;
		for(int i=0; i<nonZerosAlgStateJacCount;i++){
			varAlgpos = m_EqSystem->IsVarAlg(colsAlgStateJac[i]);
			varEqnpos = m_EqSystem->IsEqnAlg(rowsAlgEqJac[i]);
			colsAlgStateJacPerm[i] = varAlgpos;
			rowsAlgEqJacPerm[i] = varEqnpos;
		}

		cs *A = cs_spalloc(nAlg,nAlg,nonZerosAlgStateJacCount,1,1);
		Nixe::VectorCopy(nonZerosAlgStateJacCount, A->i, rowsAlgEqJacPerm.Data());
		Nixe::VectorCopy(nonZerosAlgStateJacCount, A->p, colsAlgStateJacPerm.Data());
		for(int i=0; i<nonZerosAlgStateJacCount; i++)
			A->x[i] = 1.0;
		A->nz = nonZerosAlgStateJacCount;
		cs *JacAlg = cs_compress(A);
		cs_spfree(A);
		csd *B;
		B = cs_dmperm(JacAlg,0); // Dulmage-Mendelsohn decomposition

		
        //		 Solve subsystems defined by blocs:
		
		Array<double> tempStates(m_Dae_problemInfo->NumStates(),0);
		const double startTime=m_Dae_problemInfo->StartTime();
		VectorCopy(m_Dae_problemInfo->NumStates(), tempStates.Data(), states); // necessary to initialize the differential states
		int nBlocs = B->nb;
		for (int iBloc = nBlocs-1; iBloc>=0; iBloc--){
			int numVars = *((B->r)+iBloc+1) - *((B->r)+iBloc);
			int blocCount = 1;
			
			//bool twoBlocs = false;											// 2 blocs
			//if(numVars==1 && iBloc>0){ // we take 2 blocs at a time			
			//	iBloc--;													
			//	numVars = numVars + *((B->r)+iBloc+1) - *((B->r)+iBloc);	
			//	twoBlocs = true;
			//}
			//if(numVars==1 && iBloc==0){										// last bloc is scalar
			//	numVars = numVars + *((B->r)+iBloc+2) - *((B->r)+iBloc+1);
			//	twoBlocs = true;
			//}																// 2 blocs

			//if(twoBlocs)													// 2 blocs
			//	blocCount = 2;												// 2 blocs

			Array<int> rowIndStateJac(numVars);
			Array<int> colIndStateJac(numVars);

			int nonZerosReducedJac = 0;
			
			//		recover original indices of requested entries from dF^a/dx^a in dF/dx
			
			int j = 0;
			for(int i= *((B->r)+iBloc); i < (*((B->r)+iBloc+ blocCount)) ; i++) {
				rowIndStateJac[j] = eqnsAlg[*((B->p)+i)];
				j++;
			}
			int k = 0;
			for(int i= *((B->s)+iBloc); i < (*((B->s)+iBloc+ blocCount)) ; i++) {
				colIndStateJac[k] = varsAlg[*((B->q)+i)];
				k++;
			}

			//		find number of non-zeros in jacobian bloc and save the sparsity pattern to use it later in "EvalStateJac".
			//		The sparsity pattern for the Bloc matrix is saved in the way that the number (in row-major format) of the entry
			//		is saved if the entry is non-zero.
			//													  x 0 0 x  
			//		For example: the sparsity pattern of matrix   0 0 x x  would be: [0 3 6 7 9 10 11 13 15].
			//													  0 x x x
			//													  0 x 0 x
			std::vector<int> sparsityPattern;
			
			for(int j= *((B->r)+iBloc); j < (*((B->r)+iBloc+ blocCount)) ; j++) { // select the entry in specified column of JacAlg to look for
				for(int i= *((B->s)+iBloc); i < (*((B->s)+iBloc+ blocCount)) ; i++) { // select column of JacAlg to explore	
					for(int k= JacAlg->p[*((B->q)+i)]; k < JacAlg->p[*((B->q)+i)+1]; k++){ // look in all column entries
						if( JacAlg->i[k] == *((B->p)+j)){
							nonZerosReducedJac++;
							sparsityPattern.push_back((j-*((B->r)+iBloc))*numVars+(i-*((B->s)+iBloc)));
						}
					}
				}
			}

			std::unique_ptr<MinPackBlockEquationSystem> myBlocEqSystem(new MinPackBlockEquationSystem(m_Dae, m_Dae_problemInfo.get(), sparsityPattern,
								   numVars, rowIndStateJac.Data(), colIndStateJac.Data(), tempStates.Data(), nonZerosReducedJac));
			std::unique_ptr<MinPackWrapper> mySolver(new MinPackWrapper(myBlocEqSystem.get()));

			mySolver->Solve();
			if(!mySolver->SuccesfulSolved()){
				std::ofstream logFile("BlockDeco.log");
				logFile << "Block decomposition failed, exiting program!" << std::endl;
				logFile.close();
				exit(1);
			}
			
			Array<double> subSolution(numVars);
			mySolver->GetSolution(subSolution.Data());
			for(int i=0; i<numVars;i++){
				tempStates[myBlocEqSystem->GetVarIndices(i)] = subSolution[i];
				states[myBlocEqSystem->GetVarIndices(i)] = subSolution[i];
			}

		}
		m_EqSystem->SetStates(states); // for initialization of sens, to have consistent initial states

		cs_spfree(JacAlg);
		cs_dfree(B);

	};
	/*--------------------------------------------------
	Calculates consistent dx^a/dp(0)
	--------------------------------------------------*/
	void InitInterface::InitializeAlgSens(double *sens){

		//
		// Solving linear System for Delta_dx^a/dp : -dF^a/dx^a * Delta_dx^a/dp = dF^a/dx^a * dx^a/dp + dF^a/dx^d * dx^d/dp + dF^a/dp
		//
		// the right value of dx^a/dp: dx^a/dp(0) = dx^a/dp + Delta_dx^a/dp
		//

		VectorCopy(m_Dae_problemInfo->NumStates() * m_Dae_problemInfo->NumSens(), m_Sens.Data(), sens);
		int nAlg = m_EqSystem->GetDimension();
		if (nAlg == 0) 
			return;
		int nDiff = m_EqSystem->GetDimDiff();
		int nStat = nAlg + nDiff;
		int nSens = m_EqSystem->GetNumSens();
		int nonzeroJacAlgMax = m_Dae_problemInfo->NumNonZerosJac();
		cs *A = cs_spalloc(nAlg,nAlg,nonzeroJacAlgMax,1,1);

		// Calculating System Matrix -dF^a/dx^a : 

		double *DFX = A->x;
		int *IROW = A->i;
		int *ICOL = A->p;
		int& NFILL = A->nz;
		int IFAIL=0;
		double *Xalg = new double[nAlg];
		for(int i=0; i<nAlg; i++)
			Xalg[i] = m_EqSystem->GetStatesNum(m_EqSystem->GetVarAlg(i));
		m_EqSystem->Jacobian(false,0, Xalg, DFX, IROW, ICOL, NFILL,IFAIL);  // InitializeAlgVar() has to be run first

		//Changing from Fortran Style to C style indexing
		for (int i=0; i<NFILL;i++){
			--IROW[i];
			--ICOL[i];
		}

		cs *B = cs_compress(A);
		Nixe::VectorCopy(B->n + 1,m_LinearSystem->pc, B->p);
		Nixe::VectorCopy(B->p[B->n],m_LinearSystem->ir,B->i);
		Nixe::VectorCopy(B->p[B->n],m_LinearSystem->values,B->x);
		
		cs_spfree(B);
		cs_spfree(A);
		
		delete[] Xalg;
		m_SuperLU = std::unique_ptr<Nixe::SuperLU>(new Nixe::SuperLU(m_LinearSystem.get()));

		// Calculating Rhs - dF^a/dx^a * dx^a/dp - dF^a/dx^d * dx^d/dp - dF^a/dp : 
		
		std::unique_ptr<Nixe::CompressedColumn> StateJacobian (new Nixe::CompressedColumn(nStat,nStat,nonzeroJacAlgMax));
		double * rhsSensMatrix = new double[nStat*nSens]; // column-major storage
		for(int i=0; i<nStat*nSens; i++)
			rhsSensMatrix[i] = 0;
		const double startTime=m_Dae_problemInfo->StartTime();
		m_Dae->EvalForward(false, startTime, m_EqSystem->GetStates(), m_EqSystem->GetParameters(), m_EqSystem->GetRhs(), m_EqSystem->GetSens(), StateJacobian.get(), 
			m_EqSystem->GetRhsStatesDtime(), rhsSensMatrix, *m_Dae_problemInfo.get());
			/*the first parameter 'evalJac = false' because we don't need the jacobian explicitly,
			the sensitivities were initialized by the construction of m_EqSystem*/
		DenseMatrix<double> * RhsSensMatrix = new DenseMatrix<double>(nAlg, nSens); // DMatrix is in column-major storage format
		double * rhsSensMatrixAlg = new double[nAlg*nSens];
		int varAlg;
		for(int i=0; i<nAlg; i++){
			varAlg = m_EqSystem->GetEqnsAlg(i);
			for(int j=0; j<nSens; j++){
				rhsSensMatrixAlg[j*nAlg + i] = -rhsSensMatrix[j*nStat + varAlg];
			}
		}
		Nixe::VectorCopy(nAlg*nSens,RhsSensMatrix->Data(), rhsSensMatrixAlg);
		delete[] rhsSensMatrix;
		delete[] rhsSensMatrixAlg;

		// Solving the linear system -dF^a/dx^a * Delta_dx^a/dp = dF^a/dx^a * dx^a/dp + dF^a/dx^d * dx^d/dp + dF^a/dp :

		std::unique_ptr<Nixe::SuperLUreuseInfo> ReuseInfo(new Nixe::SuperLUreuseInfo(nAlg*nSens));
		bool success = false;
		bool transpose = false;
		m_SuperLU->SLUSolve(*m_SolMatrix.get(), *RhsSensMatrix, ReuseInfo.get(), &success, transpose);
		for (int i=0; i<nAlg; i++){
			varAlg = m_EqSystem->GetVarAlg(i);
			for (int j=0; j<nSens; j++){
				m_SensAlg[j*nAlg + i] = m_SolMatrix->operator()(i,j) + m_EqSystem->GetSensNum(j*nStat + varAlg);
				// Calculates dx^a/dp(0) = dx^a/dp + Delta_dx^a/dp
				sens[j*(nAlg+nDiff) + m_EqSystem->GetVarAlg(i)] = m_SensAlg[j*nAlg + i];
			}
        } 

		delete RhsSensMatrix;



	};

	void InitInterface::PrintSensToFile(const std::string& fileName){
	    int nAlg = m_EqSystem->GetDimension();
	    int nParam = m_EqSystem->GetNumSens();
        std::ofstream outFile(fileName.c_str());
        outFile.setf(std::ios_base::scientific, std::ios_base::floatfield);
        for (int i=0; i<nAlg; i++){
          for (int j=0; j<nParam; j++)
            outFile << "  " << m_SensAlg[j*nAlg + i];
          outFile << std::endl;
        } 
        outFile.close();
    };
	





}
