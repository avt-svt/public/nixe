#include "NixeCompressedColumnUMFPACK.hpp"
#include "cs.h"
#include "NixeUtil.hpp"



namespace Nixe {

UmfpackReuseInfo::UmfpackReuseInfo(int dimension) {
	m_IsFirstCall=true;
	m_Symbolic=0;
}
UmfpackReuseInfo::~UmfpackReuseInfo() {
	if(m_Symbolic)
	   umfpack_di_free_symbolic(&m_Symbolic);
}


int CompressedColumnUMFPACK::GetMaximalBlockSize() const {
	csd *dmperm = cs_dmperm(m_Imp,0);

	int maxRowBlockSize=0;
	int maxColBlockSize=0;
	for(int i=0; i< dmperm->nb;i++){
		maxRowBlockSize=Max(dmperm->r[i+1]-dmperm->r[i],maxRowBlockSize);
		maxColBlockSize=Max(dmperm->r[i+1]-dmperm->r[i],maxColBlockSize);
	}
	cs_dfree(dmperm);
	return Max(maxRowBlockSize,maxColBlockSize);
}

CompressedColumnUMFPACK::CompressedColumnUMFPACK(const double alpha, const CompressedColumnUMFPACK& A,
		const double beta, const CompressedColumnUMFPACK& B): m_Numeric(0), m_Symbolic(0) {
  m_Imp = cs_add(A.m_Imp,B.m_Imp, alpha,beta);
  this->nzmax = m_Imp->nzmax;
  this->m     = m_Imp->m;
  this->n     = m_Imp->n;
  this->pc    = m_Imp->p;
  this->ir    = m_Imp->i;
  this->values= m_Imp->x;
  int nz = this->pc[this->n];
  int status;
  int* Tj = new int[nz];
  int* Ti = new int[nz];
  double* Tx = new double[nz];
  Nixe::VectorCopy(nz,Ti,this->ir);
  Nixe::VectorCopy(nz,Tx,this->values);
  status = umfpack_di_col_to_triplet (this->n, this->pc, Tj) ;
  status = umfpack_di_triplet_to_col (this->m, this->n, nz, Ti, Tj, Tx, this->pc, this->ir, this->values, NULL);
  delete[] Ti;
  delete[] Tj;
  delete[] Tx;
}


CompressedColumnUMFPACK::CompressedColumnUMFPACK(int m, int n, int nnz): m_Numeric(0), m_Symbolic(0){
  m_Imp = cs_spalloc(m,n,nnz,1,0);
  this->nzmax = m_Imp->nzmax;
  this->m     = m_Imp->m;
  this->n     = m_Imp->n;
  this->pc    = m_Imp->p;
  this->ir    = m_Imp->i;
  this->values= m_Imp->x;
}

CompressedColumnUMFPACK::~CompressedColumnUMFPACK() {
  cs_spfree(m_Imp);
  if(m_Numeric)
	umfpack_di_free_numeric(&m_Numeric);
};

void CompressedColumnUMFPACK::Solve(DMatrix& solution,const DMatrix& rhs,
    		UmfpackReuseInfo * reuse, bool *success, bool transpose) {
		int sys;
		if(transpose)
			sys = UMFPACK_At;
		else
			sys = UMFPACK_A;

		//double Info [UMFPACK_INFO];
		int status;

		/* symbolic analysis */
		if (reuse->GetIsFirstCall()) {
			status = umfpack_di_symbolic(this->m, this->n, this->pc, this->ir, this->values, &m_Symbolic, NULL, NULL);//Info);
			reuse->SetSymbolic(m_Symbolic);
			reuse->SetIsFirstCall(false);
		}
		else {
			reuse->GetSymbolic(m_Symbolic);
		}

		/* LU factorization */
		status = umfpack_di_numeric(this->pc, this->ir, this->values, m_Symbolic, &m_Numeric, NULL, NULL);//Info);
		
		if (status == 0)
			*success = true;
		else
			*success = false;


		/* solve system */
		const int numRhs=rhs.NumCols();
		for (int i=0; i<numRhs; i++) {
			const double *b = rhs.Col(i);
			double *x = solution.Col(i);
			umfpack_di_solve(sys, this->pc, this->ir, this->values, x, b, m_Numeric, NULL, NULL);//Info);
		}
}

void CompressedColumnUMFPACK::SolveAgain(DMatrix& solution,const DMatrix& rhs, bool transpose) {
        int sys;
		if(transpose)
			sys = UMFPACK_At;
		else
			sys = UMFPACK_A;

		/* solve system */
		const int numRhs=rhs.NumCols();
		for (int i=0; i<numRhs; i++) {
			const double *b = rhs.Col(i);
			double *x = solution.Col(i);
			umfpack_di_solve(sys, this->pc, this->ir, this->values, x, b, m_Numeric, NULL, NULL);
		}
}
void CompressedColumnUMFPACK::Multiply(const double coeff, double *result,const double *toMult,
				    const  int mm,const int nn) {
  for (int i=0;i<m*nn;i++)
    result[i]*=coeff;
  for (int i=0; i < nn; i++)
    cs_gaxpy(m_Imp,&(toMult[i*n]),&(result[i*mm]));
}

void CompressedColumnUMFPACK::TransposeMultiply(const double coeff, double *result,const double *toMult,
				     const int mm,const int nn) {
  for (int i=0;i<this->n*nn;i++)
    result[i]*=coeff;
  cs * transposeImp = cs_transpose(m_Imp,1);
  for (int i=0; i < nn; i++)
    cs_gaxpy(transposeImp,&(toMult[i*mm]),&(result[i*this->n]));
  cs_spfree(transposeImp);
}

void CompressedColumnUMFPACK::Add(double *result, const int mm, const int nn) {
	int counter = 0;
	int index;
  for(int i=0; i<n;i++)
    for (int p=pc[i]; p<pc[i+1]; p++) {    
			index = i*m+ir[p];
      result[index] += values[counter];
			++counter;
    }
}

void CompressedColumnUMFPACK::Transpose() {
	cs *res = cs_transpose(this->m_Imp,1);
	cs_spfree(m_Imp);
	m_Imp=res;
	this->nzmax = m_Imp->nzmax;
    this->m     = m_Imp->m;
    this->n     = m_Imp->n;
    this->pc    = m_Imp->p;
    this->ir    = m_Imp->i;
    this->values= m_Imp->x;
}
} // namespace Nixe 
