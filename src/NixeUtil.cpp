#include "NixeUtil.hpp"
#include "NixeMatrix.hpp"
#include "NixeBlasLapack.hpp"





void Nixe::Statistics::Initialize() {
	m_NumberRightHandSideCalls=0; // only f 
	m_NumberJacobianCalls=0;
	m_NumberDecompositions=0; // LU decomposition
	m_NumberBackSubstitutions=0;
	m_NumberSteps=0;
	m_NumberAcceptedSteps=0;
	m_NumberRejectedSteps=0;
}

void Nixe::VectorCopy(int n, double * NIXE_RESTRICT dest, const double* NIXE_RESTRICT source){
	int incx=1;
	int incy=1;
	dcopy_(&n,source,&incx,dest,&incy);
}



void Nixe::VectorCopy(int n, double* NIXE_RESTRICT dest, double source){
	int incx=0;
	int incy=1;
	dcopy_(&n,&source,&incx,dest,&incy);
	
}

void Nixe::VectorPlusEqual(int n, double* NIXE_RESTRICT dest, double* NIXE_RESTRICT toAdd){
	int incx=1;
	int incy=1;
	double alpha=1.0;
	daxpy_(&n,&alpha,toAdd,&incx,dest,&incy);
}

#ifdef WIN32
#include<windows.h>
double Nixe::timer() {
	LARGE_INTEGER freq, st;
	__int64 time;
	QueryPerformanceCounter (&st);
	QueryPerformanceFrequency (&freq);
	
	time = st.QuadPart;

	return ((double) time)/((double) freq.QuadPart);
}
#else
#include <time.h>
double Nixe::timer() {
        struct timespec m_TimeSpec;
        clock_gettime(CLOCK_PROCESS_CPUTIME_ID  , &m_TimeSpec);
        double time=m_TimeSpec.tv_sec + 1e-9*m_TimeSpec.tv_nsec;
        return time;
}
#endif

namespace Nixe{
	DenseMatrix<double>::DenseMatrix(const int numRows, const int numCols, double value) : m_Data(new double[Max(numRows*numCols,1)]), m_NumRows(numRows), m_NumCols(numCols) {
	  for(int i=0; i<m_NumRows*m_NumCols; i++)
			m_Data[i] = value;
	  }
	 DenseMatrix<double>::DenseMatrix(const int numRows, const int numCols) :m_Data(new double[Max(numRows*numCols,1)]), m_NumRows(numRows), m_NumCols(numCols) {}
	 DenseMatrix<double>::DenseMatrix(const DenseMatrix<double>& rhs) : m_Data(new double[rhs.m_NumRows*rhs.m_NumCols]), m_NumRows(rhs.m_NumRows),
		m_NumCols(rhs.m_NumCols) {
		for (int i=0; i< Size(); ++i)
			m_Data[i]=rhs.m_Data[i];
	}
	 DenseMatrix<double>& DenseMatrix<double>::operator=(const DenseMatrix<double>& rhs) {
		for(int i=0; i<Size(); i++)
			m_Data[i] = rhs.m_Data[i];
		  return *this;
    }
    DenseMatrix<double>::~DenseMatrix() {
		delete[] m_Data;
	}

	
template<> 
Array<double>::Array(const int size): m_Data(new double[size]), m_Size(size) {}

template<> 
Array<double>::Array(const int size, double value): m_Data(new double[size]), m_Size(size) {
	  for(int i=0; i<m_Size; i++){
			m_Data[i] = value;
	  }
}

template<> 
Array<double>::Array(const Array& rhs) :m_Data(new double[rhs.Size()]), m_Size(rhs.Size()) {
    for (int i=0; i< m_Size; ++i)
      m_Data[i]=rhs.m_Data[i];};

template<>
Array<double>::~Array(){delete[] m_Data;}

template<>
Array<int>::Array(const int size): m_Data(new int[size]), m_Size(size) {}

template<>
Array<int>::Array(const int size, int value): m_Data(new int[size]), m_Size(size) {
	  for(int i=0; i<m_Size; i++){
			m_Data[i] = value;
	  }
}

template<>
Array<int>::Array(const Array& rhs) :m_Data(new int[rhs.Size()]), m_Size(rhs.Size()) {
    for (int i=0; i< m_Size; ++i)
      m_Data[i]=rhs.m_Data[i];};

template<>
Array<int>::~Array(){delete[] m_Data;}


}
