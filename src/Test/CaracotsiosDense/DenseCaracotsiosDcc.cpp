#include "DenseCaracotsiosDcc.hpp"
#include "NixeUtil.hpp"
#include "NixeProblemInfo.hpp"
#include <cmath>

void rhs(double* f, double* x, double* p, int n, int m);

void b1_rhs(int& bmode_1, double* f, double* b1_f, double* x, double* b1_x, double* p, double* b1_p, int& n, int& m);

void d1_rhs(double* f, double* d1_f, double* x, double* d1_x, double* p,
		double* d1_p, int& n, int& m);

void d2_b1_rhs(int& bmode_1, double* f, double* d2_f, double* b1_f, double* d2_b1_f,
		double* x, double* d2_x, double* b1_x, double* d2_b1_x, double* p,
		double* d2_p, double* b1_p, double* d2_b1_p, int& n, int& m);

NIXE_SHARED_PTR<Nixe::ProblemInfo> DenseCaracotsiosDcc::GetProblemInfo() {
	NIXE_SHARED_PTR<Nixe::ProblemInfo> temp(new Nixe::ProblemInfo);
	
	temp->SetNumAdjoints(9);
	temp->SetNumStates(10);
	temp->SetNumNonZerosJac(36);
	temp->SetNumNonZerosMass(6);
	temp->SetNumDers(8+8*8);
	temp->SetNumSwitchFctns(0);
	temp->SetNumSens(8);
	temp->SetNumParams(8);
	temp->SetStartTime(0);
	temp->SetFinalTime(6);
	temp->SetSave(true);
	temp->SetNumSaveSens(8);


	return temp;
}
void DenseCaracotsiosDcc::EvalParameters(double *parameters,const Nixe::ProblemInfo& problemInfo) {
  double *u=parameters;
  u[0]=21.893; //k_1 hr^(-1) Kg gmole^(-1)
  u[1]=2.14E09; //k_-1 hr^(-1)
  u[2]=32.318; //k_2 hr^(-1) Kg gmole^(-1)
  u[3]=21.893; //k_3 hr^(-1) Kg gmole^(-1)
  u[4]=1.07E09;//k_-3 hr^(-1)
  u[5]=7.65E-18;//K_1 gmole Kg^(-1)
  u[6]=4.03E-11;//K_2 gmole Kg^(-1)
  u[7]=5.32E-18;//K_3 gmole Kg^(-1)
}






	void DenseCaracotsiosDcc::EvalForward(bool evalJac, double time, double* states, double *parameters, 
			double *rhsStates, double* sens, Nixe::DCMInterface *jacobian, 
			double* rhsStatesDtime, double* rhsSens, const Nixe::ProblemInfo& problemInfo) const {
				
		const int n_p=problemInfo.NumParams();
		const int n_x=problemInfo.NumStates();
		const int n_sens=problemInfo.NumSens();

		// eval f(x,p)
		double *f=rhsStates;
		double *x=states;
		double *p=parameters;
		int n=n_x;
		int m=n_p;
		rhs(f,x,p,n,m);

		// eval Dp f(x,p)
		
		for(int i=0; i<n_sens; i++){
			Nixe::Array<double> d1_p(m,0);
			d1_p[i]=1.0;
			double *d1_f = &(rhsSens[n_x*i]);
			double *d1_x = &(sens[n_x*i]);
			d1_rhs(f,d1_f,x,d1_x,p,d1_p.Data(),n,m);
		}
		
		// eval Jacobian if required

		if(evalJac) {
			Nixe::Array<double> d1_x(n);
			Nixe::Array<double> d1_p(m);
			double * d1_f=0;
			for(int j=0;j<m; j++)
				d1_p[j]=0.0;
			for (int i=0; i<n; i++){
				for(int j=0; j<n; j++)
					d1_x[j]=(i==j)?1.0:0.0;
				d1_f = &(jacobian->values[i*n]);
				d1_rhs(f,d1_f,x,d1_x.Data(),p,d1_p.Data(),n,m);

			}
		}

	};

void  DenseCaracotsiosDcc::EvalReverse(double time, double *states, double* parameters, double* sens, double* adjoints, 
						  double * rhsAdjoints, double* rhsDers, const Nixe::ProblemInfo& problemInfo) const {
	const int n_p=problemInfo.NumParams();
	const int n_x=problemInfo.NumStates();
//	const int n_sens=problemInfo.NumSens();
	const int n_lam1 = 1;  // number of first-order adjoints
	const int n_lam2 = problemInfo.NumAdjoints()-n_lam1; // number of second-order adjoints
//	const int n_ders1 = n_lam1*n_p; // dimension of gradients
//	const int n_ders2 = problemInfo.NumDers() - n_ders1; // dimension of Hessians

	double *x = states;
    double *p = parameters;

	int n=n_x;
	int n_obj=n_lam1;
	int m=n_p;

    int bmode_1=1;
	Nixe::Array<double> f(n,0.0);
	Nixe::Array<double> b1_f(n);
	double *b1_x = 0;
	double *b1_p = 0;
	for (int i=0; i<n_obj; i++){
		b1_x=&rhsAdjoints[i*n];
		Nixe::VectorCopy(n,b1_x,0.0);
		b1_p=&rhsDers[i*m];
		Nixe::VectorCopy(m,b1_p,0.0);
		Nixe::VectorCopy(n,b1_f.Data(),&adjoints[i*n]);
		b1_rhs(bmode_1,f.Data(),b1_f.Data(),x,b1_x,p,b1_p,n,m);
	}

	if(n_lam2 > 0) {
		int bmode_1 = 1;
		Nixe::Array<double> f(n,0.0);
		Nixe::Array<double> d2_f(n,0.0);
		Nixe::Array<double> b1_f(n);
		double * d2_b1_f=0;
		double * d2_x=0;
		Nixe::Array<double> b1_x(n);
		double * d2_b1_x=0;
		Nixe::Array<double> d2_p(m);
		Nixe::Array<double> b1_p(m);
		double * d2_b1_p=0;

		for(int i=0; i<n_obj;i++){
			for(int j=0; j<m;j++){
				d2_b1_f=&adjoints[n_lam1*n_x+i*n*m+j*n];
				d2_x = &sens[j*n];
				d2_b1_x =&rhsAdjoints[n_lam1*n_x+i*n*m+j*n];
				d2_b1_p =&rhsDers[n_lam1*n_p+i*m*m+j*m];
				for(int k=0; k<m;k++){
					d2_p[k]=(j!=k)?0.0:1.0;
				}
				Nixe::VectorCopy(n,b1_f.Data(),&adjoints[i*n]);
				Nixe::VectorCopy(n,b1_x.Data(),0.0);
				Nixe::VectorCopy(n,d2_b1_x,0.0);
				Nixe::VectorCopy(m,b1_p.Data(),0.0);
				Nixe::VectorCopy(m,d2_b1_p,0.0);
			
   
				d2_b1_rhs(bmode_1,f.Data(),d2_f.Data(),b1_f.Data(),d2_b1_f,x,d2_x,b1_x.Data(),
					d2_b1_x,p,d2_p.Data(),b1_p.Data(),d2_b1_p,n,m);

			} // end for j
		}  // end for i
	}  // end if n_lam2

	return;
}

void DenseCaracotsiosDcc::EvalSwitchingFunction(double time, double *states, double *parameters, 
			double *switchingFctn, const Nixe::ProblemInfo& problemInfo) const {
			//switchingFctn[0]=states[0]-0.8;//207771;
			//switchingFctn[1]=states[1]-7.4203971;

}

void DenseCaracotsiosDcc::EvalInitialValues(double time, double *parameters, double* states, double* sens, 
											 const Nixe::ProblemInfo& problemInfo){
	const int n_p=problemInfo.NumParams();
	const int n_x=problemInfo.NumStates();
	const int n_sens=problemInfo.NumSens();

  double *x0=states;
  double *p=parameters;
 x0[0]=1.5776;
  x0[1]=8.32;
  x0[2]=0;
  x0[3]=0;
  x0[4]=0;
  x0[5]=0.0131;
  x0[6]=0.5*(-p[6]+ sqrt(p[6]*p[6]+4*p[6]*1.5776));
  x0[7]=0.5*(-p[6]+ sqrt(p[6]*p[6]+4*p[6]*1.5776));
  x0[8]=0;
  x0[9]=0;
  Nixe::VectorCopy(n_x*n_sens,sens,0.0);
  const double par=p[6];
  const double dpar=(2.*par + 3944./625.)/(4.*sqrt(par*par + (3944.*par)/625.)) - 1./2.; 

  if(n_sens > 0) {
	sens[66] = dpar;
	sens[67] = dpar;
  }
}

void DenseCaracotsiosDcc::EvalFinalValues(double time, double *states, double *parameters, 
			double* sens, double* adjoints, const Nixe::ProblemInfo& problemInfo) {

	// phi(x)=x[0] is objective function
	
				for(int i=0;i<problemInfo.NumAdjoints()*problemInfo.NumStates();i++)
		adjoints[i]=0;
	adjoints[0]=1.0;

	
}


void rhs(double* f, double* x, double* p, int n, int m)
{
  // f[n] : out, right hand side of DAE
  // x[n] : in, state vector
  // p[m] : in, parameter vector
  // n    : in, dimension of f and x
  // m	  : in, dimension of p
  f[0] =-p[2]*x[1]*x[7];
  f[1] =-p[0]*x[1]*x[5]+p[1]*x[9]-p[2]*x[1]*x[7];
  f[2] =p[2]*x[1]*x[7]+p[3]*x[3]*x[5]-p[4]*x[8];
  f[3] =-p[3]*x[3]*x[5]+p[4]*x[8];
  f[4] =p[0]*x[1]*x[5]-p[1]*x[9];
  f[5] =-p[0]*x[1]*x[5]-p[3]*x[3]*x[5]+p[1]*x[9]+p[4]*x[8];
  f[6] =-0.0131+x[5]+x[7]+x[8]+x[9]-x[6];
  f[7] =p[6]*x[0]/(p[6]+x[6])-x[7];
  f[8] =p[7]*x[2]/(p[7]+x[6])-x[8];
  f[9] =p[5]*x[4]/(p[5]+x[6])-x[9];
};


