#ifndef _DENSE_CARACOTSIOS_DCC_INCLUDE_ /* allow multiple inclusions */
#define _DENSE_CARACOTSIOS_DCC_INCLUDE_ 

#include "NixeDCMInterface.hpp"
#include "NixeDaeInterface.hpp"

class DenseCaracotsiosDcc : public  Nixe::DaeInterface<Nixe::DCMInterface> {
public:


  virtual void EvalMass(Nixe::DCMInterface *mass){
	  Nixe::VectorCopy(mass->m*mass->n,mass->values,0.0);
	  for(int i=0;i<6;i++)
		  mass->values[mass->m*i+i]=1.0;
  }
  


	

  

	virtual void EvalForward(bool evalJac, double time, double* states, double *parameters, 
		double *rhsStates, double* sens, Nixe::DCMInterface *jacobian, 
			double* rhsStatesDtime, double* rhsSens, const Nixe::ProblemInfo& problemInfo) const;

	virtual void EvalReverse(double time, double *states, double* parameters, double* sens, double* adjoints, 
			double * rhsAdjoints, double* rhsDers, const Nixe::ProblemInfo& problemInfo) const;

			virtual void EvalSwitchingFunction(double time, double *states, double *parameters, 
			double *switchingFctn, const Nixe::ProblemInfo& problemInfo) const;

	virtual void EvalInitialValues(double time, double *parameters, double* states, double* sens, 
			const Nixe::ProblemInfo& problemInfo);

	virtual void EvalFinalValues(double time, double *states, double *parameters, 
			double* sens, double* adjoints, const Nixe::ProblemInfo& problemInfo);

	virtual NIXE_SHARED_PTR<Nixe::ProblemInfo> GetProblemInfo();

	virtual void EvalParameters(double *parameters,const Nixe::ProblemInfo& problemInfo);

 
}; 


#endif
