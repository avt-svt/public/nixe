#include "NixeDenseColumnMajor.hpp"
#include "NixeForwardSolver.hpp"
#include <iostream>
#include <iomanip>
#include "NixeAdjointData.hpp" 
#include "WOBatch.hpp"
#include "NixeUtil.hpp"
#include "NixeReverseSolver.hpp"
#include <fstream>
#include <string>
#include <vector>

using namespace Nixe;  




int main(int argc, char *argv[])
 {

 /************** load Reference Hessian **************/


	int degree=0;  // degree of forward sensitivity computation
	bool doReverse=true;
	int lenGrid;
	if (argc > 1) lenGrid=atoi(argv[1]);
	else lenGrid=10;
	double t0,t1,t2,t3;
	t0=timer();
	NIXE_SHARED_PTR<WOBatch> dae(new WOBatch(degree,doReverse,lenGrid));
	NIXE_SHARED_PTR<Options>  options(new Options);
	options->SetAbsTolScalar(1e-5);
	options->SetRelTolScalar(1e-5);
	options->SetInitialStepSize(100);
	options->SetTypeSequence(Seq_ShiftedHarmonic);
	options->SetComputeCIV(false);

	NIXE_SHARED_PTR<ForwardSolver<FastDenseColumnMajor> >nixeFor;
	std::vector<NIXE_SHARED_PTR<Checkpoints> > chckVec(lenGrid);
	NIXE_SHARED_PTR<Array<double> > states;

	for(int i=0; i<lenGrid;i++){
		dae->SetIndex(i);
		nixeFor.reset(new ForwardSolver<FastDenseColumnMajor>(dae,options));
		nixeFor->Solve();
		states=nixeFor->GetStates();
		dae->SetStates(states);
		if(doReverse)
			chckVec[i]=nixeFor->GetCheckpoints();
	}
	t1=timer();
	t2=timer();
	NIXE_SHARED_PTR<Nixe::DMatrix> adjoints;
	NIXE_SHARED_PTR<ReverseSolver<FastDenseColumnMajor> > nixeRev;
	Nixe::DMatrix dersMat(lenGrid,2,0.0);
	if(doReverse){
		for(int i=lenGrid-1;i>=0;i--){
			dae->SetIndex(i);
			NIXE_SHARED_PTR<Checkpoints> check = chckVec[i];
			nixeRev.reset(new ReverseSolver<FastDenseColumnMajor>(dae,check));
			nixeRev->Solve();
			adjoints=nixeRev->GetAdjoints();
			dae->SetAdjoints(adjoints);
			NIXE_SHARED_PTR<Array<double> > ders=nixeRev->GetDerivatives();
			dersMat(i,0)=(*ders)[0];
			dersMat(i,1)=(*ders)[1];
		}
	}
	t3=timer();

	/* std::cout << "Forward degree   : " << degree << std::endl;
	std::cout << "do reverse sweep : " << doReverse << std::endl;
	std::cout << "cost for forward sweep :" << t1-t0 << std::endl;
	if(doReverse) std::cout << "cost for reverse sweep :" << t3-t2 << std::endl; */

	  /******* output section ***********/
	/*
	std::cout << "Final States" << std::endl;
	states->PrintToFile("x1.dat");
	
	if(doReverse)
	{

		std::cout << "gradient matrix is" << std::endl;
			dersMat.PrintToFile("dersMat.txt");
	}
	*/
	  std::cout << lenGrid << "\t";
	  std::cout << 1000*(t1-t0) << "\t";
	  std::cout << 1000*(t3-t2) << std::endl;

	return 0;
}

