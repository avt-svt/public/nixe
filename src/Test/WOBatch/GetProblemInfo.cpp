#include "WOBatch.hpp"

NIXE_SHARED_PTR<Nixe::ProblemInfo> WOBatch::GetProblemInfo() {
	NIXE_SHARED_PTR<Nixe::ProblemInfo> temp(new Nixe::ProblemInfo);

	const int nX=9;
	const int nP=2;
	const double timeHorizon=m_FinalTime/static_cast<double>(m_LenGrid);

	if(m_DoReverse) temp->SetSave(true);
	else temp->SetSave(false);
	temp->SetNumAdjoints(m_Degree* nP +1);

	temp->SetNumStates(nX);
	temp->SetNumNonZerosJac(nX*nX);
	temp->SetNumNonZerosMass(nX*nX);
	temp->SetNumDers(nP);
	temp->SetNumSwitchFctns(0);
	temp->SetNumSens(0);
	temp->SetNumParams(nP);
	temp->SetStartTime(static_cast<double>(m_Index)* timeHorizon);
	temp->SetFinalTime(static_cast<double>(m_Index+1)* timeHorizon);
	temp->SetNumSaveSens(0);
	return temp;
}

