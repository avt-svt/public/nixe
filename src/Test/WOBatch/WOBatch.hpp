#ifndef WO_BATCH_HPP__ /* allow multiple inclusions */
#define WO_BATCH_HPP__

#include "NixeDCMInterface.hpp"
#include "NixeDaeInterface.hpp"

class WOBatch : public  Nixe::DaeInterface<Nixe::DCMInterface> {
public:
	WOBatch(const int degree, const bool doReverse, const int lenGrid) : m_Degree(degree),
		m_DoReverse(doReverse),
		m_LenGrid(lenGrid),
		m_Index(0) {m_FinalTime=1000;}
	virtual ~WOBatch() {}
  	virtual void EvalMass(Nixe::DCMInterface *mass);
  
	virtual void EvalForward(bool evalJac, double time, double* states, double *parameters, 
		double *rhsStates, double* sens, Nixe::DCMInterface *jacobian, 
			double* rhsStatesDtime, double* rhsSens, const Nixe::ProblemInfo& problemInfo) const;

	virtual void EvalReverse(double time, double *states, double* parameters, double* sens, double* adjoints, 
			double * rhsAdjoints, double* rhsDers, const Nixe::ProblemInfo& problemInfo) const;

	virtual void EvalInitialValues(double time, double *parameters, double* states, double* sens, 
			const Nixe::ProblemInfo& problemInfo);

	virtual void EvalFinalValues(double time, double *states, double *parameters, 
			double* sens, double* adjoints, const Nixe::ProblemInfo& problemInfo);

	virtual NIXE_SHARED_PTR<Nixe::ProblemInfo> GetProblemInfo();

	virtual void EvalParameters(double *parameters,const Nixe::ProblemInfo& problemInfo);
	void SetIndex(const int index) {m_Index=index;}
	void SetStates(NIXE_SHARED_PTR<Nixe::Array<double> > states) {m_States=states;}
	void SetAdjoints(NIXE_SHARED_PTR<Nixe::DMatrix > adjoints) {m_Adjoints=adjoints;}

private:
    double m_FinalTime;;
	const int m_LenGrid;
	const int m_Degree;
	const bool m_DoReverse;
	NIXE_SHARED_PTR<Nixe::Array<double> > m_States;
	NIXE_SHARED_PTR<Nixe::DMatrix > m_Adjoints;
	int m_Index;
}; 


#endif // WO_BATCH_HPP__
