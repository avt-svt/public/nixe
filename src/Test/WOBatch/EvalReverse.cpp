#include "WOBatch.hpp"
#include "NixeUtil.hpp"

void a1_rhs(int& bmode_1, double* f, double* b1_f, double* x, double* b1_x, double* u, double* b1_u);




void  WOBatch::EvalReverse(double time, double *states, double* parameters, double* sens, double* adjoints,
				  double * rhsAdjoints, double* rhsDers, const Nixe::ProblemInfo& problemInfo) const {
	const int n_p=problemInfo.NumParams();
	const int n_x=problemInfo.NumStates();
	const int n_lam1 = 1;  // number of first-order adjoints

	double *x = states;
    double *p = parameters;

	int n=n_x;
	int n_obj=n_lam1;
	int m=n_p;

    int bmode_1=1;
	Nixe::Array<double> f(n,0.0);
	Nixe::Array<double> b1_f(n);
	double *b1_x = 0;
	double *b1_p = 0;
	for (int i=0; i<n_obj; i++){
		b1_x=&rhsAdjoints[i*n];
		Nixe::VectorCopy(n,b1_x,0.0);
		b1_p=&rhsDers[i*m];
		Nixe::VectorCopy(m,b1_p,0.0);
		Nixe::VectorCopy(n,b1_f.Data(),&adjoints[i*n]);
		a1_rhs(bmode_1,f.Data(),b1_f.Data(),x,b1_x,p,b1_p);
	}



	return;
}

