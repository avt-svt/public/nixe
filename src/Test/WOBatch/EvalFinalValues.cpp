#include "WOBatch.hpp"
#include "NixeUtil.hpp"

void WOBatch::EvalFinalValues(double time, double *states, double *parameters,
		double* sens, double* adjoints, const Nixe::ProblemInfo& problemInfo) {

	// phi(x)=x[8] is objective function
	const int nX=problemInfo.NumStates();
	if(m_Index==m_LenGrid-1){
		Nixe::VectorCopy(nX,adjoints,0.0);
		adjoints[8]=1.0;
	}
	else{
		Nixe::VectorCopy(nX,adjoints,m_Adjoints->Data());
	}
}
