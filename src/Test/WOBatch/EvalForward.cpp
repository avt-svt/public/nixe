#include "WOBatch.hpp"

void rhs(double* f, double* x, double* u);
void t1_rhs(double* f, double* d1_f, double* x, double* d1_x, double* u, double* d1_u);

void WOBatch::EvalForward(bool evalJac, double time, double* states, double *parameters,
			double *rhsStates, double* sens, Nixe::DCMInterface *jacobian, 
			double* rhsStatesDtime, double* rhsSens, const Nixe::ProblemInfo& problemInfo) const {
				
	const int n_p=problemInfo.NumParams();
	const int n_x=problemInfo.NumStates();
	const int n_sens=problemInfo.NumSens();

	// eval f(x,p)
	double *f=rhsStates;
	double *x=states;
	double *p=parameters;
	double *u=p;
	int n=n_x;
	int m=n_p;
	rhs(f,x,u);

	// eval Dp f(x,p)
	
	// eval Jacobian if required
	if(evalJac) {
		Nixe::Array<double> d1_x(n);
		Nixe::Array<double> d1_p(m);
		double * d1_f=0;
		for(int j=0;j<m; j++)
			d1_p[j]=0.0;
		for (int i=0; i<n; i++){
			for(int j=0; j<n; j++)
				d1_x[j]=(i==j)?1.0:0.0;
			d1_f = &(jacobian->values[i*n]);
			t1_rhs(f,d1_f,x,d1_x.Data(),p,d1_p.Data());
	 	}
	}
};
