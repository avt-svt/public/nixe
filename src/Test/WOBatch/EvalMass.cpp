#include "WOBatch.hpp"
#include "NixeUtil.hpp"


void WOBatch::EvalMass(Nixe::DCMInterface *mass) {
	const int nX=mass->m;
	Nixe::VectorCopy(nX*nX,mass->values,0.0);
	//ODE
	for (int i=0; i<nX;i++)
		mass->values[i*(nX+1)]=1;
}

