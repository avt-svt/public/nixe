#include "WOBatch.hpp"
#include "NixeUtil.hpp"

void icd(double *x0);

void WOBatch::EvalInitialValues(double time, double *parameters, double* states, double* sens,
			const Nixe::ProblemInfo& problemInfo)
{
	const int nX=problemInfo.NumStates();
	if(m_Index==0)
		icd(states);
	else
		Nixe::VectorCopy(nX,states,m_States->Data());
}

