#include "HybridBioReactorDense.hpp"


void BioreactorDae::UpdateDersAdjoints(double *adjoints, double* ders, double t, 
									   double *x, double* p, double *sens, double *gradSwitch, int mode1, int mode2) 
{
	NIXE_SHARED_PTR<Nixe::ProblemInfo> problemInfo= this->GetProblemInfo();
	const int n_x=problemInfo->NumStates();
	const int n_s=problemInfo->NumSens();
	const int n_p=problemInfo->NumParams();
	const int n_lam = problemInfo->NumAdjoints();
	const int n_lam1 = 2;
	const int n_lam2 = n_lam-n_lam1;
	
	double f[2];
	double fd2[2];
	double fd1[2];
	double sigmad;
	double sigma;
	double pb2[9];
	double pb1[9];
	Nixe::Array<double> pd(n_p,0.0);
	Nixe::DMatrix rhsAdjoints2(n_x,n_lam1);
	Nixe::DMatrix rhsAdjoints1(n_x,n_lam1);
	Nixe::DMatrix mu_p(n_p,n_lam1,0.0);
	
	
	rhs(f,x,p,mode1);
	sigmafcn_d(&sigma,&sigmad,x,f,p,pd.Data()); // sigma_x dot f

	for (int i=0; i <n_lam1; i++) {
		for(int j=0; j<n_p; j++){
			Nixe::Array<double> pd(n_p,0.0);
			pd[j]=1.0;
			rhs_d(f,fd2,x,sens+j*n_x,p,pd.Data(),mode2);
			rhs_d(f,fd1,x,sens+j*n_x,p,pd.Data(),mode1);
			for(int k=0; k<n_x;k++)
				mu_p(j,i)+=adjoints[k+n_x*i]*(fd2[k]-fd1[k])/sigmad;

		}
	}

	Nixe::Array<double> sigma_x(n_x,0.0);
	for(int i=0; i<n_x; i++){
		Nixe::Array<double> xd(n_x,0.0);
		Nixe::VectorCopy(n_p,pd.Data(),0.0);
		xd[i]=1.0;
		sigmafcn_d(&sigma,&(sigma_x[i]),x,xd.Data(),p,pd.Data());
	}

	double *pt_mu_p = mu_p.Data();
	for (int i=0; i< n_lam1; i++)
		for (int j=0; j< n_p; j++)
			for (int k=0; k<n_x; k++)
				adjoints[4+i*n_p*n_x+j*n_x +k] += mu_p(j,i)*sigma_x[k];


	// compute sigma_p ..
	Nixe::Array<double> sigma_p(n_p,0.0);
	for(int i=0; i<n_p; i++){
		Nixe::Array<double> xd(n_x,0.0);
		Nixe::VectorCopy(n_p,pd.Data(),0.0);
		pd[i]=1.0;
		sigmafcn_d(&sigma,&(sigma_p[i]),x,xd.Data(),p,pd.Data());
	}

	for (int i=0; i< n_lam1; i++)
		for (int j=0; j< n_p; j++)
			for (int k=0; k<n_p; k++)
				ders[18+i*n_p*n_p+j*n_p +k] += mu_p(j,i)*sigma_p[k];
	



}