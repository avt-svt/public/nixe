#include "HybridBioReactorDense.hpp"


void BioreactorDae::EvalInitialValues(double time, double *parameters, double *states, double *sens, const Nixe::ProblemInfo &problemInfo) {
	
	const int n_x=problemInfo.NumStates();
	const int n_s=problemInfo.NumSens();
	const double *p=parameters;
	if (time ==0) {
		states[0]=p[6];
		states[1]=p[7];

		Nixe::VectorCopy(n_x*n_s,sens,0.0);
		sens[n_x*6]=1.0;
		sens[n_x*7+1]=1.0;
	}
	else {
		Nixe::VectorCopy(n_x,states,m_States->Data());
		Nixe::VectorCopy(n_x*n_s,sens,m_Sens->Data());
	}


}