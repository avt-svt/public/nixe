#include "HybridBioReactorDense.hpp"

NIXE_SHARED_PTR<Nixe::ProblemInfo> BioreactorDae::GetProblemInfo() {

	NIXE_SHARED_PTR<Nixe::ProblemInfo> temp(new Nixe::ProblemInfo);
	temp->SetStartTime(this->m_StartTime);
	temp->SetNumSens(9);
	temp->SetNumParams(9);
	temp->SetNumAdjoints(20);
	temp->SetNumDers(2*9*9+2*9);
	temp->SetNumStates(2);
	temp->SetNumSaveSens(9);
	temp->SetSave(true);
	temp->SetNumSaveSens(9);
	temp->SetNumSwitchFctns(1);
	temp->SetFinalTime(1.0);

	return temp;
}