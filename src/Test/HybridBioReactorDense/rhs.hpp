#ifndef _RHS_INCLUDE_ /* allow multiple inclusions */
#define _RHS_INCLUDE_ 

void rhs(double* f, double* x, double* p, int mode);
void rhs_d(double *f, double *fd, double *x, double *xd, double *p, double *pd
        , int mode);
void rhs_b(double *f, double *fb, double *x, double *xb, double *p, double *pb
        , int mode);
void rhs_b_d(double *f, double *fb, double *fbd, double *x, double *xd, double
        *xb, double *xbd, double *p, double *pd, double *pb, double *pbd, int 
        mode);

void sigmafcn(double* sigma, double *x, double *p);

void sigmafcn_d(double *sigma, double *sigmad, double *x, double *xd, double *
        p, double *pd) ;

void hamilton_d(double *h, double *hd, double *x, double *xd, double *lam, 
        double *lamd, double *p, double *pd, int mode);


#endif