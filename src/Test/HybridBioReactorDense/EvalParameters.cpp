#include "HybridBioReactorDense.hpp"

void BioreactorDae::EvalParameters(double *parameters, const Nixe::ProblemInfo &problemInfo) {

	const int n_p = problemInfo.NumParams();
	Nixe::VectorCopy(n_p,parameters,m_Parameters.Data());
}