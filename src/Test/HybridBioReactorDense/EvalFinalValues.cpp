#include "HybridBioReactorDense.hpp"

void BioreactorDae::EvalFinalValues(double time, double *states, double *parameters, double *sens, double *adjoints, const Nixe::ProblemInfo &problemInfo) {
	
	const int n_p=problemInfo.NumParams();
	const int n_x=problemInfo.NumStates();
	const int n_s=problemInfo.NumSens();
	const int n_l=problemInfo.NumAdjoints();

	if(time==1.0) {
		Nixe::VectorCopy(n_x*n_l,adjoints,0.0);
		adjoints[0]=1;
		adjoints[3]=1;
	}
	else {
		Nixe::VectorCopy(n_x*n_l,adjoints,m_Adjoints->Data());
	}
}