#ifndef HYBRID_BIO_REACTOR_DENSE_INCLUDE_ /* allow multiple inclusions */
#define HYBRID_BIO_REACTOR_DENSE_INCLUDE_ 

#include "NixeDaeInterface.hpp"
#include "NixeDenseColumnMajor.hpp"
#include "NixeUtil.hpp"
#include "rhs.hpp"



class BioreactorDae : public Nixe::DaeInterface<Nixe::DCMInterface> {
public:

	BioreactorDae(int mode, Nixe::Array<double> parameters) : m_Mode(mode), 
		m_Parameters(parameters), m_StartTime(0) {};
	virtual void EvalMass(Nixe::DCMInterface *mass);

	virtual void EvalForward(bool evalJac, double time, double* states, double *parameters, 
			double *rhsStates, double* sens, Nixe::DCMInterface* jacobian, 
			double* rhsStatesDtime, double* rhsSens, const Nixe::ProblemInfo& problemInfo) const; 


			virtual void EvalReverse(double time, double *states, double* parameters, double* sens, double* adjoints, 
			double * rhsAdjoints, double* rhsDers, const Nixe::ProblemInfo& problemInfo) const;
		
		virtual void EvalInitialValues(double time, double *parameters, double* states, double* sens, 
			const Nixe::ProblemInfo& problemInfo);
		
		virtual void EvalFinalValues(double time, double *states, double *parameters, 
			double* sens, double* adjoints, const Nixe::ProblemInfo& problemInfo);
		

		virtual void EvalSwitchingFunction(double time, double *states, double *parameters, 
			double *switchingFctn, const Nixe::ProblemInfo& problemInfo) const;

		virtual void EvalParameters(double *parameters, const Nixe::ProblemInfo& problemInfo);

		virtual NIXE_SHARED_PTR<Nixe::ProblemInfo> GetProblemInfo();


		void SetInitialStates(NIXE_SHARED_PTR<Nixe::Array<double> > states) {m_States=states;}
		void SetInitialSens(NIXE_SHARED_PTR<Nixe::DMatrix> sens) {m_Sens=sens;}
		void SetStartTime(double time) {m_StartTime=time;}
		void SetMode(int mode) {m_Mode=mode;}
		void SetAdjoints(NIXE_SHARED_PTR<Nixe::DMatrix > adjoints) {m_Adjoints=adjoints;}

		void CompGradSwitchTime(double* grad, double t, double* x, double* p, double *xp);
		void UpdateDersAdjoints(double *adjoints, double* ders, double t, double *x, double* p, double *sens, double *gradSwitch, int mode1, int mode2) ;

private:
	int m_Mode;
	Nixe::Array<double> m_Parameters;
	NIXE_SHARED_PTR<Nixe::Array<double> > m_States;
	NIXE_SHARED_PTR<Nixe::DMatrix > m_Sens;
	NIXE_SHARED_PTR<Nixe::DMatrix > m_Adjoints;
	double m_StartTime;

};



#endif