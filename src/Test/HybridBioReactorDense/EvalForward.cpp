#include "HybridBioReactorDense.hpp"


void BioreactorDae::EvalForward(bool evalJac, double time, double *states, 
								double *parameters, double *rhsStates, double *sens, 
								Nixe::DCMInterface *jacobian, double *rhsStatesDtime, 
								double *rhsSens, const Nixe::ProblemInfo &problemInfo) const {

	int mode = m_Mode;

	const int n_p=problemInfo.NumParams();
	const int n_x=problemInfo.NumStates();
	const int n_s=problemInfo.NumSens();

	// eval f(x,p)
	double *f=rhsStates;
	double *x=states;
	double *p=parameters;
	rhs(f,x,p,mode);

	// eval Dp f(x,p)
	
	for(int i=0; i<n_s; i++){
		Nixe::Array<double> d1_p(n_p,0);
		d1_p[i]=1.0;
		double *d1_f = &(rhsSens[n_x*i]);
		double *d1_x = &(sens[n_x*i]);
		rhs_d(f,d1_f,x,d1_x,p,d1_p.Data(),mode);
	}
	
	// eval Jacobian if required

	if(evalJac) {
		Nixe::Array<double> d1_x(n_x);
		Nixe::Array<double> d1_p(n_p,0);
		double * d1_f=0;
		for (int i=0; i<n_x; i++){
			for(int j=0; j<n_x; j++)
				d1_x[j]=(i==j)?1.0:0.0;
			d1_f = &(jacobian->values[i*n_x]);
			rhs_d(f,d1_f,x,d1_x.Data(),p,d1_p.Data(),mode);
		}
	}

};