#include "HybridBioReactorDense.hpp"

void BioreactorDae::CompGradSwitchTime(double* grad, double t, double* x, double* p, double *xp) {
	NIXE_SHARED_PTR<Nixe::ProblemInfo> problemInfo= this->GetProblemInfo();
	const int n_x=problemInfo->NumStates();
	const int n_s=problemInfo->NumSens();
	const int n_p=problemInfo->NumParams();
	Nixe::Array<double> rhsStates(n_x);
	Nixe::Array<double> rhsDTime(n_x);
	Nixe::DenseMatrix<double> rhsSens(n_x,n_s);
	Nixe::DenseColumnMajor jac(n_x,n_x,n_x*n_x);
	Nixe::Array<double> sigmad(n_s);
	Nixe::Array<double> pd(n_p);
	double sigma;
	double quot;


	this->EvalForward(true,t,x,p,rhsStates.Data(),xp,jac.Interface(),rhsDTime.Data(),rhsSens.Data(),*problemInfo);

	for (int i=0; i<n_s; i++) {
		Nixe::VectorCopy(n_p,pd.Data(),0.0);
		pd[i]=1.0;
		sigmafcn_d(&sigma,&(sigmad[i]),x,xp+n_x*i,p,pd.Data());
	}

	Nixe::VectorCopy(n_p,pd.Data(),0.0);
	sigmafcn_d(&sigma,&quot,x,rhsStates.Data(),p,pd.Data());

	for(int i=0; i<n_s;i++)
		grad[i]=-sigmad[i]/quot;
}