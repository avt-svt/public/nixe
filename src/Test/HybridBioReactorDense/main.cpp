#include "NixeMatrix.hpp"
#include "NixeCompressedColumn.hpp"
#include "NixeDenseColumnMajor.hpp"
#include "NixeDaeInterface.hpp"
#include "NixeCCSInterface.hpp"
#include "NixeForwardSolver.hpp"
#include <iostream>
#include "NixeAdjointData.hpp" 
#include <fstream> 
#include <ctime>
#include "HybridBioReactorDense.hpp"
#include "NixeProblemInfo.hpp"
#include "NixeUtil.hpp"
#include "NixeReverseSolver.hpp"
#include "NixeDenseOutput.hpp"
#include <vector>


int main(){
	Nixe::Array<double> p(9);
	p[0]=0.89245;
	p[1]=0.01836;
	p[2]=0.3;
	p[3]=3.9340;
	p[4]=0.03325;
	p[5]=-60;
	p[6]= 3.00538;
	p[7]= 0.032399;
	p[8]=18.7;


	double x1,x2;
	Array<double> grad_x1(9);
	Array<double> grad_x2(9);
	DMatrix  hess_x1(9,9);
	DMatrix  hess_x2(9,9);

	std::vector<Nixe::Array<double> > gradVec;
	
	
	
	Nixe::Status status;
	int mode=0;
	NIXE_SHARED_PTR<BioreactorDae> dae(new BioreactorDae(mode,p));
	NIXE_SHARED_PTR<Options>  options(new Options);

	options->SetTypeSequence(Seq_Harmonic);
	options->SetForceLocationChange(true);
	options->SetTolSwitch(1e-12);
	options->SetComputeCIV(false);
	double currentTime=0;
	NIXE_SHARED_PTR<Nixe::Array<double> > states ;
	NIXE_SHARED_PTR<Nixe::DMatrix> sens;

	std::vector<NIXE_SHARED_PTR<Nixe::Checkpoints> > checkpointsVec;
	NIXE_SHARED_PTR<Nixe::Checkpoints> checkpoints;


	NIXE_SHARED_PTR<ForwardSolver<DenseColumnMajor> >nixeFor(new ForwardSolver<DenseColumnMajor>(dae,options));
	double t0=Nixe::timer();
	status=nixeFor->Solve();
	currentTime=nixeFor->GetCurrentTime();
	states = nixeFor->GetStates();
	sens = nixeFor->GetSens();
	checkpoints=nixeFor->GetCheckpoints();
	checkpointsVec.push_back(checkpoints);
	Nixe::Array<double> grad(9);

	dae->CompGradSwitchTime(grad.Data(),currentTime,states->Data(),p.Data(),sens->Data());
	gradVec.push_back(grad);

	while (currentTime < 1) {
		mode=(mode+1)%2;
		dae->SetInitialStates(states);
		dae->SetInitialSens(sens);
		dae->SetStartTime(currentTime);
		dae->SetMode(mode);
		nixeFor.reset(new ForwardSolver<DenseColumnMajor>(dae,options));
		
		status=nixeFor->Solve();
		
		currentTime=nixeFor->GetCurrentTime();
	    states = nixeFor->GetStates();
		sens = nixeFor->GetSens();
		if (currentTime < 1) {
			dae->CompGradSwitchTime(grad.Data(),currentTime,states->Data(),p.Data(),sens->Data());
			gradVec.push_back(grad);
		}
		checkpoints=nixeFor->GetCheckpoints();
		checkpointsVec.push_back(checkpoints);

	}

	x1 = (*states)[0];
	x2 = (*states)[1];
	const int numStages=(int) checkpointsVec.size();



	NIXE_SHARED_PTR<ReverseSolver<DenseColumnMajor> > nixeRev(new 
		ReverseSolver<DenseColumnMajor>(dae,checkpointsVec[numStages-1]));
		nixeRev->Solve();
		NIXE_SHARED_PTR<Array<double> > ders1=nixeRev->GetDerivatives();
		NIXE_SHARED_PTR<Array<double> > ders2;
		NIXE_SHARED_PTR<DenseMatrix<double> > adjoints=nixeRev->GetAdjoints();
		states=nixeRev->GetStates();
		sens=nixeRev->GetSens();
	for(int i=numStages-2; i>=0; i--) {
		int mode2=mode;
		mode=(mode+1)%2;
		int mode1=mode;
		dae->SetMode(mode);
		dae->UpdateDersAdjoints(adjoints->Data(),ders1->Data(),0,states->Data(),p.Data(),sens->Data(),0,mode1,mode2);
		dae->SetAdjoints(adjoints);
		nixeRev.reset(new ReverseSolver<DenseColumnMajor>(dae,checkpointsVec[i]));
		nixeRev->Solve();
		ders2=nixeRev->GetDerivatives();
		adjoints=nixeRev->GetAdjoints();
		
		for (int i=0; i<ders1->Size();i++)
			(*ders1)[i]+=(*ders2)[i];
   
	}


	for(int i=0; i<20; i++){
		(*ders1)[9*i+6]+=(*adjoints)(0,i);
		(*ders1)[9*i+7]+=(*adjoints)(1,i);
	}



	
	Nixe::VectorCopy(9,grad_x1.Data(),ders1->Data());
	Nixe::VectorCopy(9,grad_x2.Data(),ders1->Data()+9);
	Nixe::VectorCopy(81,hess_x1.Data(),ders1->Data()+18);
	Nixe::VectorCopy(81,hess_x2.Data(),ders1->Data()+18+81); 


	return 0;
}