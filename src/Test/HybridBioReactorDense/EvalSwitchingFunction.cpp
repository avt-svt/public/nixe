#include "HybridBioReactorDense.hpp"


void BioreactorDae::EvalSwitchingFunction(double time, double *states, double *parameters, double *switchingFctn, const Nixe::ProblemInfo &problemInfo) const {

  double *p=parameters;
  double *x=states;

  sigmafcn(switchingFctn,x,p);
  

}