#include "HybridBioReactorDense.hpp"


void BioreactorDae::EvalReverse(double time, double *states, double *parameters, double *sens, double *adjoints, double *rhsAdjoints, double *rhsDers, const Nixe::ProblemInfo &problemInfo) const {
	const int n_p=problemInfo.NumParams();
	const int n_x=problemInfo.NumStates();
	const int n_s=problemInfo.NumSens();

	const int n_lam1 = 2;  // number of first-order adjoints
	const int n_lam2 = problemInfo.NumAdjoints()-n_lam1; // number of second-order adjoints
//	const int n_ders1 = n_lam1*n_p; // dimension of gradients
//	const int n_ders2 = problemInfo.NumDers() - n_ders1; // dimension of Hessians

	double *x = states;
    double *p = parameters;

	const int n=n_x;
	int n_obj=n_lam1;
	int m=n_p;
	int mode = m_Mode;
 
	Nixe::Array<double> f(n);
	Nixe::Array<double> b1_f(n);
	double *b1_x = 0;
	double *b1_p = 0;
	for (int i=0; i<2; i++){
		b1_x=&rhsAdjoints[i*n];
		Nixe::VectorCopy(n,b1_x,0.0);
		b1_p=&rhsDers[i*m];
		Nixe::VectorCopy(m,b1_p,0.0);
		Nixe::VectorCopy(n,b1_f.Data(),&adjoints[i*n]);
		rhs_b(f.Data(),b1_f.Data(),x,b1_x,p,b1_p,mode);
	}

	if(n_lam2 > 0) {
		Nixe::Array<double> f(n);
		Nixe::Array<double> d2_f(n);
		Nixe::Array<double> b1_f(n);
		double * d2_b1_f=0;
		double * d2_x=0;
		Nixe::Array<double> b1_x(n);
		double * d2_b1_x=0;
		Nixe::Array<double> d2_p(m);
		Nixe::Array<double> b1_p(m);
		double * d2_b1_p=0;

		for(int i=0; i<n_obj;i++){
			for(int j=0; j<m;j++){
				d2_b1_f=&adjoints[n_lam1*n_x+i*n*m+j*n];
				d2_x = &sens[j*n];
				d2_b1_x =&rhsAdjoints[n_lam1*n_x+i*n*m+j*n];
				d2_b1_p =&rhsDers[n_lam1*n_p+i*m*m+j*m];
				for(int k=0; k<m;k++){
					d2_p[k]=(j!=k)?0.0:1.0;
				}
				Nixe::VectorCopy(n,b1_f.Data(),&adjoints[i*n]);
				Nixe::VectorCopy(n,b1_x.Data(),0.0);
				Nixe::VectorCopy(n,d2_b1_x,0.0);
				Nixe::VectorCopy(m,b1_p.Data(),0.0);
				Nixe::VectorCopy(m,d2_b1_p,0.0);
			

				rhs_b_d(f.Data(),b1_f.Data(),d2_b1_f,x,d2_x,b1_x.Data(),
					d2_b1_x,p,d2_p.Data(),b1_p.Data(),d2_b1_p,mode);
   
			} // end for j
		}  // end for i
	}  // end if n_lam2

	return;
}