#include "NixeDenseColumnMajor.hpp"
#include "NixeForwardSolver.hpp"
#include <iostream>
#include <iomanip>
#include "NixeAdjointData.hpp" 
#include "Turanyi.hpp"
#include "NixeUtil.hpp"
#include "NixeReverseSolver.hpp"
#include <fstream>
#include "NixeMatrix.hpp"

#define NUM_IMPORTANT 5
static double g_Scale[5]={6.26e5,1.56e15,2.61e9,1.39e13,1.17e14};
static double g_Index[NUM_IMPORTANT]={0,1,2,3,4};


using namespace Nixe;  


double randn_notrig(double mu=0.0, double sigma=1.0) {
        static bool deviateAvailable=false;        //        flag
        static float storedDeviate;                        //        deviate from previous calculation
        double polar, rsquared, var1, var2;

        //        If no deviate has been stored, the polar Box-Muller transformation is
        //        performed, producing two independent normally-distributed random
        //        deviates.  One is stored for the next round, and one is returned.
        if (!deviateAvailable) {

                //        choose pairs of uniformly distributed deviates, discarding those
                //        that don't fall within the unit circle
                do {
                        var1=2.0*( double(rand())/double(RAND_MAX) ) - 1.0;
                        var2=2.0*( double(rand())/double(RAND_MAX) ) - 1.0;
                        rsquared=var1*var1+var2*var2;
                } while ( rsquared>=1.0 || rsquared == 0.0);

                //        calculate polar tranformation for each deviate
                polar=sqrt(-2.0*log(rsquared)/rsquared);

                //        store first deviate and set flag
                storedDeviate=var1*polar;
                deviateAvailable=true;

                //        return second deviate
                return var2*polar*sigma + mu;
        }

        //        If a deviate is available from a previous call to this function, it is
        //        returned, and the flag is set to false.
        else {
                deviateAvailable=false;
                return storedDeviate*sigma + mu;
        }
}

void GetCoefficients(NIXE_SHARED_PTR<Nixe::Array<double> > states,
		NIXE_SHARED_PTR<Nixe::DMatrix>& coefficients,
		NIXE_SHARED_PTR<Nixe::Array<double> >& ders){
	const int numParams=5;
	const int n_x=12;
	double p[numParams]={0};
	double t1_x[n_x]={0};

	Turanyi dae;
	NIXE_SHARED_PTR<Nixe::ProblemInfo> problemInfo=dae.GetProblemInfo();
	dae.EvalParameters(p,(*problemInfo));

	coefficients.reset(new Nixe::DMatrix(n_x,numParams,0));
	ders.reset(new Nixe::Array<double>(n_x,0));
	double *f=ders->Data();
	double *x= states->Data();

	for(int i=0; i<numParams; i++){
		double * t1_f = coefficients->Col(i);
		Nixe::Array<double> t1_p(numParams,0);
		t1_p[i]=1;

		t1_rhs(f, t1_f, x, t1_x, p, t1_p.Data(), n_x, numParams);
	}
	rhs(f,x,p,n_x,numParams);

	return;
}

NIXE_SHARED_PTR<Nixe::DMatrix> ComputeH (std::vector<NIXE_SHARED_PTR<Nixe::DMatrix> >& A){
	int numPars=5;
	int numImportant=NUM_IMPORTANT;
	const int numPoints=(int) A.size();
	NIXE_SHARED_PTR<Nixe::DMatrix > H(new Nixe::DMatrix(numPars,numPars,0));

	for(int l=0; l<numPoints;l++){
		for (int i=0; i<numPars; i++){
			for(int j=0; j<numPars;j++) {
				for(int k=0; k<numImportant;k++)
					(*H)(i,j) += g_Scale[i]*(*A[l])(g_Index[k],i) * (*A[l])(g_Index[k],j) * g_Scale[j];
			}
		}
	}

	return H;
}

NIXE_SHARED_PTR<Nixe::Array<double> > ComputeC
	(std::vector<NIXE_SHARED_PTR<Nixe::Array<double> > >& f,
	 std::vector<NIXE_SHARED_PTR<Nixe::DMatrix> >& A)
{
	const int numImportant=NUM_IMPORTANT;
	const int numParams=5;
	NIXE_SHARED_PTR<Nixe::Array<double> > C(new Nixe::Array<double>(numParams,0));
	const int numPoints=(int) f.size();


	for(int l=0; l<numPoints;l++){
		for(int i=0; i<numParams;i++){
			for(int j=0; j<numImportant;j++){
				(*C)[i] += -(*f[l])[g_Index[j]] * (*A[l])(g_Index[j],i)*g_Scale[i];
			}
		}

	}

	return C;
}


int main(int argc, char *argv[])
{
	Nixe::Status status;
	NIXE_SHARED_PTR<Nixe::Array<double> > states;

    NIXE_SHARED_PTR<Turanyi> dae(new Turanyi);
	NIXE_SHARED_PTR<Options>  options(new Options);

    options->SetAbsTolScalar(1e-14);
	options->SetTypeSequence(Seq_Harmonic);
	options->SetComputeCIV(false);

	const int numPoints=11;

	std::vector<NIXE_SHARED_PTR<Nixe::DMatrix> > A(numPoints);
	std::vector<NIXE_SHARED_PTR<Nixe::Array<double> > > f(numPoints);

	for(int i=0; i < numPoints; i++){
		double finalTime= (double) i / ( (double) (numPoints-1));
		dae->SetFinalTime(finalTime);
	    NIXE_SHARED_PTR<ForwardSolver<FastDenseColumnMajor> >nixeFor(new ForwardSolver<FastDenseColumnMajor>(dae,options));
 
	    status=nixeFor->Solve();

	    states=nixeFor->GetStates();

	    states->PrintToFile();

	    GetCoefficients(states,A[i],f[i]);
	}

	NIXE_SHARED_PTR<Nixe::DMatrix> H=ComputeH(A);
	NIXE_SHARED_PTR<Nixe::Array<double> > C=ComputeC(f,A);

	H->PrintToFile("H.txt");
	C->PrintToFile("f.txt");
	return 0;
}

