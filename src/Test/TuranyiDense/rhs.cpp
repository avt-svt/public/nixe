

void rhs(double* f, double* x, double* p, int n, int m)
{
  // f[n] : out, right hand side of DAE
  // x[n] : in, state vector
  // p[m] : in, parameter vector
  // n    : in, dimension of f and x
  // m	  : in, dimension of p

 double Br2=0;
 double Br=0;
 double H2=0;
 double H=0;
 double HBr=0;
 double X=0;
 double k1=0;
 double k2=0;
 double k3=0;
 double k4=0;
 double k5=0;




 Br2 = x[0];
 Br  = x[1];
 H2  = x[2];
 H   = x[3];
 HBr = x[4];
 X   = x[5];

 k1=p[0];
 k2=p[1];
 k3=p[2];
 k4=p[3];
 k5=p[4];

 f[0] = -k1*Br2*X + k2* Br*Br*X - k5 *H*Br2;
 f[1]   = 2*k1*Br2*X - 2*k2*Br*Br*X - k3*Br*H2 + k4*H*HBr + k5*H*Br2;
 f[2]   = - k3*Br*H2 + k4*H*HBr;
 f[3]    = k3*Br*H2 - k4*H*HBr - k5*H*Br2;
 f[4]  = k3*Br*H2 - k4*H*HBr + k5*H*Br2;
 f[5]    = 0;

 // integration of variables
 f[6] = x[0];
 f[7] = x[1];
 f[8] = x[2];
 f[9] = x[3];
 f[10] = x[4];
 f[11] = x[5];

/* // df[0]/dfk[i]
 f[6]  = -Br2*X;
 f[7]  = Br*Br*X;
 f[8]  = 0;
 f[9]  = 0;
 f[10] = -H*Br2;
 // df[1]/dfk[i]
 f[11] = 2*Br2*X;
 f[12] = -2*Br*Br*X;
 f[13] = -Br*H2;
 f[14] = H*HBr;
 f[15] = k5*H*Br2;
 // df[2]/dfk[i]
 f[16] = 0;
 f[17] = 0;
 f[18] = -Br*H2;
 f[19] = H*HBr;
 f[20] = 0;
 // df[3]/dfk[i]
 f[21] = 0;
 f[22] = 0;
 f[23] = Br*H2;
 f[24] = -H*HBr;
 f[25] = -H*Br2;
 // df[3]/dfk[i]
 f[26] = 0;
 f[27] = 0;
 f[28] = Br*H2;
 f[29] = -H*HBr;
 f[30] = H*Br2;*/


}
