#ifndef TURANYI_DENSE_DCC_INCLUDE_ /* allow multiple inclusions */
#define TURANYI_DENSE_DCC_INCLUDE_ 

#include "NixeDCMInterface.hpp"
#include "NixeDaeInterface.hpp"

void t1_rhs(double* f, double* t1_f, double* x, double* t1_x, double* p, double* t1_p, int n, int m);

void rhs(double* f, double* x, double* p, int n, int m);

class Turanyi : public  Nixe::DaeInterface<Nixe::DCMInterface> {
public:

	Turanyi() : m_FinalTime(1) {}

  virtual void EvalMass(Nixe::DCMInterface *mass){
	  Nixe::VectorCopy(mass->m*mass->n,mass->values,0.0);
	  for(int i=0;i<12;i++)
		  mass->values[mass->m*i+i]=1.0;
  }
  


	

  

	virtual void EvalForward(bool evalJac, double time, double* states, double *parameters, 
		double *rhsStates, double* sens, Nixe::DCMInterface *jacobian, 
			double* rhsStatesDtime, double* rhsSens, const Nixe::ProblemInfo& problemInfo) const;

	virtual void EvalReverse(double time, double *states, double* parameters, double* sens, double* adjoints, 
			double * rhsAdjoints, double* rhsDers, const Nixe::ProblemInfo& problemInfo) const;

			virtual void EvalSwitchingFunction(double time, double *states, double *parameters, 
			double *switchingFctn, const Nixe::ProblemInfo& problemInfo) const;

	virtual void EvalInitialValues(double time, double *parameters, double* states, double* sens, 
			const Nixe::ProblemInfo& problemInfo);

	virtual void EvalFinalValues(double time, double *states, double *parameters, 
			double* sens, double* adjoints, const Nixe::ProblemInfo& problemInfo);

	virtual NIXE_SHARED_PTR<Nixe::ProblemInfo> GetProblemInfo();

	virtual void EvalParameters(double *parameters,const Nixe::ProblemInfo& problemInfo);

	virtual ~Turanyi(){};

	void SetFinalTime(double finalTime) {m_FinalTime=finalTime;}

private:
	double m_FinalTime;

 
}; 


#endif //TURANYI_DENSE_DCC_INCLUDE_
