#include "NixeDenseColumnMajor.hpp"
#include "NixeForwardSolver.hpp"
#include <iostream>
#include <iomanip>
#include "NixeAdjointData.hpp" 
#include "HeatingStorageTankDense.hpp"
#include "NixeUtil.hpp"
#include "NixeReverseSolver.hpp"
#include <fstream>
#include <string>


using namespace Nixe;  


void WriteOrdersAndStepSizes(const char* fileName, Nixe::Checkpoints& check)
{
	std::ofstream outf(fileName);

	outf.setf(std::ios_base::scientific, std::ios_base::floatfield);
	outf.precision(16);
	const int top=check.top;

	outf << "double h[" << top << "] = { ";
	for(int i=0; i< top; i++) {
		std::string delimiter;
		if (i < top-1)
			delimiter=", \n";
		else
			delimiter=" };\n";
		outf << check.storage[i]->stepSize << delimiter;
	}

	outf << "int ord[" << top << "] = { ";
	for(int i=0; i< top; i++) {
		std::string delimiter;
		if (i < top-1)
			delimiter=", \n";
		else
			delimiter=" };\n";
		outf << check.storage[i]->order << delimiter;
	}


	return;
}



bool ComputedHessianIsCorrect(Nixe::DMatrix& computedHessian, Nixe::DMatrix& referenceHessian){
	const double relTol=1e-4;
	const double absTol=1e-4;
	

	double error=0;
	
	Nixe::DMatrix& A= computedHessian;
	Nixe::DMatrix& B=referenceHessian;
	for(int i=0; i<B.NumRows();i++){
		for(int j=0; j< B.NumRows(); j++){
			double currentError= fabs(A(i,j)-B(i,j))/(relTol*fabs(B(i,j)) + absTol);
			error = Nixe::Max(error,currentError);
		}
	}

	if (error < 1)
		return true;
	else
		return false;

}


int main(int argc, char *argv[])
 {

 /************** load Reference Hessian **************/
	const int n=33;
	int nT=50;
	int nTau=10;
	int nTheta=10;
	const double someStrangeValue=-17; // strange value for debugging purposes
	Nixe::DMatrix referenceHessian(n,n,someStrangeValue);
	if(argc > 1){
	 std::ifstream inputFile(argv[1]);
	 for(int i=0; i<n; i++)
	  	for(int j=0; j<n; j++)
			inputFile >> referenceHessian(i,j);
	 inputFile.close();
	} // if (argc > 1)
	Nixe::Status status;
	NIXE_SHARED_PTR<Nixe::Array<int> > indizesRoots;

	int degree=0;  // degree of forward sensitivity computation
	bool doReverse=true;
	double t0,t1,t2,t3;
	t0=timer();
	NIXE_SHARED_PTR<HSTDense> dae(new HSTDense(nT,nTau,nTheta,degree,doReverse));
	NIXE_SHARED_PTR<Options>  options(new Options);
	options->SetTypeSequence(Seq_Harmonic);
	options->SetAbsTolScalar(1e-5);
	options->SetRelTolScalar(1e-5);
	options->SetInitialStepSize(0.1);



	options->SetTypeSequence(Seq_ShiftedHarmonic);
	options->SetComputeCIV(false);

	NIXE_SHARED_PTR<ForwardSolver<FastDenseColumnMajor> >nixeFor(new ForwardSolver<FastDenseColumnMajor>(dae,options));

	status=nixeFor->Solve();
	t1=timer();
	t2=timer();
	NIXE_SHARED_PTR<ReverseSolver<FastDenseColumnMajor> > nixeRev;
	if(doReverse){
		NIXE_SHARED_PTR<Checkpoints> checkpoints= nixeFor->GetCheckpoints();
	//	WriteOrdersAndStepSizes("oh.txt",*checkpoints);
		nixeRev.reset(new ReverseSolver<FastDenseColumnMajor>(dae,checkpoints));
		nixeRev->Solve();
	}
	t3=timer();

	std::cout << "Forward degree   : " << degree << std::endl;
	std::cout << "do reverse sweep : " << doReverse << std::endl;
	std::cout << "cost for forward sweep :" << t1-t0 << std::endl;
	if(doReverse) std::cout << "cost for reverse sweep :" << t3-t2 << std::endl;

	  /******* output section ***********/

	NIXE_SHARED_PTR<Array<double> > stat=nixeFor->GetStates();
	stat->PrintToFile("stat.dat");
	if(degree > 0){
		NIXE_SHARED_PTR<DMatrix>  sens=nixeFor->GetSens();
		sens->PrintToFile("sens.dat");
	}

	if(doReverse) {
		
		NIXE_SHARED_PTR<Array<double> > ders=nixeRev->GetDerivatives();
		Array<double> grad(n,0);
		VectorCopy(n,grad.Data(), ders->Data());
		grad.PrintToFile("grad.dat");

		if(degree > 0) {
			DMatrix hess(n,n);
			VectorCopy(n*n,hess.Data(),ders->Data()+n); // first n entries belong to gradient
			hess.PrintToFile("hess.dat");	
			if (ComputedHessianIsCorrect(hess,referenceHessian) )
				return 0;
			else
				return 1;
		}
	}
	
	
	return 0;
}

