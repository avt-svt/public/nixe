#include <cmath>
#include <cfloat>
#include <iostream>
#include "dcc_log.hpp"
using namespace std;


double dcc_log(double& x)
{
	if (x<0){
		cout << "LOG NOT DEFINED" << endl;
		return DBL_MIN;
	}
	else if (x==0){
	  return (-1/x);
	}
	else{
		return log(x);
	}
}


