#include "HeatingStorageTankDense.hpp"

NIXE_SHARED_PTR<Nixe::ProblemInfo> HSTDense::GetProblemInfo() {
	NIXE_SHARED_PTR<Nixe::ProblemInfo> temp(new Nixe::ProblemInfo);
	const int n_T= m_NT;
	const int n_Tau= m_NTau;
	const int n_Theta=m_NTheta;
	const int nX=n_T + 2*n_Theta + 3*n_Tau + 16;
	const int nP=33;

	if(m_DoReverse) temp->SetSave(true);
	else temp->SetSave(false);
	temp->SetNumAdjoints(m_Degree* nP +1);

	temp->SetNumStates(nX);
	temp->SetNumNonZerosJac(nX*nX);
	temp->SetNumNonZerosMass(nX*nX);
	temp->SetNumDers(nP+m_Degree*nP*nP);
	temp->SetNumSwitchFctns(0);
	temp->SetNumSens(m_Degree*nP);
	temp->SetNumParams(nP);
	temp->SetStartTime(0);
	temp->SetFinalTime(1600);
	if(m_DoReverse)temp->SetNumSaveSens(m_Degree*nP);
	else temp->SetNumSaveSens(0);
	return temp;
}

