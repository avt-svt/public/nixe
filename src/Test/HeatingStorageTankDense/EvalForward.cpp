#include "HeatingStorageTankDense.hpp"

void rhs(double* f, double* x, double* u, int &n, int & m, int & n_T, int & n_Theta, int & n_Tau);
void t2_rhs(double* f, double* d2_f, double* x, double* d2_x, double* u, double* d2_u, int& n, int& m, int& n_T, int& n_Theta, int& n_Tau);

void HSTDense::EvalForward(bool evalJac, double time, double* states, double *parameters, 
			double *rhsStates, double* sens, Nixe::DCMInterface *jacobian, 
			double* rhsStatesDtime, double* rhsSens, const Nixe::ProblemInfo& problemInfo) const {
				
	const int n_p=problemInfo.NumParams();
	const int n_x=problemInfo.NumStates();
	const int n_sens=problemInfo.NumSens();
	int n_T=m_NT;
	int n_Tau=m_NTau;
	int n_Theta=m_NTheta;

	// eval f(x,p)
	double *f=rhsStates;
	double *x=states;
	double *p=parameters;
	int n=n_x;
	int m=n_p;
	rhs(f,x,p,n,m,n_T,n_Theta,n_Tau);

	// eval Dp f(x,p)
	
	for(int i=0; i<n_sens; i++){
		Nixe::Array<double> d1_p(m,0);
		d1_p[i]=1.0;
		double *d1_f = &(rhsSens[n_x*i]);
		double *d1_x = &(sens[n_x*i]);
		t2_rhs(f,d1_f,x,d1_x,p,d1_p.Data(),n,m,n_T,n_Theta,n_Tau);
	}
	
	// eval Jacobian if required
	if(evalJac) {
		Nixe::Array<double> d1_x(n);
		Nixe::Array<double> d1_p(m);
		double * d1_f=0;
		for(int j=0;j<m; j++)
			d1_p[j]=0.0;
		for (int i=0; i<n; i++){
			for(int j=0; j<n; j++)
				d1_x[j]=(i==j)?1.0:0.0;
			d1_f = &(jacobian->values[i*n]);
			t2_rhs(f,d1_f,x,d1_x.Data(),p,d1_p.Data(),n,m,n_T,n_Theta,n_Tau);
	 	}
	}
};
