#ifndef HEATING_STORAGE_TANK_HPP__ /* allow multiple inclusions */
#define HEATING_STORAGE_TANK_HPP__ 

#include "NixeDCMInterface.hpp"
#include "NixeDaeInterface.hpp"

class HSTDense : public  Nixe::DaeInterface<Nixe::DCMInterface> {
public:
	HSTDense(const int nT, const int nTau, const int nTheta, const int degree, const bool doReverse) :
		m_NT(nT), m_NTau(nTau), m_NTheta(nTheta), m_Degree(degree), m_DoReverse(doReverse) {}
	virtual ~HSTDense() {}
  	virtual void EvalMass(Nixe::DCMInterface *mass);
  
	virtual void EvalForward(bool evalJac, double time, double* states, double *parameters, 
		double *rhsStates, double* sens, Nixe::DCMInterface *jacobian, 
			double* rhsStatesDtime, double* rhsSens, const Nixe::ProblemInfo& problemInfo) const;

	virtual void EvalReverse(double time, double *states, double* parameters, double* sens, double* adjoints, 
			double * rhsAdjoints, double* rhsDers, const Nixe::ProblemInfo& problemInfo) const;

	virtual void EvalInitialValues(double time, double *parameters, double* states, double* sens, 
			const Nixe::ProblemInfo& problemInfo);

	virtual void EvalFinalValues(double time, double *states, double *parameters, 
			double* sens, double* adjoints, const Nixe::ProblemInfo& problemInfo);

	virtual NIXE_SHARED_PTR<Nixe::ProblemInfo> GetProblemInfo();

	virtual void EvalParameters(double *parameters,const Nixe::ProblemInfo& problemInfo);

private:
	const int m_NT;
	const int m_NTau;
	const int m_NTheta;
	const int m_Degree;
	const bool m_DoReverse;
}; 


#endif // HEATING_STORAGE_TANK_HPP__
