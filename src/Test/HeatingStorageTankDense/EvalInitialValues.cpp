#include "HeatingStorageTankDense.hpp"
#include "NixeUtil.hpp"

void icd(double *x0, double *u, int & n, int & m, int & n_T, int & n_Theta, int & n_Tau);
void t1_icd(double* x0, double* t1_x0, double* u, double* t1_u, int& n, int& m, int& n_T, int& n_Theta, int& n_Tau);

void HSTDense::EvalInitialValues(double time, double *parameters, double* states, double* sens, 
			const Nixe::ProblemInfo& problemInfo)
{
	int nP=problemInfo.NumParams();
	int nS=problemInfo.NumSens();
	int nX=problemInfo.NumStates();
	int n_T=m_NT;
	int n_Theta=m_NTheta;
	int n_Tau=m_NTau;

	icd(states,parameters,nX,nP,n_T,n_Theta,n_Tau);	

	for(int i=0; i<nS;i++) {
		Nixe::Array<double> t1_u(nP,0.0);
		t1_u[i]=1.0;
		t1_icd(states,&sens[i*nX],parameters,t1_u.Data(),nX,nP,n_T,n_Theta,n_Tau);
	}
}

