#include <cmath>
#include "dcc_log.hpp"

void icd(double *x0, double *u, int & n, int & m, int & n_T, int & n_Theta, int & n_Tau) 
//$ad indep u
//$ad dep x0 
{

  // x[n] : in, state vector
  //		x[0]=T_in0, x[1]=T_LTin, x[2]=Theta_in_pr, x[3]=Theta_out_pr, x[4]=Theta_in_sec, x[5]=Theta_out_sec, x[6]=Tau_in_pr, x[7]=Tau_out_pr, 
  //		x[8]=Tau_in_sec, x[9]=Tau_out_sec, x[10]=v, x[11]=v_g, x[12]=n_F, x[13]=delta_z, x[14]=delta_x1, x[15]=delta_x2,
  //		x[16 : 16+n_T-1]= T (Temp storage tank), x[16+n_T : 16+n_T+n_Theta-1]= Theta_pr (Temp heat exchanger part 1),
  //		x[16+n_T+n_Theta : 16+n_T+2*n_Theta-1]= Theta_sec (Temp heat exchanger part 2),
  //		x[16+n_T+2*n_Theta : 16+n_T+2*n_Theta+n_Tau-1]= Tau_pr (Temp burner part 1),
  //		x[16+n_T+2*n_Theta+n_Tau : 16+n_T+2*n_Theta+2*n_Tau-1]= Tau_w (Temp burner part w),
  //		x[16+n_T+2*n_Theta+2*n_Tau : 16+n_T+2*n_Theta+3*n_Tau-1]= Tau_sec (Temp burner part 2),
  // u[m] : in, parameter vector
  //		cfr function initializeParameters
  // n     : in, dimension of x0
  // m     : in, dimension of p
  int i=0;

  int cond=0;
  int cond_2=0;
  int cond_3=0;
  int cond_4=0;
  int cond_5=0;
  int cond_6=0;
  double exp_1 = 0;
 
  
  cond = 16+n_T-1;
  cond_2 = 16+n_T+n_Theta;
  cond_3 = 16+n_T+2*n_Theta-1;
  cond_4 = 16+n_T+2*n_Theta+n_Tau-1;
  cond_5 = 16+n_T+2*n_Theta+2*n_Tau;
  cond_6 = 16+n_T+2*n_Theta+3*n_Tau;

  
  x0[0]=284.996;
  x0[1]=285;
  x0[2]=285;
  x0[3]=285;
  x0[4]=285;
  x0[5]=284.996;
  exp_1=u[32];
  x0[6]=u[29]/(pow(u[25],exp_1) + u[30]) + u[31];
  x0[7]=285;
  x0[8]=285;
  x0[9]=285;
  x0[10]=(u[12]-u[13])/u[11];
  x0[11]=u[23]*u[25]/100;
  x0[12]=u[25];
  x0[13]=u[8]/(n_T-1);
  x0[14]=u[9]/(n_Theta-1);
  x0[15]=u[10]/(n_Tau-1);

  
  

 for(i=16; i<cond;i=i+1){
    x0[i] = 285;
  }
  x0[cond] = 285.0628461;


  for(i=16+n_T;i<cond_2;i=i+1){
	x0[i] = 285;
  }

  for(i=16+n_T+n_Theta;i<cond_3;i=i+1){
    x0[i] = 285;
    
  }
    x0[cond_3] = 284.996;


  for(i=16+n_T+2*n_Theta;i<cond_4;i=i+1){
    x0[i] = 285;

  }
     x0[cond_4] = x0[6];

  for(i=16+n_T+2*n_Theta+n_Tau;i<cond_5;i=i+1){
    x0[i] = 285;
  }
 
  for(i=16+n_T+2*n_Theta+2*n_Tau;i<cond_6;i=i+1){
    x0[i] = 285;
  }
}
