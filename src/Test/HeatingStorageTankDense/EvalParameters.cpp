#include "HeatingStorageTankDense.hpp"


void HSTDense::EvalParameters(double *parameters,const Nixe::ProblemInfo& problemInfo) {
  double *u=parameters;
  u[0]=293;    // T_amb
  u[1]=0.6204; // lambda_w
  u[2]=989.3;  // rho
  u[3]=4181;   // c
  u[4]=0.1954; // k_w
  u[5]=1.6897; // k_c
  u[6]=23.6556;// k_b
  u[7]=0.48;   // d
  u[8]=0.83;   // L_T
  u[9]=2.3276; // L_Theta
  u[10]=1.9894;// L_Tau
  u[11]=210.25;// a_barre
  u[12]=0.16;  // v_l
  u[13]=0.15;  // v_d
  u[14]=0.40;  // v_h
  u[15]=4079;  // alpha
  u[16]=0.022624;// r_pr
  u[17]=0.016; // r_sec
  u[18]=0.232; // r_w
  u[19]=0.4994;// gamma_pr
  u[20]=0.0751;// gamma_sec
  u[21]=2.4625;// q_pr
  u[22]=0.8101;// q_sec
  u[23]=0.4;   // v_gmax
  u[24]=0.9790;// n_Fmin
  u[25]=100;   // n_Fmax
  u[26]=50;    // n_T: number of discretization points for T
  u[27]=10;    // n_Theta: number of discretization points for Theta
  u[28]=10;    // n_Tau: number of discretization points for Tau
  u[29]=271.9958;// p1
  u[30]=0.1467;// p2
  u[31]=53.2434;// p3
  u[32]=0-1.6842;// p4
}

