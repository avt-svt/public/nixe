#include "HeatingStorageTankDense.hpp"

void HSTDense::EvalFinalValues(double time, double *states, double *parameters, 
			double* sens, double* adjoints, const Nixe::ProblemInfo& problemInfo) {

	// phi(x)=x[0] is objective function
	
	for(int i=0;i<problemInfo.NumAdjoints()*problemInfo.NumStates();i++)
		adjoints[i]=0;
	adjoints[0]=1.0;
	
}
