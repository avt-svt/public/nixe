#include "HeatingStorageTankDense.hpp"


void HSTDense::EvalMass(Nixe::DCMInterface *mass) {
	const int n_T= m_NT;
	const int n_Tau= m_NTau;
	const int n_Theta=m_NTheta;
	const int n_x=mass->m;
	const int n_differential_variables = n_T - 2 + 2*(n_Theta-1) + 3*n_Tau - 2;
	for(int i=0;i<n_x*n_x;i++)
		mass->values[i]=0;
	for (int i=17; i<n_x; i++)
		mass->values[i*(n_x+1)]=1.0;
	mass->values[(16+n_T-1)*(n_x+1)]=0;
	mass->values[(16+n_T+n_Theta-1)*(n_x+1)]=0;
	mass->values[(16+n_T+n_Theta)*(n_x+1)]=0;
	mass->values[(16+n_T+2*n_Theta+n_Tau-1)*(n_x+1)]=0;
	mass->values[(16+n_T+2*n_Theta+2*n_Tau)*(n_x+1)]=0;
}
