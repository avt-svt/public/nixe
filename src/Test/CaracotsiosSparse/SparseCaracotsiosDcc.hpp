#ifndef SPARSE_CARACOTSIOS_DCC_INCLUDE_ /* allow multiple inclusions */
#define SPARSE_CARACOTSIOS_DCC_INCLUDE_ 

#include "NixeCCSInterface.hpp"
#include "NixeDaeInterface.hpp"

class SparseCaracotsiosDcc : public  Nixe::DaeInterface<Nixe::CCSInterface> {
public:


  virtual void EvalMass(Nixe::CCSInterface *mass);
  



  

	virtual void EvalForward(bool evalJac, double time, double* states, double *parameters, 
		double *rhsStates, double* sens, Nixe::CCSInterface *jacobian, 
			double* rhsStatesDtime, double* rhsSens, const Nixe::ProblemInfo& problemInfo) const;

	virtual void EvalReverse(double time, double *states, double* parameters, double* sens, double* adjoints, 
			double * rhsAdjoints, double* rhsDers, const Nixe::ProblemInfo& problemInfo) const;

			virtual void EvalSwitchingFunction(double time, double *states, double *parameters, 
			double *switchingFctn, const Nixe::ProblemInfo& problemInfo) const;

	virtual void EvalInitialValues(double time, double *parameters, double* states, double* sens, 
			const Nixe::ProblemInfo& problemInfo);

	virtual void EvalFinalValues(double time, double *states, double *parameters, 
			double* sens, double* adjoints, const Nixe::ProblemInfo& problemInfo);

	virtual NIXE_SHARED_PTR<Nixe::ProblemInfo> GetProblemInfo();

	virtual void EvalParameters(double *parameters,const Nixe::ProblemInfo& problemInfo);

 
}; 


#endif //SPARSE_CARACOTSIOS_DCC_INCLUDE_
