#include "NixeDenseColumnMajor.hpp"
#include "NixeForwardSolver.hpp"
#include <iostream>
#include "NixeAdjointData.hpp" 
#include "SparseCaracotsiosDcc.hpp"
#include "NixeUtil.hpp"
#include "NixeReverseSolver.hpp"
#include <fstream>



using namespace Nixe;  

const int n_obj=1;


bool ComputedHessianIsCorrect(Nixe::DMatrix& computedHessian, Nixe::DMatrix& referenceHessian){
	const double relTol=1e-4;
	const double absTol=1e-4;
	

	double error=0;
	
	Nixe::DMatrix& A= computedHessian;
	Nixe::DMatrix& B=referenceHessian;
	for(int i=0; i<B.NumRows();i++){
		for(int j=0; j< B.NumRows(); j++){
			double currentError= fabs(A(i,j)-B(i,j))/(relTol*fabs(B(i,j)) + absTol);
			error = Nixe::Max(error,currentError);
		}
	}

	if (error < 1)
		return true;
	else
		return false;

}


int main(int argc, char *argv[])
 {

 /************** load Reference Hessian **************/
	const int n=8;
	const double someStrangeValue=-17; // strange value for debugging purposes
	Nixe::DMatrix referenceHessian(n,n,someStrangeValue);
	if(argc > 1) {
	 std::ifstream inputFile(argv[1]);
	 for(int i=0; i<n; i++)
		for(int j=0; j<n; j++)
			inputFile >> referenceHessian(i,j);
	 inputFile.close();
	}

	Nixe::Status status;
	NIXE_SHARED_PTR<Nixe::Array<int> > indizesRoots;
	NIXE_SHARED_PTR<SparseCaracotsiosDcc> dae(new SparseCaracotsiosDcc);
	NIXE_SHARED_PTR<Options>  options(new Options);

	options->SetTypeSequence(Seq_Harmonic);
 
	NIXE_SHARED_PTR<ForwardSolver<CompressedColumn> >nixeFor(new ForwardSolver<CompressedColumn>(dae,options));
	double t0=timer();
	status=nixeFor->Solve();
	if(status == Nixe::Stat_RootFound) {
		indizesRoots=nixeFor->GetIndizesRoots();
	}
	NIXE_SHARED_PTR<Checkpoints> checkpoints= nixeFor->GetCheckpoints();
	NIXE_SHARED_PTR<ReverseSolver<CompressedColumn> > nixeRev(new ReverseSolver<CompressedColumn>(dae,checkpoints));
	double t1=timer();
	nixeRev->Solve();
	double t2=timer();

		
	NIXE_SHARED_PTR<Array<double> > ders=nixeRev->GetDerivatives();
	DMatrix computedHessian(n,n);
	VectorCopy(n*n,computedHessian.Data(),ders->Data()+n); // first n entries belong to gradient

	bool isCorrect= ComputedHessianIsCorrect(computedHessian,referenceHessian);
	if (isCorrect)
		return 0;
	else
		return 1;
}

