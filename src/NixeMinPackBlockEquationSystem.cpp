/*
 * NixeMinPackFunction.cpp
 *
 *  Created on: 08.10.2010
 *      Author: Moritz Schmitz
 */
#include "NixeMinPackBlockEquationSystem.hpp"
#include "minpack.h"
#include "cminpack.h"

using namespace Nixe;

MinPackBlockEquationSystem::~MinPackBlockEquationSystem() {};
MinPackBlockEquationSystem::MinPackBlockEquationSystem(DaeInterface<CCSInterface> *dae, const Nixe::ProblemInfo *dae_problemInfo, const std::vector<int>& sparsityPattern,
								   int numVars, int *eqIndices, int *varIndices, double *tempStates, int nonZerosReducedJac) : 
			m_States(dae_problemInfo->NumStates()),
			myDae(dae),
			m_Nx(dae_problemInfo->NumStates()),
			m_numVars(numVars),
			m_nonZerosReducedJac(nonZerosReducedJac),
			m_EqIndices(numVars),
			m_VarIndices(numVars),
			m_X0(numVars),
			m_SparsityPattern(nonZerosReducedJac){	
		
		VectorCopy(numVars,m_EqIndices.Data(),eqIndices);
		VectorCopy(numVars,m_VarIndices.Data(),varIndices);
		VectorCopy(nonZerosReducedJac, m_SparsityPattern.Data(), &sparsityPattern[0]);
		m_Dae_problemInfo = myDae->GetProblemInfo();
		m_CurrentTime=m_Dae_problemInfo->StartTime();
		Nixe::ProblemInfo tempProblemInfo(*dae_problemInfo);
		tempProblemInfo.SetNumSens(0);
		VectorCopy(dae_problemInfo->NumStates(), m_States.Data(), tempStates);
		for(int i=0;i<m_numVars;i++){ // save initial values for actual variables because others will be overwritten by tempStates
			m_X0[i] = m_States[m_VarIndices[i]];
		}
		VectorCopy(m_Nx,m_States.Data(),tempStates); // to assign already initialized variables
	};
void MinPackBlockEquationSystem::InitialGuess(const int N, double *X0){
		for(int i=0;i<m_numVars;i++){
			X0[i] = m_X0[i];
			m_States[m_VarIndices[i]] = m_X0[i];
		}
};
void MinPackBlockEquationSystem::FuncJac(const int *nvar, const double x[], double f[], double jac[],
              const int *ldjac, int *userflag, void *comm){

	switch (*userflag) {
		case 1: 
		{
			for(int i=0;i<m_numVars;i++){
				m_States[m_VarIndices[i]]=x[i];
			}
			Nixe::Array<long> EqIndices(m_numVars);
			for(int i=0; i<m_numVars; i++){
				EqIndices[i] = m_EqIndices[i];
			}
			myDae->EvalRhs(m_numVars, EqIndices.Data(), m_CurrentTime, m_States.Data(), m_Nx, f);

			break;
		}
		case 2:
		{
			const int nnzJac=m_Dae_problemInfo->NumNonZerosJac();
			for(int i=0;i<m_numVars;i++){
				m_States[m_VarIndices[i]]=x[i];
			}

			Array<int> jacRowInd(m_nonZerosReducedJac,0);
			Array<int> jacColInd(m_nonZerosReducedJac,0);
			Array<double> jacVal(m_nonZerosReducedJac,0);

			// reduced StateJacobian will be stored in row-major triplet format:
			myDae->EvalStateJac(m_CurrentTime, m_States.Data(), m_Nx, m_EqIndices.Data(), m_VarIndices.Data(), m_numVars, m_SparsityPattern.Data(), jacRowInd.Data(), jacColInd.Data(), jacVal.Data(), m_nonZerosReducedJac);

			for(int j=0; j<m_numVars; j++){
				for(int i=0; i<m_numVars; i++){
					jac[j*m_numVars+i] = 0; // JAC (MxN matrix) in column-major storage
				}
			}
			for(int i=0; i<m_nonZerosReducedJac; i++){
				jac[jacColInd[i]*m_numVars + jacRowInd[i]] = jacVal[i];
			}
			break;
		}
		default:
			;
	}

};

namespace Nixe {


	#ifdef __cplusplus
	  extern "C" {
	#endif

	int MyMinPackFuncDerNN(void *p, int n, const double *x, double *fvec, double *fjac,
                                 int ldfjac, int iflag )
	{	
		void* comm=0;
		// p transfers the current instance of MinPackBlockEquationsSystem
		MinPackBlockEquationSystem *equationSystem = static_cast<MinPackBlockEquationSystem*>(p);
		equationSystem->FuncJac(&n,x,fvec,fjac,&ldfjac,&iflag,comm);
		return 1; //positive value to not terminate hybrj, see documentation of hybrj in cminpack.h
	}
	

	#ifdef __cplusplus
	  }; // extern "C"
	#endif

		
	MinPackWrapper::MinPackWrapper(MinPackBlockEquationSystem *equationSystem) : m_EquationSystem(equationSystem),m_N(equationSystem->GetDimension()),
			m_Solution(equationSystem->GetDimension()) {}	
	MinPackWrapper::~MinPackWrapper() {}
	void MinPackWrapper::Solve() {
		
		const int nx = m_EquationSystem->GetDimension();
		void *comm=0;
		int nvar=nx;
		int tdfjac=nx;
		int fail;
		Array<double> f(nx,0);
		Array<double> jac(nx*nx,0);

		//Unless high precision solutions are required,
        //the recommended value for TOL is the square root of the machine
        //precision.
		//If this condition is not satisfied, then HYBRD1 may incorrectly
        //indicate convergence.(http://www.math.utah.edu/software/minpack/minpack/hybrd1.html) mosc May 6, 2011
		double xtol=1e-12; //former value xtol=1e-14 

		m_Success=false;

		// inital guess
		m_EquationSystem->InitialGuess(m_N,m_Solution.Data());


			Array<double> fvec(nvar,0);
			Array<double> fjac(tdfjac*nvar,0);
		 
			int maxfev,mode,nfev,njev,nprint;
			double factor = 1e13;
			double one = 1.0;
			double zero = 0.0;
			fail = 0;
			Array<double> diag(nvar,0);
			Array<double> wa1(nvar,0);
			Array<double> wa2(nvar,0);
			Array<double> wa3(nvar,0);
			Array<double> wa4(nvar,0);
			Array<double> qtf(nvar,0);
			int lr=(nvar)*(nvar+1)/2;
			Array<double> r(lr,0);
		      
			// check the input parameters for errors.

			if (nvar <= 0 || tdfjac < nvar || xtol < zero )
				return;

			// call hybrj.

			maxfev = 100*(nvar + 1);
			mode = 2;

			// Scaling (multiplicative scale factors):
			if(nvar>1){
				for(int i=0; i<nvar; i++){
					if(fabs(m_Solution[i])<1.0 && fabs(m_Solution[i])>0.0){
						double val = fabs(m_Solution[i]);
						double k=1.0;
						do{
							k = k*10.0;
							val = fabs(m_Solution[i])*k;
						}while(val<1.0);
						if(k != 0)
							diag[i] = k;
						else
							diag[i] = 1.0;
					}
					else{
						if(fabs(m_Solution[i])>=10.0){
							double val = fabs(m_Solution[i]);
							double k=1.0;
							do{
								k = k*1e-1;
								val = fabs(m_Solution[i])*k;
							}while(val>=10.0);
							if(k != 0)
								diag[i] = k;
							else
								diag[i] = 1.0;
						}
						else // fabs(m_Solution[i] == 0 | 1<=fabs(m_Solution[i]<10))
							diag[i] = 1.0;
					}
				}
			}
			else
				diag[0] = 1.0;
			nprint = 0;

			// p transfers the current MinPackBlockEquationSystem to the function MyMinPackFuncDerNN
			void *p=static_cast<MinPackBlockEquationSystem*>(m_EquationSystem);

			fail = hybrj(&MyMinPackFuncDerNN,p,nvar,m_Solution.Data(),f.Data(),jac.Data(),tdfjac,xtol,maxfev,diag.Data(),mode,factor,nprint,&nfev,&njev,r.Data(),
				lr,qtf.Data(),wa1.Data(),wa2.Data(),wa3.Data(),wa4.Data());

		if(fail != 0 && fail !=3) m_Success=true;
		
	}

	void MinPackWrapper::Print() {
		std::cout << "Solution vector is" << std::endl;
		for (int i=0; i<m_N; i++)
			std::cout << m_Solution[i] << std::endl;
	}

}
