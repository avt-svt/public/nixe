
#include "NixeProblemInfo.hpp"

namespace Nixe {


ProblemInfo::ProblemInfo() : m_StartTime(0),m_FinalTime(0) ,
		m_NumStates(0),m_NumParams(0),m_NumNonZerosMass(0),m_MaxOrderSens(0),
		m_NumNonZerosJac(0),m_NumSwitchFctns(0),m_NumSens(0), m_NumSaveSens(0),
		m_NumAdjoints(0),m_NumDers(0),m_Save(false), m_IsForwardSweep(true) {}

ProblemInfo::ProblemInfo(const ProblemInfo& problemInfo)
{
	m_StartTime=problemInfo.m_StartTime;
	m_FinalTime=problemInfo.m_FinalTime;
	m_NumStates=problemInfo.m_NumStates;
	m_NumParams = problemInfo.m_NumParams;
	m_NumNonZerosMass = problemInfo.m_NumNonZerosMass;
	m_MaxOrderSens = problemInfo.m_MaxOrderSens;
	m_NumNonZerosJac = problemInfo.m_NumNonZerosJac;
	m_NumSwitchFctns = problemInfo.m_NumSwitchFctns;
	m_NumSens = problemInfo.m_NumSens;
	m_NumSaveSens = problemInfo.m_NumSaveSens;
	m_NumAdjoints = problemInfo.m_NumAdjoints;
	m_NumDers = problemInfo.m_NumDers;
	m_Save = problemInfo.m_Save;
	m_IsForwardSweep = problemInfo.m_IsForwardSweep;
}

ProblemInfo::~ProblemInfo() {}
	
} // end namespace Nixe

