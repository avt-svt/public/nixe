
Set(HBRD_DENSE_DIR src/Test/HybridBioReactorDense)

INCLUDE_DIRECTORIES(${HBRD_DENSE_DIR})

Set(HBRD_DENSE_HEADER_FILES  
	${HBRD_DENSE_DIR}/HybridBioReactorDense.hpp
	${HBRD_DENSE_DIR}/rhs.hpp 
	${HBRD_DENSE_DIR}/BioreactorGlobalDefs.h)

	
SOURCE_GROUP("Header Files" FILES ${HBRD_DENSE_HEADER_FILES} )

Set(HBRD_DENSE_SOURCE_FILES 
	${HBRD_DENSE_DIR}/main.cpp
	${HBRD_DENSE_DIR}/EvalForward.cpp
	${HBRD_DENSE_DIR}/EvalReverse.cpp
	${HBRD_DENSE_DIR}/EvalInitialValues.cpp
	${HBRD_DENSE_DIR}/EvalFinalValues.cpp
	${HBRD_DENSE_DIR}/EvalParameters.cpp
	${HBRD_DENSE_DIR}/EvalSwitchingFunction.cpp
	${HBRD_DENSE_DIR}/GetProblemInfo.cpp
	${HBRD_DENSE_DIR}/EvalMass.cpp
	${HBRD_DENSE_DIR}/rhs.cpp
	${HBRD_DENSE_DIR}/CompGradSwitchTime.cpp
	${HBRD_DENSE_DIR}/UpdateDersAdjoints.cpp)
	
add_executable(HybridBioReactorDense ${HBRD_DENSE_HEADER_FILES} 
	${HBRD_DENSE_SOURCE_FILES} )

target_link_libraries(HybridBioReactorDense Nixe)



if(MSVC)
SET_PROPERTY(TARGET HybridBioReactorDense PROPERTY LINK_FLAGS_DEBUG  "/NODEFAULTLIB:MSVCRT" )
endif()

#ADD_TEST(TestHeatingStorageTankDense ${EXECUTABLE_OUTPUT_PATH}/HeatingStorageTankDense ${CMAKE_CURRENT_SOURCE_DIR}/${HBRD_DENSE_DIR}/ReferenceHessian.dat)

