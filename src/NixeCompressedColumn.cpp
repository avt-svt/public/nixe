#include "NixeCompressedColumn.hpp"
#include "cs.h"
#include "NixeUtil.hpp"



namespace Nixe {
int CompressedColumn::GetMaximalBlockSize() const {
	csd *dmperm = cs_dmperm(m_Imp,0);

	int maxRowBlockSize=0;
	int maxColBlockSize=0;
	for(int i=0; i< dmperm->nb;i++){
		maxRowBlockSize=Max(dmperm->r[i+1]-dmperm->r[i],maxRowBlockSize);
		maxColBlockSize=Max(dmperm->r[i+1]-dmperm->r[i],maxColBlockSize);
	}
	cs_dfree(dmperm);
	return Max(maxRowBlockSize,maxColBlockSize);
}

CompressedColumn::CompressedColumn(const double alpha, const CompressedColumn& A,
		const double beta, const CompressedColumn& B) : m_Lu(0) {
  m_Imp = cs_add(A.m_Imp,B.m_Imp, alpha,beta);
  this->nzmax = m_Imp->nzmax;
  this->m     = m_Imp->m;
  this->n     = m_Imp->n;
  this->pc    = m_Imp->p;
  this->ir    = m_Imp->i;
  this->values= m_Imp->x;
}


CompressedColumn::CompressedColumn(int m, int n, int nnz) : m_Lu(0) {
  m_Imp = cs_spalloc(m,n,nnz,1,0);
  this->nzmax = m_Imp->nzmax;
  this->m     = m_Imp->m;
  this->n     = m_Imp->n;
  this->pc    = m_Imp->p;
  this->ir    = m_Imp->i;
  this->values= m_Imp->x;
}

CompressedColumn::~CompressedColumn() {
  cs_spfree(m_Imp);
  if (m_Lu) delete m_Lu;
};

void CompressedColumn::Multiply(const double coeff, double *result,const double *toMult,
				    const  int mm,const int nn) {
  for (int i=0;i<m*nn;i++)
    result[i]*=coeff;
  for (int i=0; i < nn; i++)
    cs_gaxpy(m_Imp,&(toMult[i*n]),&(result[i*mm]));
}

void CompressedColumn::TransposeMultiply(const double coeff, double *result,const double *toMult,
				     const int mm,const int nn) {
  for (int i=0;i<this->n*nn;i++)
    result[i]*=coeff;
  cs * transposeImp = cs_transpose(m_Imp,1);
  for (int i=0; i < nn; i++)
    cs_gaxpy(transposeImp,&(toMult[i*mm]),&(result[i*this->n]));
  cs_spfree(transposeImp);
}

void CompressedColumn::Add(double *result, const int mm, const int nn) {
	int counter = 0;
	int index;
  for(int i=0; i<n;i++)
    for (int p=pc[i]; p<pc[i+1]; p++) {    
			index = i*m+ir[p];
      result[index] += values[counter];
			++counter;
    }
}

void CompressedColumn::Transpose() {
	cs *res = cs_transpose(this->m_Imp,1);
	cs_spfree(m_Imp);
	m_Imp=res;
	this->nzmax = m_Imp->nzmax;
    this->m     = m_Imp->m;
    this->n     = m_Imp->n;
    this->pc    = m_Imp->p;
    this->ir    = m_Imp->i;
    this->values= m_Imp->x;
}
} // namespace Nixe 
