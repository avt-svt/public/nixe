/*
 *
 *  Created on: 22.09.2010
 *      Author: Moritz Schmitz and Ralf Hannemann
 */


#include "NixeTensolveBlocEquationSystem.hpp"


using namespace Nixe;

MyBlocEquationSystemTENSOLVE::~MyBlocEquationSystemTENSOLVE() {};
MyBlocEquationSystemTENSOLVE::MyBlocEquationSystemTENSOLVE(DaeInterface<CCSInterface> *dae, const Nixe::ProblemInfo *dae_problemInfo,
								   int numVars, int *eqIndices, int *varIndices, double *tempStates, int nonZerosReducedJac) : 
			m_States(dae_problemInfo->NumStates()),
			m_Parameters(dae_problemInfo->NumParams()),
			m_Sens(dae_problemInfo->NumStates() * dae_problemInfo->NumSens(), 0),
			m_RhsSens(dae_problemInfo->NumStates() * dae_problemInfo->NumSens()),
			m_RhsStatesDtime(dae_problemInfo->NumStates()),
			m_Rhs(dae_problemInfo->NumStates()),
			myDae(dae),
			m_Nx(dae_problemInfo->NumStates()),
			m_numVars(numVars),
			m_nonZerosReducedJac(nonZerosReducedJac),
			m_EqIndices(numVars),
			m_VarIndices(numVars),
			m_X0(numVars){	
		
		VectorCopy(numVars,m_EqIndices.Data(),eqIndices);
		VectorCopy(numVars,m_VarIndices.Data(),varIndices);
		m_Dae_problemInfo = myDae->GetProblemInfo();
		m_CurrentTime=m_Dae_problemInfo->StartTime();
		myDae->EvalParameters(m_Parameters.Data(), *dae_problemInfo);
		myDae->EvalInitialValues(0.0, m_Parameters.Data(), m_States.Data(), m_Sens.Data(), *dae_problemInfo);
		for(int i=0;i<m_numVars;i++){ // save initial values for actual variables because others will be overwritten by tempStates
			m_X0[i] = m_States[m_VarIndices[i]];
		}
		VectorCopy(m_Nx,m_States.Data(),tempStates); // to assign already initialized variables
	};


	void MyBlocEquationSystemTENSOLVE::InitialGuess(const int N, double *X0){
		for(int i=0;i<m_numVars;i++){
			X0[i] = m_X0[i];
			//m_States[m_VarIndices[i]] = m_X0[i];
		}
	};
	/*--------------------------------------------------
	Calculates the Right-Hand-Side in X
	--------------------------------------------------*/
	void MyBlocEquationSystemTENSOLVE::Func(const double *X, double *FX, const int& M, const int& N){
		for(int i=0;i<m_numVars;i++){
			m_States[m_VarIndices[i]]=X[i];
		}
		myDae->EvalForward(false, m_CurrentTime, m_States.Data(), m_Parameters.Data(), m_Rhs.Data(), m_Sens.Data(), (0), 
			m_RhsStatesDtime.Data(), m_RhsSens.Data(), *m_Dae_problemInfo.get());
		for(int i=0; i<m_numVars; i++){
			FX[i] = m_Rhs[m_EqIndices[i]];
		}
	};	

	/*--------------------------------------------------------------------------
	Calculates the Jacobian of the Rhs with respect to X
	--------------------------------------------------------------------------*/
	void MyBlocEquationSystemTENSOLVE::Jacobian(const double *X, double *JAC, const int& MAXM, const int& M, const int& N){
		const int nnzJac=m_Dae_problemInfo->NumNonZerosJac();
		for(int i=0;i<m_numVars;i++){
			m_States[m_VarIndices[i]]=X[i];
		}

		NIXE_SHARED_PTR<Nixe::CompressedColumn> StateJacobian (new Nixe::CompressedColumn(m_Nx,m_Nx,nnzJac));
		
		myDae->EvalForward(true, m_CurrentTime, m_States.Data(), m_Parameters.Data(), m_Rhs.Data(), m_Sens.Data(), StateJacobian.get(), 
			m_RhsStatesDtime.Data(), m_RhsSens.Data(), *m_Dae_problemInfo.get());
		Array<int> jacRowInd(m_nonZerosReducedJac,0);
		Array<int> jacColInd(m_nonZerosReducedJac,0);
		Array<double> jacVal(m_nonZerosReducedJac,0);
		myDae->EvalStateJac(m_nonZerosReducedJac, m_numVars, m_EqIndices.Data(), m_VarIndices.Data(), StateJacobian.get(), jacRowInd.Data(), jacColInd.Data(), jacVal.Data());

		for(int j=0; j<N; j++){
			for(int i=0; i<M; i++){
				JAC[j*M+i] = 0; // JAC (MxN matrix) in column-major storage
			}
		}
		for(int i=0; i<m_nonZerosReducedJac; i++){
			JAC[jacColInd[i]*MAXM + jacRowInd[i]] = jacVal[i];
		}
	};
