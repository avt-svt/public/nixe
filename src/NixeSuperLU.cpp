#include "NixeSuperLU.hpp"
#include "slu_ddefs.h"

#include "NixeUtil.hpp"
#include <iostream>


Nixe::SuperLUreuseInfo::SuperLUreuseInfo (int N) {
	m_Dimension=N; 
	m_PermCol=new int[m_Dimension]; 
	m_EliminationTree=new int[m_Dimension]; 
	m_IsAllocatedMemory = true;
	m_IsFirstCall = true;
}

Nixe::SuperLUreuseInfo::~SuperLUreuseInfo () {
	if(m_IsAllocatedMemory) {
		delete[] m_PermCol; 
		delete[] m_EliminationTree;
	}
}

namespace Nixe{
  class SuperLUImpl {
  public:
    SuperLUImpl(CCSInterface *linearSystem);
    ~SuperLUImpl();
    void SLUSolveAgain(Nixe::DMatrix& solutionMatrix,
		       const Nixe::DMatrix& rightHandSideMatrix, bool transpose);
    void SLUSolve(Nixe::DMatrix& solutionMatrix,
		  const Nixe::DMatrix& rightHandSideMatrix,
		  Nixe::SuperLUreuseInfo *reuseInfo, bool *success, bool transpose);
  private:
    void SetLinearSystem(CCSInterface *linearSystem); 
    void AllocateInitialize(int dimensionLinearSystem);
    SuperLUImpl() {};
    int m_DimensionLinearSystem; // Dimension N of square matrix A
    CCSInterface *m_LinearSystem; // Matrix that describes the linear System which has to be decomposed
    bool m_IsComputedLU;
		
    // Variables for input of dgssvx, for more infos see dgssvx.c of superLU
    superlu_options_t	m_Options; // Options
    SuperMatrix			m_A; // the linear System to be solved
    int* NIXE_RESTRICT m_PermCol; // column permutation
    int* NIXE_RESTRICT m_PermRow; // row permuation
    int* NIXE_RESTRICT m_EliminationTree; // elimination tree
    char *m_Equed; // Infos about equilibration, may be modified
    double * NIXE_RESTRICT m_RowScal; // row scaling of linear system (for equilibration)
    double * NIXE_RESTRICT m_ColScal; // column scaling of linear system (for equilibration)		 
    SuperMatrix	m_L; // L of LU
    SuperMatrix	m_U; // U of LU
    void*	m_Work;  // memory for SuperLU, only for interface, not used
     int		m_Lwork; // length of m_Work, only for interface, not used
    SuperMatrix	m_X; // Contains the solution vector
    SuperMatrix	m_B; // Right hand sides, may be modified
    double	m_RecipPivotGrowth; // The reciprocal pivot growth factor max_j( norm(A_j)/norm(U_j) ).
    double	m_RecipCond;  // The reciprocal condition number of equilibrated matrix
    double*	NIXE_RESTRICT 		m_Ferr; // forward error estimates, only for interface, not used
    double*	NIXE_RESTRICT 		m_Berr; // backward error estimates, only for interface, not used
    mem_usage_t	m_MemUsage;
    SuperLUStat_t m_Stat; // Statistics Info
    int		m_Info; // Infos about successful/non-succesful solving

  };
}

Nixe::SuperLUImpl::SuperLUImpl(Nixe::CCSInterface* linearSystem){
	int n=linearSystem->n;
	AllocateInitialize(n);
	this->SetLinearSystem(linearSystem);
}

void Nixe::SuperLUImpl::AllocateInitialize(int DimensionLinearSystem) {
	// Set Dimension 
	m_DimensionLinearSystem=DimensionLinearSystem;

	/* allocation of memory */ 
	m_Equed = new char[16];
	m_PermCol = new  int[m_DimensionLinearSystem];
	m_PermRow = new  int[m_DimensionLinearSystem];
	m_EliminationTree = new  int[m_DimensionLinearSystem];
	m_RowScal = new  double[m_DimensionLinearSystem];
	m_ColScal = new  double[m_DimensionLinearSystem];

	/*  initalizations  */
	// default options
	set_default_options(&m_Options);
	//m_Options.RowPerm = NOROWPERM;
	//m_Options.ColPerm = NATURAL;
	m_Options.PrintStat=NO;
	// these variables are only used for interfacing, set to zero
	m_Work=0;
	m_Lwork=0;
	// Initialize the statistics variables, side effect: memory is allocated
	StatInit(&m_Stat);
	m_IsComputedLU=false;

}
void Nixe::SuperLUImpl::SetLinearSystem(Nixe::CCSInterface *linearSystem) {
	m_LinearSystem=linearSystem;
}

Nixe::SuperLUImpl::~SuperLUImpl() {
	delete[] m_Equed;
	StatFree(&m_Stat);
	delete[] m_ColScal;
	delete[] m_RowScal;
	delete[] m_EliminationTree;
	delete[] m_PermRow;
	delete[] m_PermCol;
	if(m_IsComputedLU) {
		  Destroy_SuperNode_Matrix(&m_L);
      Destroy_CompCol_Matrix(&m_U);
	}
}

// Solve: assume that m_LinearSystem has been set
void Nixe::SuperLUImpl::SLUSolve(Nixe::DMatrix& solutionMatrix,
				   const Nixe::DMatrix& rightHandSideMatrix,
				   Nixe::SuperLUreuseInfo *reuseInfo, bool *success,bool transpose){

    

	int m = m_LinearSystem->m; // number of rows
	int n = m_LinearSystem->n;  //number of columns
	int nnz = m_LinearSystem->pc[n]; // Nonzeros, compressed column storag

	dCreate_CompCol_Matrix(&m_A, m, n, nnz,
							m_LinearSystem->values, m_LinearSystem->ir, m_LinearSystem->pc, 
							SLU_NC, SLU_D, SLU_GE);
	
	int nrhs = rightHandSideMatrix.NumCols();
	// should be checked if dimensions of X and B are equal

	DMatrix copyOfRightHandSideMatrix(rightHandSideMatrix);
	dCreate_Dense_Matrix(&m_B, m, nrhs, copyOfRightHandSideMatrix.Data(), m, SLU_DN, SLU_D, SLU_GE);
	dCreate_Dense_Matrix(&m_X, m, nrhs, solutionMatrix.Data(), m, SLU_DN, SLU_D, SLU_GE);
	
	if (transpose)
		m_Options.Trans = TRANS;
	else
		m_Options.Trans = NOTRANS;
	

	// Decide whether sparsitiy pattern can be reused
	// If FirstCall, pattern can not be reused
	if (reuseInfo->GetIsFirstCall()) {
		m_Options.Fact = DOFACT;
	}
	//  Reuse sparsitiy pattern  possible
	else {
		Nixe::VectorCopy(n,m_PermCol,reuseInfo->GetPermCol());
		Nixe::VectorCopy(n,m_EliminationTree,reuseInfo->GetEliminationTree());
		m_Options.Fact = SamePattern;
	}
	m_Ferr = new  double[nrhs];
	m_Berr = new  double[nrhs];

//#define DEBUGLINALG
#ifdef DEBUGLINALG
	
	static int counter=0;
	if(transpose){
		std::ofstream ofA("LU.m");

		const int nnz=this->m_LinearSystem->pc[n];

		ofA << "valsA=[" ;
		for (int i=0; i< nnz; i++)
			ofA << this->m_LinearSystem->values[i] << std::endl;
		ofA << "];" << std::endl;
		ofA << std::endl;

		ofA << "rowsA=[" ;
		for (int i=0; i< nnz; i++)
			ofA << this->m_LinearSystem->ir[i] +1 << std::endl;
		ofA << "];" << std::endl;
		ofA << std::endl;


		ofA << "colsA=[" ;
		int *pcA=m_LinearSystem->pc;
		for (int i=0; i< n; i++)
			for(int j=pcA[i]; j <pcA[i+1]; j++)
				ofA << i+1 << std::endl;
		ofA << "];" << std::endl;
		ofA << std::endl;


		ofA << "A=sparse(rowsA,colsA,valsA);" << std::endl << std::endl;


		ofA << "b=[";
		for (int i=0; i< n; i++){
			for(int j=0; j< nrhs; j++){
				ofA << copyOfRightHandSideMatrix(i,j) << " ";
			}
			ofA << std::endl;
		}
		ofA << "];" << std::endl;
		ofA << std::endl;
		ofA.close();
//		exit(1);
	}
	++counter;

#endif

	// Call Expert driver of SuperLU
	dgssvx(&m_Options, &m_A,m_PermCol,m_PermRow,m_EliminationTree,m_Equed,m_RowScal,m_ColScal,
			&m_L,&m_U,m_Work,m_Lwork,&m_B,&m_X,&m_RecipPivotGrowth,&m_RecipCond,
			m_Ferr,m_Berr,&m_MemUsage,&m_Stat,&m_Info);
	if (m_Info == 0)
		*success = true;
	else
		*success = false;

	
 

	// Keep track of storage for L and U
	m_IsComputedLU=true;

	// Transfer Info for subsequent calls
	if (reuseInfo->GetIsFirstCall()) {
		Nixe::VectorCopy(n,reuseInfo->GetPermCol(),m_PermCol);
		Nixe::VectorCopy(n,reuseInfo->GetEliminationTree(),m_EliminationTree);
		reuseInfo->SetIsFirstCall(false);
	}


	// Destroy data structures (not memory) for X,B,and A. 
	// L and U are not destroyed, Do not use SuperLU Destroy_XXXXX_Matrix,
	// because the user workspace should not be touched
	delete[] m_Berr;
	delete[] m_Ferr;
	SUPERLU_FREE ( m_B.Store );
	SUPERLU_FREE ( m_X.Store);
	SUPERLU_FREE ( m_A.Store);
	

}

void Nixe::SuperLUImpl::SLUSolveAgain(Nixe::DMatrix& solutionMatrix,
				  const  Nixe::DMatrix& rightHandSideMatrix, bool transpose) {
	
	int m = m_LinearSystem->m; // number of rows
	int n = m_LinearSystem->n;


	int nrhs = rightHandSideMatrix.NumCols();
	// should be checked if dimensions of X and B are equal

	// create MAtrix structure for SuperLU, 
	DMatrix copyOfRightHandSideMatrix(rightHandSideMatrix);
	DMatrix copyOfSolutionMatrix(m,nrhs,0.0);
	dCreate_Dense_Matrix(&m_B, m, nrhs, copyOfRightHandSideMatrix.Data(), m, SLU_DN, SLU_D, SLU_GE);
	dCreate_Dense_Matrix(&m_X, m, nrhs, copyOfSolutionMatrix.Data(), m, SLU_DN, SLU_D, SLU_GE);

	m_Options.Fact = FACTORED;
	if (transpose)
		m_Options.Trans = TRANS;
	else
		m_Options.Trans = NOTRANS;

	
		// Call Expert driver of SuperLU

	m_Ferr = new  double[nrhs];
	m_Berr = new  double[nrhs];

//#define DEBUGLINALG
#ifdef DEBUGLINALG
	
	static int counter=0;
	if(transpose){
		std::ofstream ofA("LU.m");

		const int nnz=this->m_LinearSystem->pc[n];

		ofA << "valsA=[" ;
		for (int i=0; i< nnz; i++)
			ofA << this->m_LinearSystem->values[i] << std::endl;
		ofA << "];" << std::endl;
		ofA << std::endl;

		ofA << "rowsA=[" ;
		for (int i=0; i< nnz; i++)
			ofA << this->m_LinearSystem->ir[i] +1 << std::endl;
		ofA << "];" << std::endl;
		ofA << std::endl;


		ofA << "colsA=[" ;
		int *pcA=m_LinearSystem->pc;
		for (int i=0; i< n; i++)
			for(int j=pcA[i]; j <pcA[i+1]; j++)
				ofA << i+1 << std::endl;
		ofA << "];" << std::endl;
		ofA << std::endl;


		ofA << "A=sparse(rowsA,colsA,valsA);" << std::endl << std::endl;


		ofA << "b=[";
		for (int i=0; i< n; i++){
			for(int j=0; j< nrhs; j++){
				ofA << copyOfRightHandSideMatrix(i,j) << " ";
			}
			ofA << std::endl;
		}
		ofA << "];" << std::endl;
		ofA << std::endl;
		ofA.close();
//		exit(1);
	}
	++counter;

#endif

	dgssvx(&m_Options, &m_A,m_PermCol,m_PermRow,m_EliminationTree,m_Equed,m_RowScal,m_ColScal,
			&m_L,&m_U,m_Work,m_Lwork,&m_B,&m_X,&m_RecipPivotGrowth,&m_RecipCond,
			m_Ferr,m_Berr,&m_MemUsage,&m_Stat,&m_Info);


		// Destroy data structures (not memory) for X,B. 
	// L and U are not destroyed, Do not use SuperLU Destroy_XXXXX_Matrix,
	// because the user workspace should not be touched
	solutionMatrix=copyOfSolutionMatrix;

	delete[] m_Berr;
	delete[] m_Ferr;
	SUPERLU_FREE ( m_B.Store );
	SUPERLU_FREE ( m_X.Store);



}

Nixe::SuperLU::SuperLU(Nixe::CCSInterface *linearSystem){
	m_Impl=new SuperLUImpl(linearSystem);
}

Nixe::SuperLU::~SuperLU() {
	delete m_Impl;
}

void Nixe::SuperLU::SLUSolve(Nixe::DMatrix &solutionMatrix, const Nixe::DMatrix &rightHandSideMatrix, 
							 Nixe::SuperLUreuseInfo *reuseInfo, bool *success, bool transpose){
	
	m_Impl->SLUSolve(solutionMatrix,rightHandSideMatrix,reuseInfo,success,transpose);
}

void Nixe::SuperLU::SLUSolveAgain(Nixe::DMatrix& solutionMatrix, const Nixe::DMatrix& rightHandSideMatrix, bool transpose) {
	m_Impl->SLUSolveAgain(solutionMatrix,rightHandSideMatrix,transpose);
}