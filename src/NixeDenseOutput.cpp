#include "NixeDenseOutput.hpp"
#include <cmath>
#include <iostream>


namespace Nixe {

	DenseOutput::DenseOutput(NIXE_SHARED_PTR<StepSequence> stepSequence) :
	m_StepSequence(stepSequence), 
	m_Lambda(0),
	m_Save(((stepSequence->MaxOrder()+2)*(stepSequence->MaxOrder()+1))/2,0.0)
	{
	}

	void DenseOutput::ComputeDifferences(const int maxOrder) {
		for (int k=1; k<=maxOrder; k++){
			for(int i=k; i<= maxOrder; i++){
				const double n_i= (*m_StepSequence)[i-1]; // C-Index style
				for(int j=0; j<i-k+1; j++){
					Set(i,j)=n_i*(Get(i,j+1)-Get(i,j));
				}
			}
		}

	}

	DenseOutput::~DenseOutput() {}

	void DenseOutput::ComputeExtrapolates(const int maxOrder) {
		for (int l=2;  l<= maxOrder; l++) {
			for (int k=maxOrder; k>= l; k--) {
					const double n_k= (*m_StepSequence)[k-1];
					const double n_k_l = (*m_StepSequence)[k-l];
					const double factor= 1.0/((n_k/n_k_l)-1.0); 
				for (int i=k; i >= l-1; i--) {
					Set(k,i)=Get(k,i) + factor * ( Get(k,i)-Get(k-1,i-1) );
				}
			}
		}
	}

	void DenseOutput::ComputePolyCoefficients(const int maxOrder) {
		if (maxOrder < 1) { 
			std::cerr << "Error in DenseOutput::ComputePolyCoefficients, maxOrder is < 1" << std::endl;
			return;
		}
		Faculty fac(maxOrder-1);
		m_Poly.reset(new Array<double> (maxOrder+1,0.0));
		double *p=(*m_Poly).Data();

		p[0] = Get(0,0);
	  p[1] = Get(maxOrder,maxOrder)-Get(0,0);
		
		for (int i=2; i <= maxOrder; i++){
			p[i]=Get(maxOrder,maxOrder+1-i)/fac(i-1)-p[i-1];

		}

		return;

	}

	NIXE_SHARED_PTR<Array<double> > DenseOutput::GetPoly(const int maxOrder) {
		ComputeDifferences(maxOrder);
		ComputeExtrapolates(maxOrder);
		ComputePolyCoefficients(maxOrder);
		return m_Poly;
	}

} // end namespace Nixe