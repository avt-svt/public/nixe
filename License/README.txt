The Nixe Code has its foundation in the SEULEX Code (http://www.unige.ch/~hairer/prog/stiff/seulex.f) of Ernst Hairer (please see also the file LicensSeulex.txt). Though many ideas are based on the original Fortran source code SEULEX, Nixe is a different impementation with different features in C++. In particular, the developer of Nixe (and NOT the developer of SEULEX) are responsible for all bugs and instabilities.

Further, NIXE uses some third party codes for which individual software licences apply:
- CSparse with the LGPL license
- SuperLU with some kind of BSD license
- F2C,BLAS and LAPACK with some kind of BSD license
- CMINPACK with some kind of BSD license

Please refer to the corresponding licenses in the ThirdParty directory.


ACKNOWLEDGEMENTS

This product includes software developed by the
University of Chicago, as Operator of Argonne National
Laboratory.

