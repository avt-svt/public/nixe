\documentclass[12pt]{article}

\setlength{\textwidth}{6.3in}       % Text width
\setlength{\textheight}{9.4in}      % Text height
\setlength{\oddsidemargin}{0.1in}     % Left margin for even-numbered pages
\setlength{\evensidemargin}{0.1in}    % Left margin for odd-numbered pages
\setlength{\topmargin}{-0.5in}         % Top margin
\renewcommand{\baselinestretch}{1.1}
\renewcommand{\baselinestretch}{1.1}
\usepackage{amsfonts}
\usepackage{amsmath}
\usepackage{url}

\newcommand{\R}{{\mathbb{R}}}
\newcommand{\Nixe}{{\sc NIXE}}


\begin{document}
\title{Introduction to \Nixe:\\
A tutorial for downloading, installing, and using \Nixe.}

\author{Version 0.1}

%\date{\today}
\maketitle

\begin{abstract}
  This document is a guide to using \Nixe .  It includes
  instructions on how to obtain and compile \Nixe, a description of
  the interface, user options, etc., as well as a tutorial on how to
  use \Nixe\ for the computation of second derivatives of parametric differential
  algebraic equations.
\end{abstract}

\section*{Authors}
\Nixe\ is developed by Ralf Hannemann with substantial contributions of Moritz Schmitz.

\tableofcontents

\vspace{\baselineskip}
\begin{small}
\noindent
The following names used in this document are trademarks or registered
trademarks: Intel, Matlab, Microsoft, MKL, Visual Studio C++.
\end{small}

\section{Introduction}
\Nixe\ (\underline{N}IXE  \underline{I}s e\underline{X}trapolated \underline{E}uler)
is an open source software package for higher-order discrete adjoint
sensitivity analysis of parametric differential-algebraic equations (DAEs) of index less or equal to one. It can be used to compute first- or higher-order derivatives of stiff ODE- or DAE-embedded functionals
\begin{equation}\label{eq:functional}
 \Phi(p)  := \phi(x(t_f,p)),
\end{equation}
where $\phi: \R^{n_x} \rightarrow \R$ is a sufficiently smooth function, $p\in \R^{n_p}$ are the parameters and $x(t,p)$ is the solution of the differential-algebraic initial value problem
\begin{align}\label{eq:dae}
    M \, \dot{x}(t,p) &= f(t,x(t,p),p) \quad \forall\; t\in [t_0,t_f]\\
    x(t_0,p)&=x_0(p).\label{eq:civ}
\end{align}
Here,  $f: \R \times \R^{n_x} \times \R^{n_p} \rightarrow \R^{n_x}$ is the sufficiently smooth right hand side of the differential-algebraic system, $t$ is the independent (time) variable, $t_0,t_f \in \R$ are the start respectively the end time, $M \in \R^{n_x\times n_x}$ is the possibly singular mass
matrix and $x_0(p)$ are the consistent initial values which may depend on the parameters $p$.





\subsection{Mathematical Background}
\Nixe\ implements an extrapolated linearly-implicit Euler approach for the solution of the initial value problem
(\ref{eq:dae}),(\ref{eq:civ}). For the efficient computation of first- or higher-order derivatives of the functional $\Phi$ in (\ref{eq:functional}), \Nixe\ implements a modified discrete (higher-order) adjoint approach.

\subsection{Availability}
The \Nixe\ package is available from AVT, RWTH Aachen University from the subversion repository
(\url{https://extranet.avt.rwth-aachen.de:4431/Repositories/NIXE2}) under the LGPL (GNU Lesser Public License)
open-source license and includes the source code for \Nixe.  This
means, it is available free of charge, also for commercial purposes.
However, if you give away software including \Nixe\ code (in source
code or binary form) and you made changes to the \Nixe\ source code,
you are required to make those changes public and to clearly indicate
which modifications you made.  After all, the goal of open source
software is the continuous development and improvement of software.
For details, please refer to the LGPL.

Also, if you are using \Nixe\ to obtain results for a publication, we
politely ask you to point out in your paper that you used \Nixe, and
to cite the publication ???.

\subsection{Prerequisites}\label{sec:prerequisites}
Windows user 
In order to build \Nixe, some third party components are required:
\begin{itemize}
\item BLAS (Basic Linear Algebra Subroutines).  Many vendors of
  compilers and operating systems provide precompiled and optimized
  libraries for these dense linear algebra subroutines.  You can also
  get the source code for a simple reference implementation from {\tt
    www.netlib.org} and have the \Nixe\ distribution compile it
  automatically.  However, it is strongly recommended to use some
  optimized BLAS implemetion.

  Examples for efficient BLAS implementations are:
  \begin{itemize}
  \item From hardware vendors:
    \begin{itemize}
    \item ACML (AMD Core Math Library) by AMD
    \item ESSL (Engineering Scientific Subroutine Library) by IBM
    \item MKL (Math Kernel Library) by Intel
    \item Sun Performance Library by Sun
    \end{itemize}
  \item Generic:
    \begin{itemize}
    \item Atlas (Automatically Tuned Linear Algebra Software)
    \item GotoBLAS2 (\url{http://www.tacc.utexas.edu/tacc-projects/gotoblas2/})
    \end{itemize}
  \end{itemize}
  You find more information on the web by googling them.

  Note: BLAS libraries distributed with Linux are not optimized.
\item LAPACK (Linear Algebra PACKage).  Also for LAPACK, some vendors
  offer precompiled and optimized libraries.  But like with BLAS, you
  can get the source code from {\tt www.netlib.org} and have the
  \Nixe\ distribution compile it automatically. LAPACK is for example included within the Intel Math Kernel Library (MKL) or currentversions of GotoBlas2.

\item CSparse (\url{http://www.cise.ufl.edu/research/sparse/CSparse/})

\item UMFPACK (\url{http://www.cise.ufl.edu/research/sparse/umfpack/})

\item AMD  (\url{http://www.cise.ufl.edu/research/sparse/amd/})


\item SuperLU (\url{http://crd.lbl.gov/~xiaoye/SuperLU/})


\item NLEQ1S (\url{http://www.zib.de/Numerik/numsoft/ANT/nleq1s.de.html})

\item TENSOLVE (\url{http://www.netlib.org/toms/768})



  \textbf{NOTE: The solution of the linear systems is a central
    ingredient in \Nixe\ and the integrator's performance and
    robustness depends on your choice.  The best choice depends on
    your application, and it makes sense to try different options.
    Most of the solvers also rely on efficient BLAS code (see above),
    so you should use a good BLAS library tailored to your system.}


\end{itemize}


\subsection{How to use \Nixe}

\section{Problem Definition}
Let $f: \R^n \times \R^m \rightarrow \R^n$ and $x_0 : R^m  \rightarrow \R^n$ be sufficiently smooth, $M \in \R^{n\times n}$ a possibly singular matrix. Let $p \in R^m$ be a constant parameter vector and $I=[0,T]$ an time interval. We consider the linear-implicit autonomous differential-algebraic equation
\begin{equation}\label{eq:dae}
    M \, \dot{x} = f(x,p),
\end{equation}
where we assume the index of (\ref{eq:dae}) to be less or equal to one. We assume also, that $x_0$ provides consistent initial conditions for (\ref{eq:dae}). The aim is to find a continuously differentiable function $x: I \times \R^m \rightarrow \R^n $ , such that
\begin{align}\label{eq:dae2}
    x(0,p)&=x_0(p), \\
    M \, \dot{x}(t,p) &= f(x(t,p),p) \quad \forall\; t\in I.
\end{align}

\section{Sensitivity Analysis}
Sometimes not only the solution of (\ref{eq:dae}) is required, but also the evaluation of a so-called objective function $\phi : \R^n \rightarrow \R^k $ which is build  evaluated at $x(T,p)$ resulting in an objective function $\Phi : \R^m \rightarrow \R^k $  which is only dependent from $p\in \R^m$.
\[
 \Phi(p)  = \phi(x(T,p)).
\]
Now, the first and second derivatives of $\Phi$ with respect to $p$ shall be computed:
$\nabla \Phi,\; \nabla ^2 \Phi$.\

\section{NIXE Interface}
NIXE uses a discrete second-order adjoint approach to compute the second derivatives of $ \Phi $. For adjoint approach, so called adjoint variables $\lambda(t,T,p) \in \R^{n \times k}$ and second-order adjoint variables $\lambda_p(t,T,p) \in \R^{n \times m \times k}$ are introduced. The second-order adjoint variables are the first derivatives of $\lambda(t,T,p)$ with respect to $p$. Furthermore it is useful to introduce the Hamiltonian
\begin{align}
 H :\quad \R^n \times \R^{n\times k} \times \R^m \rightarrow \R^k \notag \\
 (x,\lambda,p) \mapsto\lambda^T  f(x,p). \label{eq:hamilton}
\end{align}
to facilitate the evaluation of the adjoint equations. For details refer to \cite{Oezyurt2005}.

\subsection{Evaluation task}
The fact that in algorithmic differentiation it is beneficial to evaluate higher derivatives of order $k$ with together with its lower derivatives from order $0,\dots, k-1$ shall be exploited. Hence, if possible, these derivatives are evaluated simultaneously. The C++-struct \texttt{TaskEval} indicates which kind of derivatives shall be evaluated and additionally provides information about the problem size.
\begin{verbatim}
namespace Nixe {
    typedef struct {
        int numStates;
        int numParameters;
        int numObjectives;
        bool evalZerothOrder;
        bool evalFirstOrder;
        bool evalSecondOrder;
        bool evalStateJacobian;
	} TaskEval;
}
\end{verbatim}
The members of \texttt{TaskEval} are explained in the following table.

\begin{tabular}{|l|l|}
  \hline
  % after \\: \hline or \cline{col1-col2} \cline{col3-col4} ...
  Member & Description \\ \hline
  \texttt{numStates} & $n$: the dimension of the state vector $x$ \\
  \texttt{numParameters} & $m$: the dimension of the parameter vector $p$ \\
  \texttt{numObjectives}& $k$ : the target dimension of the objective function $\Phi$  \\
 \texttt{evalZerothOrder} & the function itself shall be evaluated \\
  \texttt{evalFirstOrder} & the first derivative shall be evaluated \\
  \texttt{evalSecondOrder} & the second derivative shall be evaluated \\
  \texttt{evalStateJacobian} & eval the partial derivative of $f(x,p)$ with respect only to $x$ \\
  \hline
\end{tabular}

In the following we use the convention below for an instance \texttt{evalTask} of \texttt{TaskEval}:
\begin{verbatim}
int& n = evalTask.numStates;
int& m = evalTask.numParameters;
int& k = evalTask.numObjectives;
\end{verbatim}


\subsection{User Supplied Function for Initial values}
For the initialization, the following functions have to be provided by the user:
\begin{align}
   x_0: \quad & \R^m \rightarrow \R^n \label{eq:init0}\\
   \frac{\partial x_0}{\partial p} : \quad  & \R^m \rightarrow \R^{n \times m} \label{eq:init1}\\
   \frac{\partial ^2 x_0}{\partial p^2}:  \quad  & \R^m \rightarrow \R^{n \times m \times m} \label{eq:init2}
\end{align}

\begin{verbatim}
namespace Nixe {
    template <typename MatrixInterface>
    class DaeInterface {
    public:
        virtual void EvalInitialValues(TaskEval evalTask, double time,
            double *parameters, double *states, double *sens,
            double ***secondOrderSens)=0;
        // ...
    };
}
\end{verbatim}


The arguments are explained in the following table:


\begin{tabular}{|l|l|l|}
  \hline
  % after \\: \hline or \cline{col1-col2} \cline{col3-col4} ...
  argument & in/out & Description \\ \hline \hline
  \texttt{evalTask} & in & evaluation task \\ \hline
  \texttt{time} & in &  time $t$, not used\\ \hline
  \texttt{parameters[m]} & in & parameter vector $p$ \\ \hline
  \texttt{states[n]} & out & $x_0(p)$ evaluated if \texttt{evalTask.evalZerothOrder == true} \\ \hline
  \texttt{sens[n*m]} & out & $\frac{\partial x_0}{\partial p}(p)$  matrix in column major storage, \\ & & evaluated if \texttt{evalTask.evalFirstOrder == true} \\ \hline
  \texttt{secondOrderSens[n][m][m]} & out &  $\frac{\partial^2 x_0}{\partial p^2}(p)$, three dimensional tensor, canonical ordering \\
   & & evaluated if \texttt{evalTask.evalSecondOrder == true} \\
  \hline
\end{tabular}


\subsection{User Supplied Function for Forwards Integration} \label{sec:EvalForward}
For the forward integration of the first-order sensitivity equations, the following functions have to be provided by the user:
\begin{align}
   f: \quad & \R^n \times \R^m \rightarrow \R^n \label{eq:for0}\\
   \frac{\partial f}{\partial x} : \quad  & \R^n \times \R^m \rightarrow \R^{n \times n} \label{eq:for1}\\
   \frac{\partial f}{\partial p}:  \quad  &\R^n \times \R^m \rightarrow \R^{n \times m} \label{eq:for2}
\end{align}

The Jacobians $\frac{\partial f}{\partial x}$ and $\frac{\partial f}{\partial p}$ can be provided in dense or sparse format. Either one can use the class \texttt{Nixe::CCSInterface} to provide the Jacobians as compressed-column matrix or as dense matrix in FORTRAN-like column-major orientation by using the class \texttt{Nixe::DCMInterface}.

\begin{verbatim}
namespace Nixe {
    class CCSInterface {
    public:
        int nzmax; // maximum number of entries
        int m;     // number of rows
        int n;     // number of columns
        int *pc;   // column pointers, size n+1
        int *ir;   // row indizes, size nzmax
        double *values;  // numerical values, size nzmax
    };
  	
    class DCMInterface {
    // DenseMatrix in column major format
    public:
        int m; //number of rows
        int n; //number of columns
        double *values; // numerical values, size m*n
    };
} // namespace Nixe
\end{verbatim}



\begin{verbatim}
namespace Nixe {
    template <typename MatrixInterface>
    class DaeInterface {
    public:
        virtual void EvalForward(TaskEval evalTask, double time,
            double* states, double *parameters, double *rightHandSide,
            MatrixInterface *stateJacobian,
            MatrixInterface *parameterJacobian)=0;
        // ...
    };
}
\end{verbatim}

\begin{tabular}{|l|l|l|}
  \hline
  % after \\: \hline or \cline{col1-col2} \cline{col3-col4} ...
  Argument & in/out & Description \\ \hline \hline
  \texttt{evalTask} & in & evaluation task \\ \hline
  \texttt{time} & in &  time $t$, not used\\ \hline
  \texttt{states[n]} & in & state vector $x$ \\ \hline
  \texttt{parameters[m]} & in & parameter vector $p$ \\ \hline
  \texttt{rightHandSide}[n] & out & $f(x,p)$, evaluated if \texttt{evalTask.evalZerothOrder == true} \\ \hline
  \texttt{stateJacobian} & out & $\frac{\partial f}{\partial x}(x,p)$  matrix in column major storage, \\ & & evaluated if \texttt{evalTask.evalFirstOrder == true} \\ && or \texttt{evalTask.evalStateJacobian == true}  \\ \hline
  \texttt{parameterJacobian} & out & $\frac{\partial f}{\partial p}(x,p)$  matrix in column major storage, \\ & & evaluated if \texttt{evalTask.evalFirstOrder == true} \\ \hline
\end{tabular}

\subsection{User functions to be provided for sparse format}
If the compressed-column format is used for $\frac{\partial f}{\partial x}$ and $\frac{\partial f}{\partial p}$, the user has to provide information about the number of nonzeros of both Jacobians. He has to provide a function like
\begin{verbatim}
void GetNumberNonZeros(int& nonZerosStateJacobian,
                       int& nonZerosParameterJacobian);
\end{verbatim}
Here \texttt{nonZerosStateJacobian} and \texttt{nonZerosParameterJacobian} shall be set to the number of nonZeros of \texttt{stateJacobian} and \texttt{parameterJacobian} in
		\texttt{EvalForward} (cf. Section \ref{sec:EvalForward}). This information is essential for NIXE to provide proper allocated data structures.


\subsection{User Supplied Function for Backwards Integration}
For the first-order respectively second-order integration of the discrete adjoint equations, the following functions have to be provided by the user where we refer to the Hamiltonian $H$ in (\ref{eq:hamilton}):
\begin{align}
   \frac{\partial H}{\partial x} : \quad  & \R^n \times  \R^{n\times k} \times \R^m \rightarrow \R^{n\times k} \label{eq:rev1}\\
   \frac{\partial H}{\partial p} : \quad  & \R^n \times  \R^{n\times k} \times \R^m \rightarrow \R^{m \times k} \label{eq:rev2}\\
     \frac{D}{Dp} \frac{\partial H}{\partial x} : \quad & \R^n \times  \R^{n\times k} \times \R^m \times
     \R^{n\times m} \times \R^{n\times m \times k}  \rightarrow \R^{n\times m \times k} \notag \\
      & (x,\lambda,p,X,\Lambda) \mapsto \left( \frac{\partial ^2 H_i}{\partial x \partial x} X +
      \frac{\partial ^2 H_i}{\partial \lambda \partial x} \Lambda^{(i)} +
      \frac{\partial ^2 H_i}{\partial p \partial x} \right)_{i=1,\dots,k} \label{eq:rev3} \\
      \frac{D}{Dp} \frac{\partial H}{\partial p} : \quad & \R^n \times  \R^{n\times k} \times \R^m \times
     \R^{n\times m} \times \R^{n\times m \times k}  \rightarrow \R^{n\times m \times k} \notag \\
      & (x,\lambda,p,X,\Lambda) \mapsto \left( \frac{\partial ^2 H_i}{\partial x \partial p} X +
      \frac{\partial ^2 H_i}{\partial \lambda \partial p} \Lambda^{(i)} +
      \frac{\partial ^2 H_i}{\partial p \partial p} \right)_{i=1,\dots,k}\label{eq:rev4}   ,
\end{align}
where $H_i$ is evaluated at $(x,\lambda,p)$. Furthermore we have the semantics
 $X=\frac{\partial x}{\partial p}$ and $\Lambda=\frac{\partial \lambda}{\partial p}$ where
 $\Lambda^{(i)}$ is a slice of a three-dimensional tensor:
\[
 \Lambda^{(i)} = \left(\Lambda_{r,s,i}\right)_{r,s}.
\]

The user has to provide the following C++-function:


\begin{verbatim}
namespace Nixe {
    template <typename MatrixInterface>
    class DaeInterface {
    public:
        virtual void EvalReverse(TaskEval evalTask, double time, double* states,
            double *adjoints, double* parameters, double * stateGradient,
            double *parameterGradient, double *sens, double *sensAdjoints,
            double *stateHessian, double* parameterHessian);
        // ...file:///home/ralf/Desktop/rha_hess.tex

    };
}
\end{verbatim}

 In the following table, the arguments of \texttt{EvalReverse} are described.

\begin{tabular}{|l|l|l|}
  \hline
  % after \\: \hline or \cline{col1-col2} \cline{col3-col4} ...
  Argument & in/out & Description \\ \hline \hline
  \texttt{evalTask} & in & evaluation task \\ \hline
  \texttt{time} & in &  time $t$, not used\\ \hline
  \texttt{states[n]} & in & state vector $x$ \\ \hline
  \texttt{adjoints[n*k]}  & in & adjoint vector $\lambda$ \\
  \texttt{parameters[m]} & in & parameter vector $p$ \\ \hline
  \texttt{stateGradient[n*k]} & out & $H_x(x,\lambda,p)\in \R^{n\times k}$, matrix in column major storage, \\
  & & evaluated if \texttt{evalTask.evalFirstOrder == true} \\ \hline
  \texttt{parameterGradient[m*k]} & out & $H_p(x,\lambda,p)\in \R^{m\times k}$, matrix in column major storage, \\
  & & evaluated if \texttt{evalTask.evalFirstOrder == true} \\ \hline
  \texttt{sens[n*m]} & in & $X \in \R^{n\times m}$, matrix in column major storage \\ \hline
  \texttt{sensAdjoints[n*m*k]} & in & $\Lambda \in \R^{n\times m\times k}$, matrix in column major storage \\ \hline
  \texttt{stateHessian[n*m*k]} & out & $D/Dp \,H_x\in \R^{n\times m \times k}$, tensor in column major storage, \\
  & & evaluated if \texttt{evalTask.evalSecondOrder == true} \\ \hline
  \texttt{parameterHessian[m*m*k]} & out & $D/Dp \,H_p\in \R^{m\times m \times k}$, tensor in column major storage, \\
  & & evaluated if \texttt{evalTask.evalSecondOrder == true} \\ \hline
\end{tabular}

\section{Case Study No. 1}
As first case study we take the chemical reaction system from \cite{Caracotsios1985}.
The system is set of parametric differential-algebraic equations of index 1 as introduced in
(\ref{eq:dae}) and (\ref{eq:dae2}). The dimension of the state vector $x$ is $n=10$, the parameter vector $p$ has the dimension $m=8$. The C-function \texttt{rhs} (mnemonic: right-hand-side), provides an implentation of $f:\R^n \times \R^m \longrightarrow \R^n$:
\begin{verbatim}
void rhs(double* f, double* x, double* p, int n, int m)
{
  // f[n] : out, right-hand-side of DAE
  // x[n] : in, state vector
  // p[m] : in, parameter vector
  // n    : in, dimension of f and x
  // m    : in, dimension of p
  f[0] =-p[2]*x[1]*x[7];
  f[1] =-p[0]*x[1]*x[5]+p[1]*x[9]-p[2]*x[1]*x[7];
  f[2] =p[2]*x[1]*x[7]+p[3]*x[3]*x[5]-p[4]*x[8];
  f[3] =-p[3]*x[3]*x[5]+p[4]*x[8];
  f[4] =p[0]*x[1]*x[5]-p[1]*x[9];
  f[5] =-p[0]*x[1]*x[5]-p[3]*x[3]*x[5]+p[1]*x[9]+p[4]*x[8];
  f[6] =-0.0131+x[5]+x[7]+x[8]+x[9]-x[6];
  f[7] =p[6]*x[0]/(p[6]+x[6])-x[7];
  f[8] =p[7]*x[2]/(p[7]+x[6])-x[8];
  f[9] =p[5]*x[4]/(p[5]+x[6])-x[9];
};
\end{verbatim}
The C-function \texttt{icd} (mnemonic: initial conditions) provide the function
$x_0: \R^m \longrightarrow \R^n$:
\begin{verbatim}
void icd( double *x0, double *p, int n, int m) {
  // x0[n] : out, initial values
  // p[m]  : in, parameter vector
  // n     : in, dimension of x0
  // m     : in, dimension of p
  x0[0]=1.5776;
  x0[1]=8.32;
  x0[2]=0;
  x0[3]=0;
  x0[4]=0;
  x0[5]=0.0131;
  x0[6]=0.5*(-p[6]+ sqrt(p[6]*p[6]+4*p[6]*1.5776));
  x0[7]=0.5*(-p[6]+ sqrt(p[6]*p[6]+4*p[6]*1.5776));
  x0[8]=0;
  x0[9]=0;
};
\end{verbatim}

A possible implemtation of the Hamiltonian $H: \R^n \times \R^{n\times k} \times
\R^m \longrightarrow \R^k$
can be the following
\begin{verbatim}
void hamilton( double *h, double *x, double *lambda, double *p, int n, int m, int k)
{
  // h[k]        : out, value of Hamiltonian
  // x[n]        : in, state vector
  // lambda[n*k] : in, adjoint vector
  // p[m]        : in, parameter vector
  // n           : in, dimension of x
  // m          : in, dimension of p
  // k           : in, dimension of h
  double *f = new double[n];
  rhs(f,x,p,n,m);
  for(int i=0; i<k;i++){
    h[i]=0;
    for(int j=0; j<n; j++)
      h[i]+=lambda[i*n+j]*f[j];
  }
  delete[] f;
}
\end{verbatim}

\subsection{TASK}
Based on the presented C-functions an user should be able provide implemtations of
\begin{enumerate}
\item \texttt{EvalForward},
\item \texttt{EvalReverse},
\item \texttt{EvalInitialValues}.
\end{enumerate}

\bibliographystyle{plain}
\bibliography{ExternalLiterature}

\end{document}
