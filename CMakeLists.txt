# -*- cmake -*-
cmake_minimum_required(VERSION 3.11)
PROJECT(ProjectNixe)


SET ( Nixe_SOURCE_FILES   ./src/NixeNleq1sEquationSystem.cpp
	./src/NixeAdjointData.cpp               ./src/NixeNleq1sInitialization.cpp
	./src/NixeCompressedColumn.cpp          ./src/NixeNleq1sWrapper.cpp
	./src/NixeProblemInfo.cpp        		 ./src/NixeZerosPoly.cpp
	./src/NixeDenseColumnMajor.cpp          ./src/NixeRowTable.cpp
	./src/NixeDenseOutput.cpp               ./src/NixeStepSequence.cpp
	./src/NixeExtrapolationTab.cpp          ./src/NixeSuperLU.cpp
	./src/NixeLapackLu.cpp                  ./src/NixeUtil.cpp
	./src/NixeNleq1sBlocEquationSystem.cpp  ./src/NixeMinPackBlockEquationSystem.cpp
	./src/NixeDaeInterface.cpp )

SET (Nixe_HEADER_FILES ./inc/NixeAdjointData.hpp  ./inc/NixeMatrix.hpp
	./inc/NixeBlasLapack.hpp               ./inc/NixeNleq1sBlocEquationSystem.hpp
	./inc/NixeCCSInterface.hpp             ./inc/NixeNleq1sEquationSystem.hpp
	./inc/NixeCnames.hpp                   ./inc/NixeNleq1sInitialization.hpp
	./inc/NixeCompressedColumn.hpp         ./inc/NixeNleq1sWrapper.hpp
	./inc/NixeCompressedColumnUMFPACK.hpp  ./inc/NixeOptions.hpp
	./inc/NixeComputeCIV.hpp               ./inc/NixeProblemInfo.hpp
	./inc/NixeConfig.hpp                   ./inc/NixeReverseExtrapolationTab.hpp
	./inc/NixeDaeInterface.hpp             ./inc/NixeReverseSolver.hpp
	./inc/NixeDCMInterface.hpp             ./inc/NixeRowTable.hpp
	./inc/NixeDenseColumnMajor.hpp         ./inc/NixeStepControl.hpp
	./inc/NixeDenseOutput.hpp              ./inc/NixeStepSequence.hpp
	./inc/NixeExtrapolationTab.hpp         ./inc/NixeSuperLU.hpp
	./inc/NixeForwardSolver.hpp            ./inc/NixeUtil.hpp
	./inc/NixeLapackLu.hpp                 ./inc/NixeZerosPoly.hpp
	./inc/NixeMinPack.hpp			       ./inc/NixeMinPackBlockEquationSystem.hpp
	./inc/NixeSharedPtr.hpp)


ADD_LIBRARY(Nixe SHARED ${Nixe_SOURCE_FILES}  ${Nixe_HEADER_FILES})

target_include_directories(Nixe BEFORE PUBLIC ${CMAKE_CURRENT_SOURCE_DIR}/inc ${CS_INCLUDE_DIRS})
target_link_libraries(Nixe csparse SuperLU clapack cblas f2c cminpack)
if (UNIX AND NOT APPLE)
  target_link_libraries(Nixe rt)
endif()

if(CMAKE_SOURCE_DIR STREQUAL PROJECT_SOURCE_DIR)
	# Add dependencies
        add_subdirectory(dep/cminpack)
	message("cminpack done.")
        add_subdirectory(dep/csparse)
	message("csparse done.")
	add_subdirectory(dep/f2cblaslapack)
	message("f2cblaslapack done.")
	add_subdirectory(dep/superlu)
	message("superlu done.")
endif()





if(WIN32)
############### Use Uppercases for Fortran routines and create DLL############
set_property(TARGET Nixe PROPERTY COMPILE_DEFINITIONS MAKE_NIXE_DLL WIN32)
############### resolve runtime library conflict #####################
#SET_PROPERTY(TARGET Nixe PROPERTY LINK_FLAGS_DEBUG  "/NODEFAULTLIB:MSVCRT" )
endif()
SET_PROPERTY(TARGET Nixe PROPERTY COMPILE_FLAGS  "-D CMINPACK_NO_DLL" )


############### Files to be installed (also used by CPack) ############


install (TARGETS Nixe  
	RUNTIME DESTINATION bin 
	LIBRARY DESTINATION lib 
	ARCHIVE DESTINATION lib )
install(FILES ${Nixe_HEADER_FILES}
   DESTINATION include)


################## Testing ############################################

enable_testing()

## First Test Case #####

#include( ${CMAKE_CURRENT_SOURCE_DIR}/TestCaracotsiosDense.cmake )


## Second Test Case #####

##include( ${CMAKE_CURRENT_SOURCE_DIR}/TestCaracotsiosSparse.cmake )


## Third Test Case #####

#include( ${CMAKE_CURRENT_SOURCE_DIR}/TestHeatingStorageTankDense.cmake )

## Fourth Test Case #####

#include( ${CMAKE_CURRENT_SOURCE_DIR}/TestWOBatch.cmake )

## Fith Test Case ######

#include( ${CMAKE_CURRENT_SOURCE_DIR}/TestHybridBioReactorDense.cmake )


#### PSeudo Test ###########

#include ( ${CMAKE_CURRENT_SOURCE_DIR}/TestTuranyiDense.cmake )

################## CPack Section ######################################


set (MY_PACKAGE_NAME "Nixe")

 
 set(CPACK_PACKAGE_NAME ${MY_PACKAGE_NAME})
 set(CPACK_PACKAGE_VENDOR "RWTH Aachen University, AVT.PT")
 set(CPACK_PACKAGE_DESCRIPTION_SUMMARY "NIXE - Nixe Is eXtrapolated Euler")
 set(CPACK_PACKAGE_VERSION "1.0.0")
 set(CPACK_PACKAGE_VERSION_MAJOR "1")
 set(CPACK_PACKAGE_VERSION_MINOR "0")
 set(CPACK_PACKAGE_VERSION_PATCH "0")
 set(CPACK_PACKAGE_INSTALL_DIRECTORY ${MY_PACKAGE_NAME})
 SET(CPACK_RESOURCE_FILE_LICENSE ${CMAKE_CURRENT_SOURCE_DIR}/doc/license.txt)
 #set(CPACK_NSIS_MODIFY_PATH "ON")
include(CPack)

